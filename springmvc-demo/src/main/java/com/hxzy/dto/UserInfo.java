package com.hxzy.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Getter
@Setter
@ToString
public class UserInfo {
    private String name;
    private Integer age;
    private String sex;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

}
