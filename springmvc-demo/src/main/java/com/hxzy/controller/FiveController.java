package com.hxzy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 文件上传
 *
 * @author tonneyyy
 */
@Controller
public class FiveController {

    //文件存放的位置
    private static final String ROOT_PATH="D:\\image_server";
    //返回前端访问地址
    private static final String IMAGE_SERVER="/img/";

    /**
     * 显示上传页面
     * @return
     */
    @GetMapping(value = "/demo06")
    public String  demo06(){
     return "demo06";
    }

    /**
     * 文件上传
     * @param attach
     * @return  json数据
     * @throws IOException
     */
    @ResponseBody
    @PostMapping(value = "/fileupload")
    public Map<String,Object> uploadFile(MultipartFile attach) throws IOException {
        Map<String,Object>  mp=new HashMap<>();

        // 如果文件为空
        if(attach.isEmpty()){
            mp.put("error","上传文件不允许为空");
            return mp;
        }

        //文件原始名称
        String oldFileName=attach.getOriginalFilename();
        //文件的mime类型  https://www.jianshu.com/p/6e3b0d980bec   ，如果没有后缀名，它会认为你一个二进制流文件 application/octet-stream
        String mineTyipe= attach.getContentType();
        //文件大小 字节
        long size=attach.getSize();

        String ext="";
        //得到文件后缀名,  这个判断代表没有后缀名
        if(oldFileName.lastIndexOf(".")>0){
            ext= oldFileName.substring( oldFileName.lastIndexOf("."));
        }

        //创建唯一的名称   UUID最简单的唯一标识
        String fileName= UUID.randomUUID().toString()+ext;
        //创建一个新的文件
        File newFile=new File(ROOT_PATH, fileName);

        //文件另存为哪里  transferTo  需要调用 commons-fileupload.jar的类
        attach.transferTo( newFile );

        //返回给前端信息
        String url =IMAGE_SERVER+fileName;
        mp.put("filename",oldFileName);
        mp.put("url",url);
        return mp;
    }
}
