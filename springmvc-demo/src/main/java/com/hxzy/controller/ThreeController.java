package com.hxzy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * 接收数组或集合、map等
 *
 * @author tonneyyy
 */
@Controller
public class ThreeController {


    @GetMapping(value = "/deletes")
    public String arrInfo(Integer[]  ids, Model model){

        for(Integer s : ids){
            System.out.println(s);
        }
        model.addAttribute("ids",ids);
        return "demo04";
    }

    /**
     * 后台使用map来接收数据
     * @param map
     * @param model
     * @return
     */
    @PostMapping(value = "/map")
    public String mapInfo(@RequestBody  Map  map, Model model){

        for(Object key : map.keySet()){
            System.out.println(key.toString()+","+ map.get(key));
        }
        model.addAttribute("mp",map);
        return "demo05";
    }
}
