package com.hxzy.controller;

import com.hxzy.dto.UserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 接收复杂参数
 *
 * @author tonneyyy
 */
@Controller
public class SecondController {

    /**
     * 显示用户界面
     * String 就是之前的viewName
     * @return
     */
    @GetMapping(value = "/user")
    public String showUserPage(Model model){
        return "user";
    }

    /**
     * 保存数据  前端提交数据类型 application/x-www-form-urlencoded
     * @param userInfo
     * @return
     */
    @PostMapping(value = "/user")
    public String add(UserInfo userInfo){
        System.out.println(userInfo.toString());
        return "user";
    }


    /**
     * 保存数据  前端提交数据类型 application/json
     * 使用工具模拟提交数据 (axios)
     * @param userInfo
     * @return
     */
    @PostMapping(value = "/user/stream")
    public String addStream(@RequestBody UserInfo userInfo){
        System.out.println(userInfo.toString());
        return "user";
    }

}
