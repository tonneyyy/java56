package com.hxzy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 引用静态资源
 * @author tonneyyy
 */
@Controller
public class SixController {

    @GetMapping(value = "/demo07")
    public String demo07(){
        return "demo07";
    }
}
