package com.hxzy.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 输入json
 *
 * @author tonneyyy
 */
@RestController
public class FourController {


    @GetMapping(value = "/json/{n}")
    public List  getInfo(@PathVariable(value = "n") int n){
        List<Map<String,Object>>  list=new ArrayList<>();
        for(int i=0;i<n;i++){
            Map<String,Object>  mp=new HashMap<>();
            mp.put("name","小张"+i);
            mp.put("sex", i%2==0?"男":"女");
            mp.put("age", 10+i);
            list.add(mp);
        }
        return list;
    }
}
