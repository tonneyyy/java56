package com.hxzy.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@RestController
public class OrderController {

    @GetMapping(value = "/order")
    public String orderList(){
        return "要显示订单的页面";
    }
}
