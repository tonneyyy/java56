





# redis

# 1、什么是redis

Remote Dictionary Server(Redis) 是一个由 Salvatore Sanfilippo 写的 key-value 存储系统，是跨平台的非关系型数据库。 NoSql

Redis 是一个开源的使用 ANSI C 语言编写、遵守 BSD 协议、支持网络、可基于内存、分布式、可选持久性的键值对(Key-Value)存储数据库，并提供多种语言的 API。



是c语言开发的，基内存的，键-值对存储系统.





未来NoSql：      redis(缓存)，      mongodb(数据存储),       ElaststicSearch(搜索引擎)

没有分布式缓存之前：     基于内存的框架    EhCacha   ,     取而代之就是： Redis



# 2、redis作用(背)

  用redis缓存用的   （临时数据存储）

 用于大批量数据运算

用于位图bitmap运算  （连续签到打卡）

投票

购物车

前后端分离 (令牌存储，   验证码存储)





  Redis 的 QPS 可以达到约 100000（每秒请求数）

*mysql支持每秒并发16384*



默认redis端口    6379  

默认没有密码，更改密码   requirepass   密码

daemonize yes         后台运行



本地启动redis服务

./bin/redis-server      ./redis.conf 

可以看到redis后台进程

ps -ef |grep redis



# 3、redis数据类型(背)

## 3.1、基本数据类型

1）、字符串(String)  

2)、哈希(Hash)

3)、有序列表(list)

4)、无效集合(sets)  

5)、  有序集合(sorted sets)



## 3.2、复合（扩展）数据类型

1)、 位图  bitmap

2)、Streams

3)、Geospatial indexes     地理位置

4)、Bitfields

5)、HyperLogLog





## 3.3、redis源码安装

先安装编译环境

apt install  gcc

apt install  软件2

apt install 软件3





1.下载
wget https://download.redis.io/releases/redis-6.0.9.tar.gz
2.解压
tar xzf redis-6.0.9.tar.gz
3.移动到你要安装的目录,我这里安装到了/user/local下
sudo mv ./redis-6.0.9 /usr/local/redis
4.进入你移动的目录
cd /usr/local/redis
5.编译redis
sudo make
6.测试编译是否成功(这一步时间会比较长,测试耗时5分钟左右)
sudo make test
7.安装
make install    PREFIX=/usr/local/redis     (/usr/local/redis 安装目录)



# 4、使用docker 虚拟技术安装软件

## 4.1、什么是docker(背)

Docker 是一个开源的应用容器引擎，让开发者可以打包他们的应用以及依赖包到一个可移植的[镜像](https://baike.baidu.com/item/镜像/1574?fromModule=lemma_inlink)中，然后发布到任何流行的 [Linux](https://baike.baidu.com/item/Linux?fromModule=lemma_inlink)或[Windows](https://baike.baidu.com/item/Windows/165458?fromModule=lemma_inlink)操作系统的机器上，也可以实现[虚拟化](https://baike.baidu.com/item/虚拟化/547949?fromModule=lemma_inlink)。容器是完全使用[沙箱](https://baike.baidu.com/item/沙箱/393318?fromModule=lemma_inlink)机制，相互之间不会有任何接口。

<img src="assets/1.png">

用来替换docker的框架简称  k8s



学习 网址  https://www.runoob.com/docker/docker-tutorial.html



## 4.2、安装docker

```java
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```

需要的 ubuntu至少18上以的版本





## 4.3、docker组成部份(背)

Docker 包括三个基本概念:

**仓库（Repository）**：仓库可看成一个代码控制中心，用来保存镜像。

- **镜像（Image）**：Docker 镜像（Image），就相当于是一个 独立的linux下面的root 文件系统。

  

- **容器（Container）**：镜像（Image）和容器（Container）的关系，就像是面向对象程序设计中的类和实例一样，镜像是静态的定义，容器是镜像运行时的实体。  容器可以被创建、启动、停止、删除、暂停等。

  

  docker运行：  执行容器运行命令，     检查 本地仓库  是否有你执行的镜像文件， 如果没有，就要去docker官网下载保存到本地的仓库里面，然后再把本地仓库的镜像，运行到 docker 容器里面（每一个容器可以简称为实例）

  

Docker 使用客户端-服务器 (C/S) 架构模式，使用远程API来管理和创建Docker容器。

Docker 容器通过 Docker 镜像来创建。

容器与镜像的关系类似于面向对象编程中的对象[容器]与类【镜像】。



## 4.4、docker图

<img src="https://www.runoob.com/wp-content/uploads/2016/04/576507-docker1.png">



clients 指就是安装有拥有docker命令的电脑

hosts  主机

docker hub(docker俱乐部)   看成mvn的https://mvnrepository.com/ 库存  ,      所有的镜像都存放到  docker hub里面的

​                    https://hub.docker.com/    docker 仓库网站



## 4.5、 docker  image  镜像命令(背)

当运行容器时，使用的镜像如果在本地中不存在，docker 就会自动从 docker 镜像仓库中下载，默认是从 Docker Hub 公共镜像源下载。

下面我们来学习：

- 1、管理和使用本地 Docker 主机镜像
- 2、创建镜像



### image常用命令(背)

| 命令                  | 作用                         |
| --------------------- | ---------------------------- |
| docker  images        | 查看本地的镜像有哪些         |
| docker pull    镜像名 | 拉取（下载）指定镜像名的文件 |
| docker search  镜像名 | 在本地查询镜像               |
| docker  rmi    镜像名 | 删除本地镜像                 |



## 4.6、 docker  Container 容器的命令(背)

| 命令                                            | 作用                                                         |
| ----------------------------------------------- | ------------------------------------------------------------ |
| docker   ps                                     | 查看正在运行的容器(应用程序)                                 |
| docker ps  -a                                   | 查看所有的容器(应用程序)                                     |
| docker run  镜像名                              | 安装一个新的镜像到容器里面 (安装一个新的应用程序)            |
| 参数                                            | --name    名称            为容器指定一个新的名称<br />-v   linux本地目录:容器里面的目录                把容器目录共享到本地的目录中<br />---指令   其它软件固有配置<br />-p linux系统端口:容器的端口                         把linux端口 映射到  容器的端口 |
| docker  stop   容器名/容器id                    | 停止一个正在运行的容器                                       |
| docker  rm     容器名/容器id                    | 删除一个已停止的容器( 相当于是windows卸载这个软件)           |
| docker restart    容器名/容器id                 | 重启容器                                                     |
| docker   exec  -it   容器名/容器id    /bin/bash | 进入到容器里面的linux系统中                                  |
| docker cp      linux本地文件:容器id/目录        | 复制linux本地文件上传到 容器里面指定位置                     |
| docker cp    容器id/目录:linux本地文件          |                                                              |



## 4.7、在docker中安装redis

简单版的

```js
docker run -itd  -p 6379:6379    redis:6.2.4 --requirepass hxzy
```

复杂版的

```js
docker run -itd --name my-redis -p 6379:6379  -v /home/hadoop/software/myredis/data:/data -v /home/hadoop/software/myredis/conf/redis.conf:/usr/local/etc/redis/redis.conf  redis:6.2.4 --requirepass hxzy
```



```js
docker run -itd --name my-redis -p 6379:6379  -v /home/hadoop/software/myredis/config/redis01.conf:/etc/redis/redis.conf  redis:6.2.4       redis-server /etc/redis/redis.conf           启动服务，指定自己的配置文件
```





docker run -itd                     在后台运行容器

​       --name my-redis         指定个自定义容器名

​       -p 6379:6379               指定端口

​       -v /home/hadoop/software/myredis/data:/data                           把容器中的数据共享到本地/home/hadoop/software/myredis/data(需要你自己创建一个/home/hadoop/software/myredis目录)

​       -v /home/hadoop/software/myredis/conf/redis.conf:/usr/local/etc/redis/redis.conf     把容器/usr/local/etc/redis/redis.conf 配置共享到/home/hadoop/software/myredis/conf/redis.conf这里来

​      redis:6.2.4                  镜像名:版本号

​    --requirepass hxzy     配置redis密码



最简单安装

```js
                   linux端口:redis端口           redis的端口是永远不能变的
docker run -itd  -p 6379:6379   redis:6.2.4 --requirepass hxzy

如果再要安装一台redis
docker run -itd  -p 6380:6379   redis:6.2.4 --requirepass hxzy

如果还要安装一台redis
docker run -itd  -p 6381:6379   redis:6.2.4 --requirepass hxzy

如果还要安装一台redis
docker run -itd  -p 6382:6379   redis:6.2.4 --requirepass hxzy
```









## 5、redis的配置文件(背)

​     windows中 叫  redis.windows.conf            在redis中叫 redis.conf

  

| 配置名                                       | 作用                                                         |
| -------------------------------------------- | ------------------------------------------------------------ |
| bind 0.0.0.0                                 | 绑定主机的IP，0.0.0.0  允许任意电脑连接                      |
| port 6379                                    | 端口6379                                                     |
| databases 16                                 | 默认创建16个数据库 通过      SELECT <dbid>   0-15            |
| save 900 1<br/>save 300 10<br/>save 60 10000 | 保存数据到磁盘的配置<br />after 900 sec (15 min) if at least 1 key changed（15分之内有一个key被更改）<br/>after 300 sec (5 min) if at least 10 keys changed（5分之内至少有10个key被更改）<br/> after 60 sec if at least 10000 keys changed（1分之内至少有10000个key被更改） |
| dbfilename dump.rdb                          | 数据库存储文件名     RDB 存储模式                            |
| requirepass   密码                           | redis的密码                                                  |
| # slaveof <masterip> <masterport>            | 集群配置    主服务器IP    主服务器的端口                     |
| # masterauth <master-password>               | 集群配置    连接主服务器的密码                               |
| appendonly no                                | AOF  存储模式                                                |
| appendfilename "appendonly.aof"              | aop文件名                                                    |

  

# 6、redis命令

## 6.1、redis对数据库操作命令(背)

1、dbsize        统计当前数据库键的个数

2、flushdb     删除当前数据库中所有的键

3、flushall     删除所有数据库中所有的键



## 6.2、redis的对某个数据库的键操作(背)

1、DEL key                该命令用于在 key 存在时删除 key。      成功返回>0的数字     失败返回0
2、DUMP key          序列化给定 key ，并返回被序列化的值。
3、EXISTS key            检查给定 key 是否存在。
4、EXPIRE key seconds       为给定 key 设置过期时间，以秒计。
5、KEYS pattern         查找所有符合给定模式( pattern)的 key 。
6、MOVE key db       将当前数据库的 key 移动到给定的数据库 db 当中。
7、PERSIST key        移除 key 的过期时间，key 将持久保持。

8、TTL key                  以秒为单位，返回给定 key 的剩余生存时间(TTL, time to live)。

 

9、RANDOMKEY                   从当前数据库中随机返回一个 key 。
10、RENAME key newkey             修改 key 的名称15	RENAMENX key newkey            仅当 newkey 不存在时，将 key 改名为 newkey 。

11、SCAN cursor [MATCH pattern] [COUNT count]         迭代数据库中的数据库键。

12、TYPE key     返回 key 所储存的值的类型。

13、EXPIREAT key timestamp
        EXPIREAT 的作用和 EXPIRE 类似，都用于为 key 设置过期时间。 不同在于 EXPIREAT 命令接受的时间参数是 UNIX 时间戳(unix timestamp)。

14	PEXPIRE key milliseconds             设置 key 的过期时间以毫秒计。
15	PEXPIREAT key milliseconds-timestamp           设置 key 过期时间的时间戳(unix timestamp) 以毫秒计

16	PTTL key              以毫秒为单位返回 key 的剩余的过期时间。



## 6.3、 String数据类型操作

### 6.3.1、redis中的String数据类型

String 是最基本的 key-value 结构。 而 SDS 则作为 String 数据类型的一种数据结构实现。

String 类型没有直接使用C语言传统的[字符串](https://so.csdn.net/so/search?q=字符串&spm=1001.2101.3001.7020)表示(以空字符 \0 结尾的字符数组，以下简称 C字符串)，而是自己构建了一种名为简单动态字符串(simple dynamic string,SDS)的抽象类型，并将 SDS 作为默认字符串表示。



### 6.3.2、redis中的String应用场景(背)

1. **存储数据 **    如常见存储 K-V         V( 字符串、JSON字符串)。
2. **程序计数 **    INCR   命令递增或递增一个数。
3. **分布式锁**    使用 SET key value NX ，NX 不存在才写入。           为这个键添加一个过期时间  expire   key   秒   (什么情况用：秒杀)
4. **单点登录**   可作为存储共享会话实现单点登录（令牌存储）。



### 6.3.3、内部编码

String 对象的内部编码（encoding）有 3 种 ：int、raw(序列化的值) 和 embstr

<img src="assets/2.png">

SDS    简单动态字符串



#### 6.3.3.1、int 编码

String 如果存储的是整数值，并且这个整数值可以用 long 类型来表示，那么字符串对象会将整数值保存在字符串对象结构的ptr属性里面（将void*转换成 long），并将字符串对象的编码设置为int。

<img src="assets/3.png">



#### 6.3.3.2、embstr 编码

String 如果存储的是字符串，并且字符串长度小于等于一定的长度则使用 embstr 编码方式（专门用于保存短字符串的一种优化编码方式）。

其中 redis 2.+ 长度小于等于 32 字节、redis 3.0-4.0 是 39 字节、redis 5.0 是 44 字节



#### 6.3.3.3、raw 编码

String 如果存储的是字符串，并且字符串长度大于 embstr 编码的取值长度则使用 raw （二进制）方式编码。

<img src="assets/4.png">





### 6.3.4、redis中String的常用命令(背)

https://redis.io/commands/?group=string]

| 命令                         | 作用                                                         |
| ---------------------------- | ------------------------------------------------------------ |
| get       键                 | 取得某个键                                                   |
| set    键   值               | 设定某个键和值                                               |
| set    键   值    EX  过期秒 | 设定某个键和值并添加一个过期时间（秒）                       |
| SET 键   值  NX              | 当某一个键不存在的时候，才能为它添加一个键和值( 分布式锁)    |
| SET 键   值  NX   EX 过期秒  | 当某一个键不存在的时候，才能为它添加一个键和值( 分布式锁)，并且设定一个过期时间 |
| INCR 键                      | 为某个键增加1，如果这个键不存在的时候，添加一个键并且赋值为1 |
| INCRBY 键   增加值           | 为某个键增加几                                               |
| decr   键                    | 为某个键减去1                                                |
| DECRBY 键  减去几            | 为某个键减去几                                               |



## 6.4、List数据类型操作

### 6.4.1、List数据类型  （quicklist）

List 列表是简单的字符串列表，**按照插入顺序排序**，可以从头部或尾部向 List 列表添加元素。

列表的最大长度为 `2^32 - 1`，也即每个列表支持超过 `40 亿`个元素。

### **内部实现**

List 类型的底层数据结构是由**双向链表或压缩列表**( ziplist (压缩列表) )实现的：

- 如果列表的元素个数小于 `512` 个（默认值，可由 `list-max-ziplist-entries` 配置），列表每个元素的值都小于 `64` 字节（默认值，可由 `list-max-ziplist-value` 配置），Redis 会使用**压缩列表**作为 List 类型的底层数据结构；
- 如果列表的元素不满足上面的条件，Redis 会使用**双向链表**作为 List 类型的底层数据结构；

但是**在 Redis 3.2 版本之后，List 数据类型底层数据结构就只由 quicklist 实现了，替代了双向链表和压缩列表**。

quicklist说明：   https://blog.csdn.net/m0_51504545/article/details/117603534



 ziplist 是一种十分节省内存的结构，紧凑的内存布局、变长的编码方式在内存使用量上有着不小的优势。但是修改操作下并不能像一般的链表那么容易，需要从新分配新的内存，然后复制到新的空间。

quicklist =  ziplist 节省内存  +   双向链表优点的



List 数据类型的底层实现其实是 quicklist。
quicklist 是一个双向链表的复合结构体。quicklistNode 让它拥有链表的查询优点；ziplist 让它在内存使用上有着相对链表可以节省大量前后指针的优势。
但是需要合理的配置节点中 ziplist 的 entry 个数。entry 过少，则退化成普通链表。entry 过多，则会放大 ziplist 的缺点。



### 6.4.2、使用场景

  1)、比如我需要记录朋友圈动态的点赞用户信息(如id, name, avatar)

  2）、用户对你的评论（时间从先到后评论）

​     其实用 MySQL 也可以实现，但是避免DB的查询，还是用 Redis 来提高查询效率吧。



### 6.4.3、List常用指令



<img src="assets/5.png">



  <img src="assets/6.png">



https://redis.io/docs/data-types/lists/

|                                     | 作用                                   |
| ----------------------------------- | -------------------------------------- |
| lpush       键     值               | 在列表顶部插入数据 （下标0的位置插入） |
| rpush     键   值                   | 在列表尾部插入数据 （在列表未尾追加）  |
| lpop    键                          | 取出并移出list头部第一笔数据           |
| rpop 键                             | 取出并移出list尾部最后一笔数据         |
| llen   键                           | 统个列表的长度                         |
| lrange   键    开始下标    结束下标 | 获取列表指定范围内的元素               |
| lindex    键    下标                | 通过下标索引列表中元素的值             |



## 6.5、Hash数据结构

### 6.5.1、hash数据结构

hash类型是一种类似关系型数据库结构的数据结构。有一个键名      ，键存的内容是以键值对的形式存在

<img src="assets/7.png">



### 6.5.2、实现算法

Hash 类型的底层数据结构是由**压缩列表(ziplist)或哈希表**实现的：

- 如果哈希类型元素个数小于 `512` 个（默认值，可由 `hash-max-ziplist-entries` 配置），所有值小于 `64` 字节（默认值，可由 `hash-max-ziplist-value` 配置）的话，Redis 会使用**压缩列表**作为 Hash 类型的底层数据结构；
- 如果哈希类型元素不满足上面条件，Redis 会使用**哈希表**作为 Hash 类型的 底层数据结构。

**在 Redis 7.0 中，压缩列表数据结构已经废弃了，交由 listpack 数据结构来实现了**。



https://zhuanlan.zhihu.com/p/435724042

- 从listpack自身来讲，把增删改统一成新增与修改两种模式，统一在lpinsert函数中实现，为listpack中的特色，redis其他数据结构并没有用到。而listpack的查找，从设计上来说，只能遍历，目前也看不到更好的优化。
- 与ziplist做对比的话，牺牲了内存使用率，避免了连锁更新的情况。从代码复杂度上看，listpack相对ziplist简单很多，再把增删改统一做处理，从listpack的代码实现上看，极简且高效。
- 从redis 5中率先在streams中引入listpack，直到6后作为t_hash御用底层数据结构，redis应该是发现极致的内存使用远远不如提高redis的处理性能。也能看出来从redis 08年出现到如今内存的普及与价格下降，各个平台qps的显著提高趋势。
- 从这里也可以猜到redis之后的优化趋势，将是淡化极致的内存使用率，向更快的方向发力。不过较为可惜的是，可能不会再出现堪称‘奇技淫巧’的类似ziplist，hyperloglog等复杂的代码了。



### 6.5.3、使用场景

 1）、存储对象信息

 2)、**购物车**

### 6.5.4、命令

https://redis.io/commands/?group=hash

| 命令                                        | 作用                                                         |
| ------------------------------------------- | ------------------------------------------------------------ |
| HSet       键     字段  值                  | 赋值                                                         |
| HGet   键    字段                           | 取值                                                         |
| HMGET  键    字段1  字段2   字段....        | 取得指定的多个字段的值                                       |
| HMSet  键      字段1   值1      字段2   值2 | 为某个键同时添加多个字段                                     |
| HExists   键  字段                          | 查询某个键中的字段是否存在，存在返回1，不存在返回0           |
| HDel    键   字段   【字段1，字段2】        | 删除指定字段的值，成功返回1，失败返回0                       |
| HINCRBY   键    字段     数值               | 为指定键下面的字段增加或减少几 ,  正数就是增加，负数就是减少 |



## 6.6、Set数据结构

### 6.6.1、Set数据类型 

Set 类型是一个无序并唯一的键值集合，它的存储顺序不会按照插入的先后顺序进行存储。

一个集合最多可以存储 `2^32-1` 个元素。概念和数学中个的集合基本类似，可以交集，并集，差集等等，所以 Set 类型除了支持集合内的增删改查，同时还支持多个集合取交集、并集、差集



Set 类型和 List 类型的区别如下：

- List 可以存储重复元素，Set 只能存储非重复元素；
- List 是按照元素的先后顺序存储元素的，而 Set 则是无序方式存储元素的。
- 

### **内部实现**

Set 类型的底层数据结构是由**哈希表或整数集合**实现的：

- 如果集合中的元素都是整数且元素个数小于 `512` （默认值，`set-maxintset-entries`配置）个，Redis 会使用**整数集合**作为 Set 类型的底层数据结构；
- 如果集合中的元素不满足上面条件，则 Redis 使用**哈希表**作为 Set 类型的底层数据结构。



### 6.6.2、使用场景

1)、不可重复、交集，并集，差集

2）、统计出昨天登录用户和前天登录用户的区别 (交集)

3）、统计新注册用户前3天的登录情况  (交集)

4)、抽奖统计(用户中奖，不可能再中奖)   唯一性



### 6.6.3、命令

https://redis.io/commands/?group=set

| 命令                                      | 作用                                                         |
| ----------------------------------------- | ------------------------------------------------------------ |
| SAdd       键        值                   | 添加值                                                       |
| SISMEMBER   键    值                      | 查看值是否在键中，存在返回1，不存在返回0                     |
| SPOP    键       个数                     | 随机从键中删除指定个数的值                                   |
| SMEMBERS  键                              | 显示出集合的值                                               |
| SMOVE  集合1   集合2    "要删除集合1的值" | 删除集合1中的指定的值，移到集合2中去                         |
| SREM  键   "要删除的值"                   | 删除指定集合中的值，成功返回1，失败返回0                     |
| SDIFF     key1 key2                       | 求出两个set集合的差集<br />key1 = {a,b,c,d} <br />key2 = {c} <br />SDIFF key1 key2       结果为：   {a,b,d} |
| SUNION key [key ...]                      | 求出多个集合的并集<br />key1 = {a,b,c,d} <br />key2 = {c,e} <br />SUNION key1 key2       结果为：   {a,b,c,d,e} |
| SINTER key [key ...]                      | 求出多个集合的交集<br />key1 = {a,b,c,d} <br />key2 = {c} <br />SINTER key1 key2       结果为：   {c} |



## 6.7、Sorted Set   (zset)可排序集合

### 6.7.1、数据类型

zset类型和set类型都是属于集合类型，两者不同点，在设置zset数据时要设置一个分数，这个分数可以用来做数据排序,并且zset类型的数据是有序的，因此zset也被叫做有序集合。

实现原理  quicklist



### 6.7.2、使用场景

1）、投票

2)、排行榜

3）、排序相关的



### 6.7.3、常用命令

https://redis.io/commands/?group=sorted-set

| 命令                                                         | 作用                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ZAdd       键    分数        值                              | 添加值                                                       |
| ZINCRBY   键     分数     值                                 | 在指定的键集合中查询值，并且把值的分数加上几分               |
| ZSCORE   键   值                                             | 在指定的键集合中查询值的分数                                 |
| ZRANGEBYSCORE key min max [WITHSCORES] [LIMIT offset count]  | 按照分数进行排序(升序)，取得前几笔 (6.0.2以后被弃用用zrang  with来代替)<br />ZRANGEBYSCORE  键   (1 5       `1 < score <= 5` |
| ZREVRANGEBYSCORE key max min [WITHSCORES] [LIMIT offset count] | 按照分数进行排序(降序)，                                     |
| ZRANGEBYLEX key min max [LIMIT offset count]                 | 按照hash值来排序<br />ZRANGEBYLEX names [A (B                |
| ZCOUNT key min max                                           | 按照score统计满足条件的个数<br />ZCOUNT 键    -inf +inf        统计这个键所有个数 <br />ZCOUNT myzset (1 3       统计这个 1<键<=3 个数 <br /> |
| ZMPOP numkeys key [key ...] <MIN \| MAX> [COUNT count]       | (redis的7.0.0)移出满足条件的值，如果键为空就会报错<br />ZADD myzset 1 "one" 2 "two" 3 "three" <br />ZMPOP 1 myzset MIN     移出分数最小的1个值 <br />SUNION key1 key2       结果为：   {a,b,c,d,e} |
|                                                              |                                                              |



## 6.8、其它命令-位图  bitmap

### 6.8.1、数据结构

Bitmap，即位图，是一串连续的二进制数组（0和1），可以通过偏移量（offset）定位元素。BitMap通过最小的单位bit来进行`0|1`的设置，表示某个元素的值或者状态，时间复杂度为O(1)。

由于 bit 是计算机中最小的单位，使用它进行储存将非常节省空间，特别适合一些数据量大且使用**二值统计的场景**。



### 6.8.2、使用场景

连续签到、统计范围的个数(50000 万 用户只需要 6 MB 的空间)



### 6.8.3、常用命令

| 命令                           | 作用                                                   |
| ------------------------------ | ------------------------------------------------------ |
| SETBIT 键  offset  value(0或1) | 设置值，其中value只能是 0 和 1                         |
| GETBIT key offset              | 取得值                                                 |
| BITCOUNT key start end         | 获取指定范围内值为 1 的个数  start 和 end 以字节为单位 |



### 6.8.4、统计一个人这一个月连续签到情况

用户id:  1001:2022         key规则：     sign:用户id:当前年月

获取当前年(LocalDate.now().getYear())     当前月(LocalDate.now().getMonth())    当前日  LocalDate.now().getDay()        

​                                          键                               位移(offset)                 值

10/1        签到       setbit         sign:1001:2022          当前日-1                     1

10/2          没有签到

10/3     签到    setbit         sign:1001:2022          当前日-1                    1

....

10/31



统计签到次数      BITCOUNT   sign:1001:2022          0     ?值



### 6.8.5、计录每天用户有多少人登录

用户id  设定为long      key=loginstate:年：月：日

setbit     loginstate:2022:10:1        offset(用户id)      1

统计数据库 0    到    用户表最大值          bitcount         loginstate:2022:10:1      0      用户表最大值(id)

记录99万用户，消费内存大小为122kb左右



## 6.9、其它命令-HyperLogLog

### 6.9.1、使用场景

- 统计注册 IP 数
- 统计每日访问 IP 数
- 统计页面实时 UV 数
- 统计在线用户数
- 统计用户每天搜索不同词条的个数



鉴于 HyperLogLog 不保存数据内容的特性，所以，它只适用于一些特定的场景。

我这里给出一个最常遇到的场景需要：计算日活、7日活、月活数据。

分析：如果我们通过解析日志，把 ip 信息（或用户 id）放到集合中，

例如：HashSet。如果数量不多则还好，但是假如每天访问的用户有几百万。无疑会占用大量的存储空间。

且计算月活时，还需要将一个整月的数据放到一个 Set 中，这随时可能导致我们的程序 OOM。



### 6.9.2、常用命令

https://redis.io/commands/?group=hyperloglog

| 命令                                      | 作用                   |
| ----------------------------------------- | ---------------------- |
| PFADD key    值                           | 添加值                 |
| PFCOUNT key                               | 统计个数（不重复的值） |
| PFMERGE destkey sourcekey [sourcekey ...] | 合并多个值到一个值中   |



```java
PFADD hll1（键） foo bar zap a
PFADD hll2 a b c foo
 
PFMERGE hll3 hll1 hll2      合并 hll1 和 hll2 到  hll3中        foo bar zap  a    b   c 
PFCOUNT  hll3             结果为6  
```



## 6.10、其它命令-geospatial（地理位置服务）

 ### 6.10.1、数据算法

Redis 在 3.2 版本中加入了地理空间（geospatial）以及索引半径查询的功能，主要用在需要地理位置的应用上。将指定的地理空间位置（经度、纬度、名称）添加到指定的 key 中，这些数据将会存储到 sorted set。这样的目的是为了方便使用 GEORADIUS 或者 GEORADIUSBYMEMBER 命令对数据进行半径查询等操作。也就是说，推算地理位置的信息，两地之间的距离，周围方圆的人等等场景都可以用它实现。

小结：geo 底层原理是使用 zset来实现的，因此我们也可以使用 zset 的命令操作 geo。


### 6.10.2、常用命令

| 命令                                                         | 作用                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| GEOADD key [NX \| XX] [CH] longitude latitude member [longitude   latitude member ...] | 添加值位置信息                                               |
| GEODIST key member1 member2 [M \| KM \| FT \| MI]            | 计算两个坐标的位置距离  <br />m代表米，KM公里，ft英尺, mi英里 |
| geopos    key        member1    [member1]                    | 获取键中的名称经纬度                                         |
| georadius key lopngitude latitude radius unit [WITHCOORD] [WITHDIST] [COUNT count] [ASCDESC] […] | 以给定的经纬度为中心，返回与中心距离 不超过 给定最大距离的值   加上 WITHCOORD 可以返回经纬度<br />withdist 可以返回距离 |



### 6.10.3、案例

往 china:city 这个 key 里添加了 3个地方的经纬度：北京、上海、广州。

geoadd   china:city    116.405285        39.904989        beijing

geoadd   china:city    121.472644        31.231706        shanghai

geoadd   china:city    113.280637        23.125178        guangzhou



求 离我不超过1000KM的城市有哪些   



georadius     china:city       106.4663597202377 29.62638524890341      1000  km  

 

在数据库 函数   st_distance:st_distance 计算的结果单位是度，需要乘111195（地球半径6371000*PI/180）是将值转化为米

​                     坐标1                         ，坐标2

st_distance(POINT (**lon1** , **lat1** ),POINT(**lon2** , **lat2**))* 111195 

  重庆  --》广东距离

select (st_distance(point(106.4663597202377 ,29.62638524890341), point(113.280637, 23.125178))*111195)/1000  as km



# 7、redis的存储方式（背）

## 7.1、redis有2种存储方式

 https://redis.io/docs/manual/persistence/

- **RDB** (Redis Database): The RDB persistence performs point-in-time snapshots of your dataset at specified intervals.   (默认)

  ​                                        RDB持久化以指定的时间间隔执行数据集的时间点快照( 攒一波，存一次)

  ​                                        save  900   1          after 900 sec (15 min) if at least 1 key changed
  ​                                       save  300 10           after 300 sec (5 min) if at least 10 keys changed
  ​                                       save  60  10000      after 60 sec if at least 10000 keys changed

  

- **AOF** (Append Only File): The AOF persistence logs every write operation received by the server, that will be played again at server startup, reconstructing the original dataset. Commands are logged using the same format as the Redis protocol itself, in an append-only fashion. Redis is able to [rewrite](https://redis.io/docs/manual/persistence/#log-rewriting) the log in the background when it gets too big.

  ​             只要有数据写入操作，就会立即持久化   （默认是关闭的，  如果想要找到 appendonly  yes）

  

- **No persistence**: If you wish, you can disable persistence completely, if you want your data to just exist as long as the server is running.

  ​                只存在内存中，不写入到磁盘中

  

- **RDB + AOF**: It is possible to combine both AOF and RDB in the same instance. Notice that, in this case, when Redis restarts the AOF file will be used to reconstruct the original dataset since it is guaranteed to be the most complete.    ( aof-use-rdb-preamble yes)

​        可以在同一实例中组合AOF和RDB。注意，在这种情况下，当Redis重新启动时，AOF文件将用于重建原始数据集，因为它保证是最完整的

  **混合持久化优点：**

- 混合持久化结合了 RDB 和 AOF 持久化的优点，开头为 RDB 的格式，使得 Redis 可以更快的启动，同时结合 AOF 的优点，有减低了大量数据丢失的风险。

**混合持久化缺点：**

- AOF 文件中添加了 RDB 格式的内容，使得 AOF 文件的可读性变得很差；
- 兼容性差，如果开启混合持久化，那么此混合持久化 AOF 文件，就不能用在 Redis 4.0 之前版本了。



## 7.2、 RDB和AOF的区别？

​    RDB 对磁盘的IO操作比较少的，但是可能存在某个时间点的数据丢失

​    AOF  数据几乎不会被丢失，但对IO操作频率高，会导致读取数据效率变低



# 8、Redis过期删除策略 (背)

## 8.1、2种淘汰机制

   定期删除

   惰性删除



## 8.2、定期删除

  指为redis中的某个删除设定一个过期时间, 到了时间，redis自动的删除键

 

redis 会将每个设置了过期时间的 key 放入到一个独立的字典中，以后会定期遍历这个字典来删除到期的 key。



Redis 默认会每秒进行十次（配置文件 hz 10）过期扫描（100ms一次），过期扫描不会遍历过期字典中所有的 key，而是采用了一种简单的贪心策略。

1.从过期字典中随机 20 个 key；

2.删除这 20 个 key 中已经过期的 key；

3.如果过期的 key 比率超过 1/4，那就重复步骤 1；

redis默认是每隔 100ms就随机抽取一些设置了过期时间的key，检查其是否过期，如果过期就删除。注意这里是随机抽取的。为什么要随机呢？

你想一想假如 redis 存了几十万个 key ，每隔100ms就遍历所有的设置过期时间的 key 的话，就会给 CPU 带来很大的负载。


 ## 8.3、 惰性删除

所谓惰性策略    就是在客户端访问这个key的时候，redis对key的过期时间进行检查，如果过期了就立即删除，不会给你返回任何东西



## 8.4、为何要配置2种删除策略

 定期删除可能会导致很多过期key到了时间并没有被删除掉。所以就有了惰性删除。假如你的过期 key，靠定期删除没有被删除掉，还停留在内存里，除非你的系统去查一下那个 key，才会被redis给删除掉。这就是所谓的惰性删除，即当你主动去查过期的key时,如果发现key过期了,就立即进行删除,不返回任何东西.

总结：定期删除是集中处理，惰性删除是零散处理。



## 9、Redis内存不足的淘汰机制算法（背）

Redis是基于内存key-value键值对的内存数据库，我们安装完数据库之后，内存往往会受到系统内存大小的限制，我们也可以配置redis能使用的最大的内存大小.

两种方式配置redis的内存：

- 通过配置文件修改

- 通过客户端修改

  

## 9.1、redis配置内存大小

maxmemory <bytes>         默认没有开启的

当对redis中文件大小做了限定的时候，就需要对redis中的数据要进行强制删除算法。



下面的写法均合法：

 maxmemory 1024000 

maxmemory 1GB 

maxmemory 1G 

maxmemory 1024KB 

maxmemory 1024K

 maxmemory 1024MB



## 9.2、redis内存不足删除数据策略

https://blog.csdn.net/crazymakercircle/article/details/115360829

maxmemory-policy noeviction      配置，默认未启用



 volatile-lru -> remove the key with an expire set using an LRU algorithm

​                        使用LRU（Least Recently Used，最近最少使用）算法，从redis中设置过过期时间的key中，选取最少使用的进行淘汰

 allkeys-lru -> remove any key according to the LRU algorithm

​                       使用LRU（Least Recently Used，最近最少使用）算法，从redis中选取使用最少的key进行淘汰 



 volatile-random -> remove a random key with an expire set

​                                 从redis中设置过过期时间的key，进行随机淘汰

 allkeys-random -> remove a random key, any key

​                                从所有key中进行随机淘汰

 volatile-ttl -> remove the key with the nearest expire time (minor TTL)

​                            从redis中选取即将过期的key，进行淘汰

 noeviction -> don't expire at all, just return an error on write operations

​                        该策略对于写请求不再提供服务，会直接返回错误，当然排除del等特殊操作，**redis默认是no-envicition**策略。



# 10、redis重启服务如何加载数据？

<img src="assets/8.png">



redis重启服务加载的详细情况：

1.AOF持久化开启且存在AOF文件时(appendonly.aof)，优先加载AOF文件。

2.AOF关闭或者AOF文件不存在时，加载RDB文件（dump.rdb）。

3.加载AOF/RDB文件成功后，Redis启动成功。

4.AOF/RDB文件存在错误时，Redis启动失败并打印错误信息



# 11、redis一些常见问题以及解决方案 (背)

缓存穿透   、缓存击穿 、   缓存雪崩



## 11.1、  什么是缓存穿透

<img src="assets/9.png">

缓存穿透，就是用户想要查询一个数据，在 redis 中查询不到，即没有在缓存中命中，那么就会直接去持久化的 mysql 中进行查询，发现也没有这个数据，那么本次查询就失败了。

当用户巨多的时候，查询缓存都没有查询到，那么这些全部都去查询持久化的 mysql 数据库，压力全部打到 mysql 上面，这就是缓存穿透


### 11.1.1、解决方案：

解决方案有一般有 2 种方式

1. 使用布隆过滤器
2. 缓存空的对象

### 11.1.2、使用布隆过滤器
布隆过滤器是一种数据结构，对所有可能查询到的参数都是以 hash 的方式存储，会先在控制层进行校验，不符合的话，则丢弃，这就避免了对底层存储系统的压力

<img src="assets/10.png">



布隆过滤器部署在 redis 的前面，去拦截数据，减少对 redis 的冲击，进而减小对 持久化层的冲击



### 11.1.3、缓存空的对象

缓存空对象，就是当我们在持久化的数据库中没有查询到我们期望的数据时，那么就返回一个空对象，并且将这个空对象缓存起来，再对其设置一个过期时间

那么之后再有访问这个对象的请求时，缓存直接访问空对象即可，这就可以保护持久化数据层，减少对他的冲击压力

<img src="assets/11.png">



通过上述缓存空对象的方式，貌似也能解决问题，但是使用持久下去，会发现 key 值对应的空对象越来越多，会出现下面 2 个问题

1)、非常多的空对象被缓存起来，那么对应就很多的 key 占用 内存空间，占用资源，内存压力直线上升
2)、如果空对象的过期时间到了，那么请求的压力还是会打到持久化数据库上面，这会影响数据的一致性业务



## 11.2 、**什么是缓存击穿**



<img src="assets/12.png">



出现缓存击穿的情况是数据量太大，或者是缓存过期了

当某个 key 在过期的瞬间，有大量的请求这个 key 的数据，这种数据是热点数据，由于在缓存过期的瞬间，请求会同时访问到持久化的数据库来查询数据，并且会将数据会写到缓存中，此时就会导致数据库瞬间的压力过大，导致击穿



此处可以理解    击穿和穿透的区别：

击穿，是一个 key 非常热点，大量的访问都打在这个 key 上面，在 key 失效的瞬间，所有请求打在数据库上，就打出一个洞，击穿了

而穿透更多的是访问的数据不存在的情况，大量的请求访问的都是不存在的数据(带有恶意攻击行为)



### 11.2.1、缓存击穿的解决方案

1)、将热点数据设置不过期，不设置过期时间，就不会出现热点 key 过期的瞬间造成问题

2）、加上分布式锁，保证对于每一个 key ，同时只有一个服务进行访问，其他的服务没有获取到锁，就不能访问 redis 的这个 key，那么就需要等待获取锁
这种方式，锁的压力就非常大了，访问 redis 前先去访问锁，相当于锁给 redis 挡了一层



## 11.3、 缓存雪崩

<img src="assets/13.png">

缓存雪崩就是在某一个时间段，缓存集中过期，或者 redis 宕机的情况会出现



例如：

在某些热点活动中，会设置某些商品在一个固定的时间内过期，那么在 redis 里面，这个固定的时间点，大量的 key 过期，这就导致在这个时间段 缓存失效了，

且大量的请求数据都打在了持久化数据库上面了，这就很难受，在这种压力波峰下，压力全部打在持久化数据库上，这会造成持久化数据库宕机(连接池保证数据库不会宕机)



上述的情况，key 集中过期问题还不是非常的痛，最痛的是 redis 宕机了，自然周期性的形成的波峰压力，咱们的持久化数据库还是能够顶得住压力的，偏偏是在 redis 异常宕机，一挂挂一片，这就很有可能将后方的持久化数据库全部打挂，这是毁灭性的压垮




## 13.2、缓存雪崩的解决方案：

### 13.2.1、将 redis 做成高可用的
​     搭建 redis 集群，异地多活，既然担心 redis 会挂，那么我们就多准备一些 redis ，做成主备，或者异地多活

### 13.2.2、限流降级
​    就是在缓存失效的时候，通过锁的方式来限制访问数据顺序，或者关掉一些不重要的服务，让资源和性能全力提供给我们的主要服务

### 13.2.3、做数据预热
数据预热就是咱们在正式要上线之前，咱们就先将需要访问的数据预先访问一次，这样就可以将大量要访问数据库的数据写到缓存中
这样就可以在即将发生的高并发访问数据前手动的触发并加载不同的 key ，且会设置不同的过期时间，主要是可以将缓存失效的事情均衡一些，这样就尽量避免掉大量的 key 集中过期的情况



加载1000千key到redis中，统一计过期时间为30分钟

加载1000千key到redis中，随机设计过期时间  （Random.nextInt(10)+20）



# 14、redis集群

## 14.1、主从模式

### 14.1.1、图

<img src="assets/1.jpg">

master  主服务器          slave1和slave2 代表从服务器   ,    主服务器可以进行读和写的操作， 一旦主服务器做写操作，就会把写的数据分发给从服务器，从服务器自己再把数据写入到本机的redis中



### 14.1.2、主从模式配置很简单，只需要在从节点配置主节点的ip和端口号即可。

​     slaveof <masterip> <masterport>               例如# slaveof 192.168.1.214 6379

​     masterauth <master-password>                  如果主服务器有密码，就要配置密码



### 14.1.3、主从模式的原理

启动**主从**节点的所有服务，查看日志即可以看到**主从**节点之间的服务连接。



从上面很容易就想到一个问题，既然主从复制，意味着master和slave的数据都是一样的，有数据冗余问题。



在程序设计上，为了高可用性和高性能，是允许有冗余存在的。这点希望大家在设计系统的时候要考虑进去，不用为公司节省这一点资源。



对于追求极致**用户体验**的产品，是绝对不允许有**宕机**存在的。



主从模式在很多系统设计时都会考虑，一个master挂在多个slave节点，当master服务宕机，会**选举**产生一个新的master节点，从而保证服务的高可用性。



### 14.1.4、主从模式的优点：

一旦 主节点宕机，**从节点** 作为 主节点 的 **备份** 可以随时顶上来。扩展 **主节点** 的 **读能力**，分担主节点读压力。高可用基石：

除了上述作用以外，主从复制还是哨兵模式和集群模式能够实施的基础，因此说主从复制是Redis高可用的基石。

也有相应的缺点，比如我刚提到的数据冗余问题：

一旦 **主节点宕机**，**从节点** 晋升成 **主节点**，同时需要修改 **应用方** 的 **主节点地址**，还需要命令所有 **从节点** 去 **复制** 新的主节点，整个过程需要 **人工干预**。**主节点** 的 **写能力** 受到 **单机的限制**。**主节点** 的 **存储能力** 受到 **单机的限制**。



## 14.2、redis搭建集群(多主多从 至少6个redis实例)



docker create --name redis-node1 --net host -v /home/hadoop/data/redis-data/node1:/data redis:6.2.4 --cluster-enabled yes --cluster-config-file nodes-node-1.conf --port 6379  

docker create --name redis-node2 --net host -v /home/hadoop/data/redis-data/node2:/data redis:6.2.4 --cluster-enabled yes --cluster-config-file nodes-node-2.conf --port 6380  

docker create --name redis-node3 --net host -v /home/hadoop/data/redis-data/node3:/data redis:6.2.4 --cluster-enabled yes --cluster-config-file nodes-node-3.conf --port 6381 

docker create --name redis-node4 --net host -v /home/hadoop/data/redis-data/node4:/data redis:6.2.4 --cluster-enabled yes --cluster-config-file nodes-node-4.conf --port 6382 

docker create --name redis-node5 --net host -v /home/hadoop/data/redis-data/node5:/data redis:6.2.4 --cluster-enabled yes --cluster-config-file nodes-node-5.conf --port 6383 

docker create --name redis-node6 --net host -v /home/hadoop/data/redis-data/node6:/data redis:6.2.4 --cluster-enabled yes --cluster-config-file nodes-node-6.conf --port 6384 





进入到主节点   redis-node1
redis-cli --cluster create 192.168.26.180:6379 192.168.26.180:6380 192.168.26.180:6381  192.168.26.180:6382 192.168.26.180:6383 192.168.26.180:6384 --cluster-replicas 1

检查集群信息
root@CentOS7:/data# redis-cli  
127.0.0.1:6379> cluster nodes



搭建集群，你必须要有6个redis，一主一从     cluster-replicas 1       一个master必须要有一个slave



## 14.3、搭建主机模式（一主从多 2台redis）

可以主服务器进行读和写的操作，   只允许对从服务器进读操作，

如果对从服务器进行写操作报这个异常  "READONLY You can't write against a read only replica."

<img src="assets/14.png">



复制2份redis.conf    命名为 redis01.conf           redis02.conf

| 名称                       | 配置                                                         |
| -------------------------- | ------------------------------------------------------------ |
| redis01.conf    (主服务器) | port=6379<br />requirepass=hxzy<br />                        |
| redis02.conf (从服务器)    | port=6380<br />requirepass=hxzy<br />replicaof    主服器IP<masterip> <masterport>主服务器端口<br />masterauth <master-password>  主服务器密码 |
|                            |                                                              |

启动2个redis实例

<img src="assets/15.png">



## 14.4、哨兵模式 （一主二从三哨兵）

### 14.4.1、哨兵模式概念（背）

哨兵模式是redis`高可用`的实现方式之一

 使用一个或者多个哨兵(Sentinel)实例组成的系统，对redis节点进行监控，在主节点出现故障的情况下，能将从节点中的一个升级为主节点，进行故障转义，保证系统的可用性。

<img src="assets/16.png">



### 14.4.2、哨兵们是怎么感知整个系统中的所有节点(主节点/从节点/哨兵节点)的

1）、首先主节点的信息是配置在哨兵(Sentinel)的配置文件中         sentinel.conf

2）、哨兵节点会和配置的主节点建立起两条连接             `命令连接     `和       `订阅连接`

3)、哨兵会通过`命令连接`每10s发送一次`INFO repliacation`命令，通过`INFO命令`，主节点会返回自己的run_id和自己的`从节点信息`

4)、哨兵会对这些从节点也建立两条连接`命令连接`和`订阅连接`

5)、哨兵通过`命令连接`向从节点发送`INFO`命令，获取到他的一些信息
 a. run_id
 b. role
 c. 从服务器的复制偏移量 offset
 d. 等

6)、因为哨兵对与集群中的其他节点(主从节点)当前都有两条连接，`命令连接`和`订阅连接`
 a. 通过`命令连接`向服务器的`_sentinel:hello`频道发送一条消息，内容包括自己的ip端口、run_id、配置纪元(后续投票的时候会用到)等
 b. 通过`订阅连接`对服务器的`_sentinel:hello`频道做了监听，所以所有的向该频道发送的哨兵的消息都能被接受到
 c. 解析监听到的消息，进行分析提取，就可以知道还有那些别的哨兵服务节点也在监听这些主从节点了，更新结构体将这些哨兵节点记录下来
 d. 向观察到的其他的哨兵节点建立`命令连接`----没有`订阅连接`



### 14.4.3、哨兵工作模型图

<img src="assets/17.png">



为何必须要有3个哨兵，    

<img src="assets/18.png">           redis中发送命令：    info  replication





当某个哨兵监听到master主服务器没有响应的时间（默认时间30s），哨兵就要去确认是否真的master失效了

sentinel down-after-milliseconds <master-name> <milliseconds>      默认是30秒   

 

<img src="assets/19.png">

哨兵就会推选另一个slave为master  ,这种叫做  选举制度，要求必须是slave个数=2n+1       (redis满足，哨兵也要满足)

<img src="assets/20.png">



哨兵与哨兵之间   通过注册服务进行观察 (订阅/发布)     设计叫观察者模式

<img src="assets/21.png">

搭建了一主二从三哨兵的模型时候，我们只能对哨兵进行连接，在哨兵里面进行redis操作









### 14.4.3、修改3个redis的config和三个哨兵的配置文件信息

1）、三个redis配置信息

  ```java
主(master)服务器  (可读可写)       redis01.conf
    port    6379
    requirepass   hxzy
   

从(slave01)服务器 (只能读)     redis02.conf
    port    6380
    requirepass   hxzy
    slaveof <masterip>   6379
    masterauth  hxzy
   

从(slave02)服务器 (只能读)     redis03.conf
    port    6381
    requirepass   hxzy
  
    slaveof <masterip>   6379
    masterauth  hxzy   
  
  ```

启动  三个redis     

./bin/redis-server     ./redis01.conf

./bin/redis-server     ./redis02.conf

./bin/redis-server     ./redis03.conf

通过RDM进入到 主redis中，输入命令观察状态    info     replication

通过RDM进入到 子redis1中，输入命令观察状态    info     replication

通过RDM进入到 子redis2中，输入命令观察状态    info     replication



2）、三个哨兵

```java
哨兵1  (sentinel01.conf)
port 26379
daemonize yes                                           修改了，后台运行
sentinel monitor mymaster 192.168.26.180 6379  2         主redis的ip和端口,2个哨兵同时监听到主服务不可用了，就要进行选举
sentinel auth-pass mymaster hxzy                       主redis连接密码

sentinel down-after-milliseconds <master-name> <milliseconds>        哨兵监听主服务器ping方式确认主redis是否下线  (以下都是可选项)
sentinel sentinel-user <username>                                    连接哨兵的用户名
sentinel sentinel-pass <password>                                    连接哨兵的密码 
sentinel notification-script mymaster /var/redis/notify.sh           当maser下线了，自己写一个脚本，发送邮件给管理员
                                                     notify.sh   写  curl     自己的项目/send/email/notify_redis_down
    
    
哨兵2 (sentinel02.conf)
port 26380
daemonize yes                                           修改了，后台运行
sentinel monitor mymaster 192.168.26.180 6379  2         主redis的ip和端口,2个哨兵同时监听到主服务不可用了，就要进行选举
sentinel auth-pass mymaster hxzy                       主redis连接密码

哨兵3 (sentinel03.conf)
port 26381
daemonize yes                                           修改了，后台运行
sentinel monitor mymaster 192.168.26.180 6379  2         主redis的ip和端口,2个哨兵同时监听到主服务不可用了，就要进行选举
sentinel auth-pass mymaster hxzy                       主redis连接密码
```

启动  三个哨兵

./bin/redis-sentinel     ./sentinel01.conf

./bin/redis-sentinel     ./sentinel02.conf

./bin/redis-sentinel     ./sentinel03.conf



## 14.4.4、把master主服务结束掉

<img src="assets/22.png">



# 15、springboot中使用redis

## 15.1、导包

```java
  <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>         
        </dependency>
```



## 15.2、配置

单机版

```java
spring.redis.host=redis的ip
spring.redis.port=redis的port
spring.redis.password=redis的密码
spring.redis.database=redis要使用的哪一个数据库索引（0-15）

```



哨兵版

```java
spring:
  redis:
    sentinel:
      master: mymaster         哨兵配置的名称
      nodes: 192.168.26.180:26379, 192.168.26.180:26380, 192.168.26.180:26381
      password: hxzy              #连接哨兵的密码
    password: hxzy       # 连接redis的密码(一定要配置盘，不然连接不上)
```



## 15.3、使用RedisTemplate模板操作redis

<img src="assets/23.png">



### 15.3.1、RedisOperations接口

| 方法名                                          | 说明                                                |
| ----------------------------------------------- | --------------------------------------------------- |
| ValueOperations<K, V> opsForValue();            | 对Redis中的String数据类型封装(包含了bitmap位图操作) |
| ListOperations<K, V> opsForList()               | 对Redis中的List数据类型封装                         |
| <HK, HV> HashOperations<K, HK, HV> opsForHash() | 对Redis中的Hash数据类型封装                         |
| SetOperations<K, V> opsForSet()                 | 对Redis中的Set数据类型封装                          |
| ZSetOperations<K, V> opsForZSet()               | 对Redis中的ZSet数据类型封装                         |
| GeoOperations<K, V> opsForGeo()                 | 对Redis中的GEO地址位置数据类型封装                  |
| HyperLogLogOperations<K, V> opsForHyperLogLog() | 对Redis中的HyperLogLog数据类型封装                  |
| delete(),  expired()                            | 对键的操作                                          |



### 15.3.2、RedisTempalte类对RedisOperations接口实现

这个RedisTemplate所有存储(key,value) 都是以java序列化和反序列化操作

```java
JdkSerializationRedisSerializer
    
    SerializingConverter  序列化   （把java数据类型转换成二进制流过程）
         
    DeserializingConverter 反序化   （把二进制流 转换成 java 对象的过程）
```



java序列化

```java
  //存放到内存 字节流中
  ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
  
   //序列化的代码
    ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
    objectOutputStream.writeObject(java可以序列化的对象);       这个类有什么标识implements Serializable 
    objectOutputStream.flush();  
  
  byte[]  out.toByteArray();
```



java反序列化

```java
 
 ByteArrayInputStream byteStream = new ByteArrayInputStream(byte[] 文件流);
 
 ObjectInputputStream objectInputStream = new ObjectInputputStream(byteStream);
 Object  obj= objectInputStream.readObject();
   
```

如果使用默认JDK序列化，我们在redis的可视化管理软件中，你就不能看到任何你能看懂的数据

有时候，我需要使用  json序列化和反序列来代替 jdk序列化和反序列化



StringRedisTemplate默认的 所有的key都是以String来存储，所有的Value都是以String的方式来存储，不允许你存储对象Object



### 15.3.3、配置一个json序列化的反序列化的RedisTempalte模板

导包

```java
      <!-- JSON工具类 -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </dependency>

        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.79</version>
        </dependency>
```



编写自定义使用fastjson序列化和反序列的转换器

```java
public class FastJson2JsonRedisSerializer<T> implements RedisSerializer<T>
{
    @SuppressWarnings("unused")
    private ObjectMapper objectMapper = new ObjectMapper();

    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    private Class<T> clazz;

    static
    {

        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
    }

    public FastJson2JsonRedisSerializer(Class<T> clazz)
    {
        super();
        this.clazz = clazz;
    }

    /**
     * 序列化(把java对象转换成json字符串，流)
     * @param t
     * @return
     * @throws SerializationException
     */
    @Override
    public byte[] serialize(T t) throws SerializationException
    {
        if (t == null)
        {
            return new byte[0];
        }
        return JSON.toJSONString(t, SerializerFeature.WriteClassName).getBytes(DEFAULT_CHARSET);
    }

    /**
     * 反序列化
     * @param bytes
     * @return
     * @throws SerializationException
     */
    @Override
    public T deserialize(byte[] bytes) throws SerializationException
    {
        if (bytes == null || bytes.length <= 0)
        {
            return null;
        }
        String str = new String(bytes, DEFAULT_CHARSET);

        return JSON.parseObject(str, clazz);
    }

    public void setObjectMapper(ObjectMapper objectMapper)
    {
        Assert.notNull(objectMapper, "'objectMapper' must not be null");
        this.objectMapper = objectMapper;
    }

    protected JavaType getJavaType(Class<?> clazz)
    {
        return TypeFactory.defaultInstance().constructType(clazz);
    }
}
```

再配置redisTemplate模板

```java
@Configuration
public class RedisTemplateConfig {

    @Bean
    @SuppressWarnings(value = { "unchecked", "rawtypes" })
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory connectionFactory)
    {
        RedisTemplate<Object, Object> template = new RedisTemplate<>();
        
        template.setConnectionFactory(connectionFactory);

        // 使用fastjson的自定义序列化和反序列化的类
        FastJson2JsonRedisSerializer serializer = new FastJson2JsonRedisSerializer(Object.class);

        //jackson2框架 映射对象，怎么个序列法
        ObjectMapper mapper = new ObjectMapper();
       
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);

        serializer.setObjectMapper(mapper);

        // 使用StringRedisSerializer来序列化和反序列化redis的key值
        template.setKeySerializer(new StringRedisSerializer());
        
        template.setValueSerializer(serializer);

        // Hash的key也采用StringRedisSerializer的序列化方式
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(serializer);

        template.afterPropertiesSet();
        return template;
    }

}
```



### 15.3.4、测试

```java
@SpringBootTest
class RedisApplicationTests {


    @Autowired
    private RedisTemplate  redisTemplate;


    @Test
    public void testStringAdd(){

        //set赋值
        redisTemplate.opsForValue().set("abcd","123");

        //取值
        String value= (String) redisTemplate.opsForValue().get("abcd");

        System.out.println(value);

        //添加一个过期时时间(5秒)
      redisTemplate.expire("abcd",50, TimeUnit.SECONDS);
    }
}
```



# 16、配置springboot测试环境和生产环境 

测试环境，直接可以配置自己的redis,自己的数据库

生产环境，配置服务器redis哨兵以及服务器的数据库

## 16.1、什么是 profile

配置profile作用：

​    我们在开发Spring Boot应用时，通常同一套程序会被安装到不同环境，比如:开发、测试、生产等。其中数据库地址、服务器端口等等配置都不同，如果每次打包时，都要修改配置文件，那么非常麻烦。profile功能就是来进行动态配置切换的

1)、profile配置方法
            多profile文件方式   (推荐使用)

​                        application.yml      (主要配置端口，spring.application.name名称)

​                       application-dev.yml          开发环境(给开发程序员用的)

​                       application-prod.yml        生产环境（线上服务器)

​                       application-test.yml           给测试人员使用 



​           yml多文档方式

​                 不要阅读，不建议使用

```
---
server:
  prot: 8081
  
spring:
  profiles: dev
---
server:
  prot: 8082
  
spring:
  profiles: test
---
server:
  prot: 8083
  
spring:
  profiles: pro
```





2)、profile激活方法
      配置文件

​            application.yml中     

​                spring: 

​                      profiles:    

​                               active: dev 

​      虚拟机参数 在VM options指定: -Dspring.profiles.active=dev

​      命令行参数 java-jar xxx.jar --spring.profiles.active=dev


# 17、redis的项目中的实际案例

## 17.1、把 other表数据以 table_name为key，存放到redis中

​       key=" other:"+ table_name名       value=List<Other>

​     只要用到了 other表的地方，都需要进行缓存读取

​          other新增，对缓存新增，other修改，对缓存进行修改，other删除,对缓存进行删除   达到   缓存同步的状态

​        手动 清空所other所有缓存，手动加载other所有缓存操作

 

## 17.2、 jdk 1.8 的集合的Stream

<img src="assets/24.png">

```java
@Component
public class RedisCacheUtil {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * other业务逻辑
     */
    @Autowired
    private OtherService  otherService;

    private Lock   lock=new ReentrantLock(true);

    /**
     * 初始化other表的数据
     */
    public void other_loadAllData(){
        //查询所有的数据库
        List<Other> allList = this.otherService.list();

        //对allList进行分组，按照table_name来分组  返回就是这个数据结构 Map<String,List<Other>>
        //JDK 1.8 Stream增强流
        Map<String, List<Other>> collect = allList.stream().collect(Collectors.groupingBy(Other::getTableName));

        //循环遍历map
        for(String key : collect.keySet()){
            String redisKey= RedisConstans.getRedisKey( RedisConstans.OTHER_KEY, key);
            List<Other> redisValue=collect.get(key);
            // 缓存永不过期
            this.redisTemplate.opsForValue().set(redisKey,redisValue);
        }
    }

    /**
     * 清空redis中other表的数据
     */
    public void other_clearAllData(){
        //1、查询redis中以 other:开头的所有的key
        Set keys = this.redisTemplate.keys(RedisConstans.OTHER_KEY + "*");
        for(Object key :keys){
            this.redisTemplate.delete(key);
        }
    }

    /**
     * 根据redis中other缓存查询数据
     * @param tableName
     * @return
     */
    public List<Other>  other_getByTableName(String tableName) throws InterruptedException {
        String redisKey = RedisConstans.getRedisKey(RedisConstans.OTHER_KEY, tableName);
        Object redisValue = this.redisTemplate.opsForValue().get(redisKey);
        if (redisValue == null) {
            try {
                //尝试加锁1秒
                lock.tryLock(500, TimeUnit.MILLISECONDS);
                // 还需再一次查询redis中是否存在值
                redisValue = this.redisTemplate.opsForValue().get(redisKey);
                if (redisValue == null) {
                    // 查询数据库
                    List<Other> dbList = this.otherService.findByTableName(tableName);
                    //存入到redis中
                    this.redisTemplate.opsForValue().set(redisKey, dbList);
                    return dbList;
                }
                //redis已有缓存了
                return (List<Other>) redisValue;

            }  finally {
                lock.unlock();
            }
        }
        return (List<Other>) redisValue;
    }

}
```



## 17.2、程序启动的时候，把数据库某些值加载到redis的缓存中，这个专业名称叫做   数据预热

使用 spring框架的一个注解  @PostConstruct

```java
@Configuration
public class InitDataPostConstruct {

    @Autowired
    private RedisCacheUtil redisCacheUtil;

    @PostConstruct
    public void initData(){
        System.out.println("-----程序启动，调用了InitDataPostConstruct中的initData方法");
        this.redisCacheUtil.other_loadAllData();
    }
}
```



## 17.3、项目中如何解决类的循环引用的问题

https://blog.csdn.net/m0_46420244/article/details/126215891

<img src="assets/25.png">



可以使用代理来解决类的循环引用(依赖)的问题

<img src="assets/26.png">

## 17.4、redis来改造验证码的令牌存储

以前验证码和令牌使用数据库来存，需要使用 定时任务来定时清除数据库无效的值 (定时删除，惰性删除)

```java
 @ApiOperation(value = "算术运算图片验证码")
    @GetMapping(value = "airth")
    public R airth(@RequestParam(value = "width",required = false,defaultValue = "150") int width,
                 @RequestParam(value = "height",required = false,defaultValue = "50") int height,
                 @RequestParam(value = "len",required = false,defaultValue = "2") int len) {

        ArithmeticCaptcha captcha = new ArithmeticCaptcha(width, height);
        captcha.setLen(len);
        //返回结果
        String text = captcha.text();
        String img = captcha.toBase64();
        String uuid = IdUtil.fastUUID();

//        // 把图片验证码保存到某个地方（数据库）
//        CaptchaInfo  captchaInfo=new CaptchaInfo();
//        captchaInfo.setUuid(uuid);
//        captchaInfo.setCaptchaValue(text);
//        captchaInfo.setCreateTime(new Date());
//        //设定5分钟过期
//        captchaInfo.setExpiredTime(DateUtil.offsetMinute( captchaInfo.getCreateTime(), 5));
//        boolean save = this.captchaInfoService.save(captchaInfo);

        String redisKey = RedisConstans.getRedisKey(RedisConstans.CAPTCHA_KEY, uuid);
        this.redisTemplate.opsForValue().set(redisKey, text, expired, TimeUnit.MINUTES);

        //返回给前端
        Map<String, Object> data = new HashMap<>();
        data.put("img", img);
        data.put("uuid", uuid);
        return R.okHasData(data);
    }
```

删除之前的定时任务，定时删除过期的验证码的代码

````java
    @Async
    @Scheduled(cron = "0 0/30 * * * ? ")
    public void removeExpiredCaptcha(){
      this.captchaInfoService.removeExpiredCaptcha();
    }
````



登录令牌的改造，之前是写入到数据库 Tokens表中,然后使用定时任务去删除过期时间

```java
 @Override
    public String generatorToken(Role role) throws JsonProcessingException {

        String uuid= IdUtil.simpleUUID();

        //数据库存储值
        Tokens tokens=new Tokens();
        tokens.setRoleId(role.getRoleId());
        tokens.setUuid(uuid);

        //需要把 role 转换成  json(Jackson2框架)格式
        ObjectMapper  objectMapper=new ObjectMapper();
        String jsonValue=objectMapper.writeValueAsString(role);
        tokens.setJsonValue(jsonValue);
        tokens.setCreateTime(new Date());
        //令牌过期时间1小时
        tokens.setExpiredTime(DateUtil.offsetHour(tokens.getCreateTime(),1));
        boolean save = this.tokensService.save(tokens);
        if(!save){
            throw new ServiceException(AckCode.TOKEN_SAVE_ERROR);
        }

        //再次把 uuid使用jwt进行加密
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uuid",uuid);

        //使用jwt加密
        String token =this.jwtUtil.createToken(map);

        return token;
    }
```



使用redis改造登录存储令牌

```java
  @Override
    public String generatorToken(Role role) throws JsonProcessingException {

        String uuid= IdUtil.simpleUUID();

        //令牌的key
        String tokenRedisKey= RedisConstans.getRedisKey(RedisConstans.TOKEN_KEY,uuid);
        this.redisTemplate.opsForValue().set(tokenRedisKey,role,this.expired, TimeUnit.MINUTES);

        //需要记录当前令牌对应的值是谁    HASH来存储当前用户的登录令牌( field=id, value=uuid)
        String tokenUserIdRedisKey=RedisConstans.TOKEN_USER_KEY;
        this.redisTemplate.opsForHash().put(tokenUserIdRedisKey,role.getRoleId()+"",uuid);


        //再次把 uuid使用jwt进行加密
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("uuid",uuid);

        //使用jwt加密
        String token =this.jwtUtil.createToken(map);

        return token;
    }
```



登录拦截器的改造

```java
  @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String jwtToken=request.getHeader(this.headerName);
        // 没有令牌
        if(StrUtil.isBlank(jwtToken)){
            ServletUtils.renderString(response,R.build(AckCode.SYSTEM_TOKEN_FAIL));
            return false;
        }

        //有令牌，判断是否 有  持票人标识 Bearer 空格 令牌
        if(jwtToken.startsWith("Bearer")){
           jwtToken= jwtToken.replace("Bearer","").trim();
        }

        //判断令牌有效性 jwt解码
        DecodedJWT decodedJWT = jwtUtil.parseToken(jwtToken);
        if(decodedJWT==null){
            ServletUtils.renderString(response,R.build(AckCode.APPLE_ILLEGAL));
            return false;
        }



        //取得令牌里面存储的值

        Claim claim = decodedJWT.getClaim("value");
        Map<String, Object> stringObjectMap = claim.asMap();
        String uuid=stringObjectMap.get("uuid").toString();


//        //去数据库查询uuid是否正确、存在
//        Tokens byId = this.tokensService.findById(uuid);
//        if(byId==null){
//            ServletUtils.renderString(response,R.build(AckCode.APPLE_EXPIRED));
//            return false;
//        }

        // 使用redis来查询
        String redisKey= RedisConstans.getRedisKey(RedisConstans.TOKEN_KEY,uuid);
        Object redisValue=this.redisTemplate.opsForValue().get(redisKey);
        if(redisValue==null){
            ServletUtils.renderString(response,R.build(AckCode.APPLE_EXPIRED));
            return false;
        }

        // 把这个信息转换为用户信息
        TokenUserVO byId= (TokenUserVO) redisValue;

        // 当前用户保存到本地线程池，以便后续方法调用
        AbortionThreadLocation.roleThreadLocal.set(byId);

        return true;
    }

    /**
     * 程序执行完Controller之后要做事情
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
      AbortionThreadLocation.roleThreadLocal.remove();
    }
```





