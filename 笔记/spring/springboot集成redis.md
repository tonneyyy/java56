# springboot集成redis

## 1、导入依赖的包

```
implementation 'org.springframework.boot:spring-boot-starter-data-redis'
implementation 'org.apache.commons:commons-pool2'
```

commons-pool2 连接池



## 2、配置

```
srping:
  redis:
    host: localhost
    port: 6379
    password: hxzy
    database: 1
```



## 3、spring集成redis中默认有2个模板

  <h3>
      RedisTemplate<K,V><br>
      StringRedisTemplate extends RedisTemplate<String, String>
  </h3>

RedisTemplate<Object,Object>  键是二进制数据，值也是二进制数据   JAVA序列化的数据JdkSerializationRedisSerializer

java序列化的语法

```
 Stock  stock=new Stock();
        stock.setSid(1);
        stock.setId(100);
        stock.setPrice(10.5F);
        //序列化对象
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("d:\\1.txt"));
        objectOutputStream.writeObject(stock);
        objectOutputStream.flush();
```

<img src="14.png">

java反序列化

```
 //反序列化对象
  ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("d:\\1.txt"));
        try {
            Object obj= objectInputStream.readObject();
            Stock stock= (Stock) obj;
            System.out.println(stock.toString());
        }
        catch (ClassNotFoundException ex) {
            throw new NestedIOException("Failed to deserialize object type", ex);
        }
```

有些公司为了更人性化，会自定义RedisTemplate，把二进制的数据，换为json序列化.

阿里帮你实现 FastJsonRedisSerializer    

Jackson2[springmvc默认的]帮你实现 Jackson2JsonRedisSerializer



## 4、使用fastjson+jackson2代表jdk二进制序列化

[](FastJson2JsonRedisSerializer.java)



[](RedisConfig.java)



## 5、使用redis的 lua脚本来设定对redis读取的次数的限定

对redis操作每秒频率，把它叫做 限流。

```
/**
 *
 *  针对某个key使用lua脚本进行限流
 *  使用lua优点，可以保证多个命令是一次行传输到Redis服务器并且是串行执行的，保证串行执行的命令中不行插入其他命令，防止并发问题
 *  步骤：
 *   1、判断key是否存在，如果不存在，保存该key，设置值为1，设置多少毫秒（n）最多进行几次（m)
 *   2、如果值存在，先判断key是否超时了，如果超时则删除，并重新执行步骤1，如果key未超时，则判断是否超过n毫秒最多m次的限制
 *
 * 限流脚本
 */
private String limitScriptText()
{
    my   10  100  100毫秒内，my键只能允许访问10次
    
    return "local key = KEYS[1]\n" +
            "local count = tonumber(ARGV[1])\n" +
            "local time = tonumber(ARGV[2])\n" +
            "local current = redis.call('get', key);\n" +
            "if current and tonumber(current) > count then\n" +
            "    return tonumber(current);\n" +
            "end\n" +
            "current = redis.call('incr', key)\n" +
            "if tonumber(current) == 1 then\n" +
            "    redis.call('expire', key, time)\n" +
            "end\n" +
            "return tonumber(current);";
}
```



## 6、spring aop前置增强对控制器实现限流操作

```
@Component
@Aspect
public class RedisLimitAspect {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 切入表达式
     */
    @Pointcut(value = "@annotation(com.hxzy.annon.RedisLimit)")
    private void redisLimitExecution(){
    }

    @Autowired
    private DefaultRedisScript  redisScript;

    /**
     * 前置增强
     * @param joinPoint
     * @param redisLimit
     */
    @Before(value = "redisLimitExecution() && @annotation(redisLimit)")
    public void beofreAdvice(JoinPoint joinPoint, RedisLimit redisLimit){
       String key = "redis_limit:"+redisLimit.key();
       //限流次数
       int limitCount=redisLimit.count();
       //限定时间毫秒
        long limitTime=redisLimit.time();
        Long result= (Long)this.redisTemplate.execute(redisScript, Arrays.asList(key),limitCount,limitTime);
        //超过了限流次数了
        if(result.intValue()>limitCount){
            throw new LimitException();
        }
    } 
}
```

## 7、使用

```
@RedisLimit(key="StockSearchData",count = 1,time = 10000)
@ResponseBody
@GetMapping(value = "/stock/data")
public ResultAjax searchData(){
    IPage page = this.stockService.search(super.startPage());
    return ResultAjax.success_page(page);
}
```



## 8、希望对异常不报500错误，返回自定义json

[使用springmvc提供给我们的全局异常处理](https://www.cnblogs.com/hfultrastrong/p/10488280.html)



[@RestControllerAdvice+ @ ExceptionHandler 注解](https://www.cnblogs.com/junzi2099/p/7840294.html#_label2)

```
@Log4j2
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = LimitException.class)
    public ResultAjax redisLimitException(LimitException e){
        //写入到日志文件(给你们开发人员调试用的)
        log.error(e);
        //给前端用户看
       return  ResultAjax.error(500,e.getMessage());
    } 
}
```







