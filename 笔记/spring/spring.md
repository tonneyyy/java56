# 一、Spring 介绍

## 1、 什么是 Spring

Spring 被描述为构建 Java 应用程序的轻量级(快速开发)框架。

可以使用 Spring Java 中的任何应用程序(桌面应用程序，web网站).

只需要对应用程序代码进行很少的更改（如果有的话）就可以获得 Spring Core 所带来的好处.

https://spring.io/

Spring程序员开发的主流框架。 

2002年spring发布第一个版本, 使用的控制反转(IoC）容器的框架 和AOP面向切面编程. 用于构建轻量级的JAVA应用程序( JAVAEE).



# 2、Spring1.X的核心模型(七大)

1.  Spring Core:   bean 容器以及支持的实用程序

2. Spring Context :  ApplicationContext(全局管理[上下文])、验证，邮件处理，远程处理

3.  Spring DAO ：事务基础结构、 Java Database Connectivity(JDBC)和数据访问对象（DAO）支持

4. Spring ORM :  ORM（Object Relational Mapping ）  java对象与数据库表进行映射关系的框架，    框架支持  (mybatis,hibernate,spring-data)

5. Spring AOP :  面向切(方)面编程 【学习重点：面向对象编程+自定注解+反射+动态代理】

6. Spring Web:  servlet的框架，简化servlet操作，支持第三方框架(struts2)

7. Spring Web Mvc:   MVC设计模型框架，优化Servlet操作

   
   
   <img src="15.png">
   
   

# 3、spring 2.5.x以注解方式来代替XML配置

1. @Component[组件]、@Repository[数据访问层组件]、@Service[业务逻辑层组件]、 @Controller [控制器Servlet层组件]

2. ＠Autowired 自动装配 以及对 JSR-250 注解（@Resource 、＠PostConstruct 和＠PreDestroy）的支持

3. Spring MVC @Controller 注解，还添加了＠RequestMapping @RequestParam和@Mode!Attribute

4. 自动类路径扫描支持@ComponentScan，可以检测和连接带有构造型注解的类

5. 引入了 SpringTestContextframework，提供注解驱动和集成测试支持

6. AOP 更新，包括一个新的 bean 切入点元素以及 @AspectJ 加载时织入（weaving)

   现目前Spring主流版本是5.X，功能更强悍

   

   ## 二、spring framework最核心2个功能

        <h3 style="color:red;"> 控制反转(IoC)或依赖注入(DI)   、  AOP 面向切面编程</h3> 

   <h3 style="color:red;"> 控制反转(IoC)或依赖注入(DI)   、  AOP 面向分(切)面编程</h3>

   

   IoC 种将组件Bean（类 @Componen、@Repository、@Service、 @Controller、@Configuartion）依赖项的  创建和管理    外化(容器)  的技术.

   

   

   ## 1、 控制反转 IoC

   ​      原始的编程方式

   ![](1.png)

   上面代码看起来没有任何问题。它经不起需求变更。

   UserInfoSearchServlet  强依赖 于 UserInfoServiceImpl。想办法把它解耦.

   

   有的spring 控制反转以后上面图就变成下面这样

   ![](2.png)

   你写的类还是这样么，只不过，类与类之间强耦合关系，不存在了。

   

   ## 2、第一个IoC案例(三层架构为例)

   User表为它实现三层架构(原始方式)

      多个类中都出现new关键字，它是强耦合代表。一次性软件产品。
   
   
   
   User表为它实现三层架构(Spring IoC方式)
   
   ```
   
   <dependency>
       <groupId>org.springframework</groupId>
       <artifactId>spring-context</artifactId>
       <version>5.3.14</version>
   </dependency>
    
   <dependency>
       <groupId>org.springframework</groupId>
       <artifactId>spring-beans</artifactId>
       <version>5.3.14</version>
   </dependency>
    
   <dependency>
       <groupId>org.springframework</groupId>
    <artifactId>spring-core</artifactId>
       <version>5.3.14</version>
</dependency>
   

   ```

   

   ## 3、spring ioc注解方式

    1、构造函数注入  （推荐）

    2、以setter方法注入

   
   
   ## 4、spring ioc管理类实例

   单例模式(singletone)， 该类在内存中是唯一的存在

   ```
<bean id="userHxzyDao" class="com.hxzy.dao.impl.UserOralceDaoImpl"></bean>
   ```
   
    

   原型模式(prototype)    单例(singletone)，每一次取出来类都是一个新的对象

   ```
<bean id="userHxzyDao" class="com.hxzy.dao.impl.UserOralceDaoImpl" scope="prototype"></bean>
   ```
   
   ## 5、spring ico可以把类做成延迟加载

   lazy-init=true 延迟加载，在使用的时候才去读取, 默认是立即加载

   ```
<bean id="userService" class="com.hxzy.service.impl.UserServiceImpl" lazy-init="true">
   ```
   
   ## 6、 spring ioc对属性赋值2种操作
   
   值类型    value  (int,double,String,char,boolean,long)

   ```
   <bean id="userHxzyDao" class="com.hxzy.dao.impl.UserOralceDaoImpl" scope="prototype">
       <property  name="set方法"  value="值">
   </bean>
   ```
   
   引用类型： ref

```
<bean id="userHxzyDao" class="com.hxzy.dao.impl.UserOralceDaoImpl" scope="prototype">
    <property  name="set方法"  ref="定义的bean的id">
</bean>
```

## 7、spring ioc基于注解配置(重点)

 1、开启注解， 以自动扫描包

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd">

    <!-- 开启注解 -->
    <context:annotation-config></context:annotation-config>

    <!-- 扫描包 -->
    <context:component-scan base-package="com.hxzy"></context:component-scan>

</beans>
```

2、在你类中加入以下几个注解

```
@Component
@Repository   继承@Component，意思是告诉开发者这个一个数据访问的实现类
@Service      继承@Component，意思是告诉开发者这个一个业务逻辑的实现类
@Controller   承@Component，意思是告诉开发者这个一个Servlet的实现类
```

当你构造函数中，接口有多少实现的时候，必须要指定名称

@Qualifier(value = "bean的id") 

```
 public UserServiceImpl( @Qualifier(value = "userOralceDaoImpl") UserDao userDao) {
        this.userDao = userDao;
    }
```

如果不使用以上的注解筛选，会出现以下错误

![](3.png)



如果一个接口下面有多个实现类的时候，我们可以定认一个默认的主类   @Primary

 

```
@Primary
@Repository
public class UserMySqlDaoImpl  implements UserDao{

}

@Repository
public class UserOracleDaoImpl  implements UserDao{

}



```



引用spring-test测试框架

```java
  <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>5.3.20</version>
        </dependency>
       <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.1</version>
        </dependency>
```



```java
@RunWith(value = SpringJUnit4ClassRunner.class)
//加载哪一个spring配置文件
//ApplicationContext  ac=new ClassPathXmlApplicationContext("classpath:spring-annon2.xml");
@ContextConfiguration(locations = "classpath:spring-annon2.xml")
public class Demo03Text {

    //相当于是   ac.getBean(name=mySqlUserService, 类型UserService.class);
    @Qualifier(value = "mySqlUserService")
    @Autowired
    private UserService  userService;

    //可以这样写法 与上面的写法是等价的
    @Autowired
    private UserService  oracleUserService;


    @Test
    public void test01(){

      this.userService.insert();

      this.oracleUserService.insert();
    }
}
```







3、spring ioc 自动装配Bean5种方式(必背)

1. ​       byName 方式：  使用该模式进行自动装配时， Spring 自动尝试找到与变量名相同的名称进行注入 。

   ```
   @Repository
   public class UserMySqlDaoImpl implements UserDao 
   
   @Repository
   public class UserOralceDaoImpl implements UserDao 
   
   进行UserServiceImpl使用构造函数注入的时候  
   @Service
   public class UserServiceImpl implements UserService {
   
       private UserDao  userDao=null;
       
       // (成功的帮你注解  userOralceDaoImpl实现类 )
       public UserServiceImpl(   UserDao userOralceDaoImpl) {
           this.userDao = userOralceDaoImpl;
       }
    }
       
    
    进行UserServiceImpl使用构造函数注入的时候   
   @Service
   public class UserServiceImpl implements UserService {
   
       private UserDao  userDao=null;
        
        // (报错，  找到了2个相似的类型 userMySqlDaoImpl，  userOralceDaoImpl)
       public UserServiceImpl( UserDao userDao) {
           this.userDao = userOralceDaoImpl;
       }
    }
   ```

   

2. byType 模式：  使用该模式进行自动装配时，找到相同的类型来匹配

   **这种模式可能会现异常，当一个接口有多个实现类的时候，可以使用@Qualifier(value = "bean的id")**      @Primary

   ```
   @Repository
   public class UserDaoImpl implements UserDao 
   
   @Repository
   public class UserOralceDaoImpl implements UserDao 
   
   进行UserServiceImpl使用构造函数注入的时候
   @Service
   public class UserServiceImpl implements UserService {
       private UserDao  userDao=null;
       public UserServiceImpl(   UserDao userDao) {
           this.userDao = userDao;
       }
   ```

   以上代码报错误，解决方案

   ```
   @Service
   public class UserServiceImpl implements UserService {
       private UserDao  userDao=null;
       public UserServiceImpl( @Qualifier(value = "userOralceDaoImpl") UserDao userDao) {
           this.userDao = userDao;
       }
   ```

   

3. 构造函数模式

   当一个类中的多个构造函数的时候，spring自动匹配到参数最多的那个构造函数进入装配

   ```java
   // 使用lombok
   @AllArgsConstructor
   public class FileUploadController extends BaseController {
       
       private FileUploadService fastDfsFileUploadServiceImpl;
   }
   
   
   //自己创建带参的构造函数
   public class FileUploadController extends BaseController {  
       private FileUploadService fastDfsFileUploadServiceImpl;
   
       public FileUploadController(FileUploadService fastDfsFileUploadServiceImpl) {
           this.fastDfsFileUploadServiceImpl = fastDfsFileUploadServiceImpl;
       }
   }
   
   //自己创建带参的构造函数，但是名称不一样
   public class FileUploadController extends BaseController {
       
       private FileUploadService a;
       public FileUploadController(@Qualifier(value = "fastDfsFileUploadServiceImpl") FileUploadService a) {
           this.a = a;
       }
   }
   ```

   

4. 默认模式

   spring自动在byName和byType之间自动选择

   

5. 不要装配（手动装配）    ,不要使用注解，使用XML自己去配置

​     不要有 @Component,@Repository ,@Service ,@Controller这些注解



## 7.1、希望应用程序在启动的时候，初始化一些数据  @PostConstruct

```java
@Configuration
public class InitConfig {


    /**
     * 程序在启动的时候执行操作
     */
    @PostConstruct
    public void init(){
        System.out.println("----加载数据到哪里去，或者做什么事情-----");
    }

}
```



## 7.2、spring如何读取自定义的配置信息   *.properties

```java
 读取当前项目下面的my.properties

<context:property-placeholder location="classpath:my.properties"></context:property-placeholder>
    
    
 读取所有引用包以及当前项目下面的my.properties
 <context:property-placeholder location="classpath*:my.properties"></context:property-placeholder>
```



```java
@Component
public class MyInfo {

    //读取配置文件属性
    @Value(value = "${my.password}")
    private String password;

    @Value(value = "${my.version}")
    private String version;

    /**
     * 如果没有配置，给一个默认值
     */
    @Value(value = "${my.jdbc.pwd:1234}")
    private String passwordDefault;

    @Override
    public String toString() {
        return "MyInfo{" +
                "password='" + password + '\'' +
                ", version='" + version + '\'' +
                ", passwordDefault='" + passwordDefault + '\'' +
                '}';
    }
}
```





## 8、 spring bean生命周期

<img src="16.png">

## 9、Spring ApplicαtionContext 国际化配置

什么是国际化：  i18n（其来源是英文单词 internationalization的首末字符i和n，18为中间的字符数）是“国际化”的简称

国际化（Internationalization）指的是同一个网站可以支持多种不同的语言，以方便不同国家，不同语种的用户访问。

```java
在spring中配置
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
         http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource">
          <!-- 指国际化文件开发的文件名     文件名_en.properties   文件名_zh_CN.properties -->
        <property name="basenames">
            <list>
                <value>message</value>     
            </list>
        </property>
        
        <property name="defaultEncoding" value="UTF-8"></property>
    </bean>


</beans>
```

国际化名称规范  https://wenku.baidu.com/view/906e26d0b90d4a7302768e9951e79b89680268f9.html?_wkts_=1667358279719&bdQuery=%E4%BB%80%E4%B9%88%E6%98%AF%E5%9B%BD%E9%99%85%E5%8C%96+i18n%E6%96%87%E4%BB%B6%E5%91%BD%E5%90%8D%E8%A7%84%E8%8C%83



建立相应国家的文件                 文件名规范           文件名 _ 语言  _  国家.properties

message_en.properties               英国                     message这个名称必须要与配置好的名称对应  basenames
message_zh_CN.properties        中国

设定ideal  File encoding  

<img src="17.png">



编写键和值

<img src="18.png">

测试



```java
public class MessageSourceDemo {
    public static void main(String... args) {
        ApplicationContext cx = new ClassPathXmlApplicationContext("spring-message.xml");

       Locale locale= Locale.getDefault();

        String message = cx.getMessage("msg", null,  Locale.ENGLISH);
        System.out.println(message);

        String login = cx.getMessage("login.name", null,  Locale.ENGLISH);
        System.out.println(login);
    }
}
```



```java
@RunWith(value = SpringJUnit4ClassRunner.class)
//加载哪一个spring配置文件
//ApplicationContext  ac=new ClassPathXmlApplicationContext("classpath:spring-annon3.xml");
@ContextConfiguration(locations = "classpath:spring-app4.xml")
public class Demo05Text {

    @Autowired
    private MessageSource  messageSource;

    @Test
    public void test01(){
        //指定国家
        //Locale aDefault = Locale.getDefault();
        Locale  aDefault=Locale.ENGLISH;


        String login = this.messageSource.getMessage("login", null, aDefault);
        System.out.println(login);
    }
}
```







## 二、Spring AOP 面向(方面)切面编程 (背)

<h3 style='color:red;'>AOP=面向对象编程+反射+动态代理+自定注解<br>
解决不同类中的不同方法内部的代码重复的问题,对OOP的补充</h3>


<h4 style='color:blue;'>AOP在项目中使用场景：
    <ol>
        <li>权限认证(spring security,shiro、自定义)</li>
        <li>操作日志写入操作</li>
        <li>关键字脱敏(手机银行、身份证号、银行卡号)</li>
        <li>JDBC事务处理(Spring-tx  @Transactional)</li>
        <li>性能监控，SQL执行时间   Druid</li>
         <li>拦截器 (Filter, HandlerInterceptor)</li>
        <li>防止用户重复提交数据</li>
         <li>接口访问次数限制（限流）</li>
        <li>分布锁</li>






### 1、术语

1.1、连接点（Aspect）

```html
 <p style="color:red;">定义公共方法（横切关注点），1.2声明通知,1.3 切入点表达式</p>
```



   连接点是应用程序执行期间明确定义的一个点。（程序方法内部中某些重复的位置）

  连接点的典型示例包括方法调用、方法调用本身、类初始化和对象实例化。

   连接点是AOP的核心概念，并且定义了在应用程序中可以使用AOP插入其他逻辑的点。

 1.2、 通知（Advice）

​     在特定连接点(某个类的某个方法中)执行的代码就是通知，它是由类中的方法定义的。
![4](4.png)

   spring给我们定义好的通知：

​      前置通知：  核心关注点之前的代码  @Before

​      后置通知：  核心关注点之后的代码   @After

​      异常通知：  抛出异常的时候的代码   @AfterThrowing

​     返回通知 :    返回结果的通知               @AfterReturning

​      环绕通知：  前置和后绕包围             @Around

![5](5.png)

正常情况aop执行顺序

![6](6.png)

异常情况aop执行顺序

![7](7.png)

1.3、切入点：(PointCut)

​     切入点是用于定义何时执行通知的连接点集合。定义对些类中的哪些方法进入注入。

     <p style="color:Red;">学习切入点的表达式(正则表达式)</p>





1.4、切面：Introduction

   切面:切面是封装在类中的通知和切入点的组合。这种组合定义了应该包含在应用程序中的逻辑及其应该执行的位置。
  spring框架帮你实现的，我们不需要关心



1.5、真实的对象（Target object）

​     spring框架会把你的真实对象与你的 1.1 连接点 类，使用动态代理模式，实现动态类，帮你实现完整功能。

 

1.6、动态代理模式 AOP proxy

  <p style="color:red;">学习动态代理模式 (JDK内置 接口级别，第三方框架cglib 方法级别的)</p>



1.7、织入(Weaving)

   这是通过引入其他方法或字段来修改对象结构的过程 (不需要关心)

### 2、 JAVA中动态代理(背景)

2.1、 JDK内置的动态代理

 1)、必条条件： 被代理的对象Target object 必须要有接口

 2)、新建一个类，实现InvocationHandler接口，重写invoke的方法

```
public class JDKDaynimcProxy implements InvocationHandler { 
    //真实的对象
    private Object targetObject; 
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //前置增强
        System.out.println("---动态代理前置增强-----");
        //核心关注点（反射调用方法）
        Object value=method.invoke(this.targetObject, args);

        //后置增强
        System.out.println("---动态代理后置增强-----");
        return value;
    }
}
```

 3)、通过JDK自动创建的Proxy类，来创建代理对象

```
  public Object createProxy(){

        //真实类的类加载器
        ClassLoader classLoader = this.targetObject.getClass().getClassLoader();
        //真实类的接口
        Class<?>[] interfaces = this.targetObject.getClass().getInterfaces();
        //实现InvocationHandler类
        InvocationHandler  invocationHandler=this;
        Object obj=Proxy.newProxyInstance(classLoader,interfaces,invocationHandler);
        return obj;
    }
```

![8](8.png)

使用cglib的增强，它不需要接口

1）导入第三方jar

```java
 <dependency>
            <groupId>cglib</groupId>
            <artifactId>cglib</artifactId>
            <version>3.3.0</version>
        </dependency>
```



2)、编写 一个类实现 MethodInterceptor接口  重写  intercept()



3)、生成动态代理对象

```java
public class CglibProxy implements MethodInterceptor {

    //真实的实现类(万能的)
    private Object  realObj;

    public CglibProxy(Object realObj) {
        this.realObj = realObj;
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        System.out.println("----万能代理对象，前置增强");

        //反射，调用真实对象方法
        Object value = method.invoke(this.realObj, args);

        System.out.println("----万能代理对象，后置增强");

        return value;
    }

    /**
     * 根据真实对象，生成接口的代理实现类
     * @return
     */
    public Object createProxy(){
        Enhancer  enhancer=new Enhancer();
        //父类为真实对象的类
        enhancer.setSuperclass(this.realObj.getClass());
        //回调实现
        enhancer.setCallback(this);

        return enhancer.create();
    }
}
```









![9](9.png)



<h3 style='color:red;'>spring框架判断该代理的对象是否有接口，如果有就使用JDK动态代理，如果没有就要使用第三方的CGLIB代理;<br/>
 jdk动态代理类必须要有接口，叫做类级别的代理，cglib不需要口被代理的类要实现接口，方法级别代理   
</h3>



SpringAOP代理接口  变为它帮你定义好的接口    前置，后置(最终)，环绕，异常，返回






### 3、@AspectJ

 3.1）、导包

​          aspectjweaver.jar

 

```
<dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>5.3.14</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.aspectj/aspectjweaver -->
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.9.8</version>
        </dependency>


      <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjrt</artifactId>
            <version>1.8.10</version>
        </dependency>
        
        
   如果是springboot ，就直接导入这个
    <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-aop</artifactId>
        </dependency>
```

​		3.2)、PointCut类型和表达式  (声明在哪些类中的哪些方法需要代理的)

​           PointCut支持类型:

​             execution:  采用则表达式的方式，来匹配方法 (重点掌握)      模糊匹配方法

​             within    : 根据类型进行匹配

​             args: 根据方法参数来匹配

​           @annotation： 根据自定注解来匹配  (重点掌握)                       精准匹配



​     3.3)、execution表达式

​     *  (匹配0 个或多个)                 + (匹配1个或多个))     ?  (匹配0个或1个)

  https://www.runoob.com/regexp/regexp-tutorial.html



   execution(  

​        modifiers-pattern?

​         return-type-pattern 

​       declaring-type-pattern?name-pattern(param-pattern)      

​     throws-pattern?)



 execution(

​         访问修饰符?  可有可无

​        返回值类型    必须要

​        包名+类名 （可有可无） 方法名(参数匹配)

​      抛出异常 (可有可无)

)



```
<T> T getBean(String name, Class<T> requiredType) throws BeansException
```



读一读以下execution代表的函义是什么？

<table>
    <tr>
        <td>execution(public * * (..))   匹配所有类中的公共方法</td>
    </tr>
    <tr>
    <td> execution(* set*(..))  匹配所有类中的set开头的方法 </td>
    </tr>
    <tr>
    <td>  execution(* com.xyz.service.AccountService.*(..)) <br>
        匹配com.xyz.service.AccountService类中所有的方法
      </td>
    </tr>
    <tr>
      <td>
        execution(* com.xyz.service.*.*(..))  <br>
           匹配com.xyz.service包下面的所有类中所有的方法(并不包含子包)<br>
          这个能不能匹配上  com.xzy.service.impl.UserImpl.set() 匹配不了
       </td>
    </tr>
    <tr>
      <td>
         execution(* com.xyz.service..*.*(..))
          匹配com.xyz.service当前包以及子包下面的所有类中所有的方法<br
        </td>
    </tr>
</table>


基于注解匹配，更精准匹配

@annotation(注解的完整路径名)



## 三、JAVA中的注解

1、什么是注解

  注解,就是对某一事物进行添加注释说明，会存放一些信息，这些信息可能对以后某个时段来说是很有用处的。

 注解java中以一种元数据格式存在，它是一种特殊接口

java可以使用反射机制获取某些类、变量、方法、参数 上面的注解，便于程序使用。

AOP=面向对象编程+反射+动态代理+自定注解



2、注解里面的 元注解类型

​    必须要在注解中定义1、2，  3和4选配

1. @Target 表示该注解用于什么地方

   > 1.1、ElemenetType.TYPE           类，接口（包括注解类型）或enum声明 
   >
   > 1.2、ElemenetType.METHOD    方法声明 
   >
   > 1.3、ElemenetType.FIELD          变量上面声明（包括 enum 实例)
   >
   > 1.4、ElemenetType.PARAMETER   方法的参数上面声明 

2. @Retention 表示在什么级别保存该注解信息

   > 2.1、RetentionPolicy.SOURCE 注解将被编译器丢弃  
   >
   > 2.2、RetentionPolicy.CLASS 注解在class文件中可用，但会被VM丢弃  
   >
   > 2.3、RetentionPolicy.RUNTIME   VM将在运行期也保留注释，因此可以通过反射机制读取注解的信息(重点)

3. @Documented    将此注解包含在 javadoc 中

4. @Inherited          允许子类继承父类中的注解



3、实现控制器新增，修改，删除操作时候，记录数据库日志表中

```
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log
{
    /**
     * 模块 
     */
    public String title() default "";
    /**
     * 功能
     */
    public BusinessType businessType() default BusinessType.OTHER;
    /**
     * 是否保存请求的参数
     */
    public boolean isSaveRequestData() default true;
    /**
     * 是否保存响应的参数
     */
    public boolean isSaveResponseData() default true;
}

public enum BusinessType
{
    /**
     * 其它
     */
    OTHER,

    /**
     * 新增
     */
    INSERT,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 授权
     */
    GRANT,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 导入
     */
    IMPORT
}
```



## 4、spring aop实现日志记录

<img src="20.png">

```java

DefaultAopProxyFactory类     spring aop根据目标对象，判断是否有接口
    
public AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException {
		if (!NativeDetector.inNativeImage() &&
				(config.isOptimize() || config.isProxyTargetClass() || hasNoUserSuppliedProxyInterfaces(config))) {
			Class<?> targetClass = config.getTargetClass();
			if (targetClass == null) {
				throw new AopConfigException("TargetSource cannot determine target class: " +
						"Either an interface or a target is required for proxy creation.");
			}
			if (targetClass.isInterface() || Proxy.isProxyClass(targetClass) || ClassUtils.isLambdaClass(targetClass)) {
				return new JdkDynamicAopProxy(config);
			}
			return new ObjenesisCglibAopProxy(config);
		}
		else {
			return new JdkDynamicAopProxy(config);
		}
	}
```



<img src="21.png">



@Before        					 AspectJMethodBeforeAdvice实现类

@After            			 		AspectJAfterAdvice
@AfterReturing     			AspectJAfterReturningAdvice
@AfterThrowing                    AspectJAfterThrowingAdvice
@Around								AspectJAroundAdvice
@PointCut							AspectJExpressionPointcut
AspectJExpressionPointcutAdvisor
AspectJMethodBeforeAdvice
AspectJPointcutAdvisor



对控制器Controller在做增强，是在执行Controller时候，才会去判断是否要做增强的



## 5、使用JSoup对网站内容进行解析

https://zhuanlan.zhihu.com/p/574405520





## 6、防止用户重复提交数据方案

### 6.1、纯前端的方案(面试官，太前端化了)

​       点击提交，禁用 按钮   ,                   但是如何用户按了F5刷新，怎么办呢？



### 6.2、后台如何来解决的

  为何用户会进行重复提交动作呢？           因为你的设计不合理，或者 由于网络延迟等等原因，让用户看不到未来（结果）



 重复提交，会发生在**很短时间**之内,同一笔数据，发生很多次提交操作.



以前在前后端不分离的情况下面，别人框架是这样解决的    

#### 6.2.1、struts2框架，前后端不分离，如何防止重复提交

步骤：
 （1）在第一次请求打开表单页面时，在Action中，通过uuid生成一个随机的**token**，然后保存到session中，在JSP表单的隐藏项中，赋值这个token
 （2）在第二次请求执行注册或者提交时，到达Action之前先用一个拦截器拦截一下，然后获取session中的token，再获取表单隐藏项的token，比较
 （3）如果相同，删除session中的token并放行
 （4）如果不同，转发到错误页面

实际上，知道了以上步骤之后，我们自己就能处理重复提交，但是，Struts2已经帮我们处理好了，所以我们只需要调用即可



### 6.2.2、 springmvc前后台分离，如何来实现呢?

  1)、解决  HttpServletRequest请求流，可以多次读取的问题 ，这个步骤意义在哪里？

​         收集用户HttpServletRequest中请求方式是(POST,PUT的请求类型中的值)   , 如果我们自己收集数据（通过拦截器来做），HttpServletRequest中的流，只允许被读取一次（转换成JSON，存储到 redis中 ,加一个过期时间    md5（字符串） ==md5（字符串)   ），    拦截器放行， 后面真正的servlet还能读取HttpServletReuqest中的数据吗？



<img src="22.png">















## 四、SpringMVC框架

### 1、什么是springmvc 

​     是spring的一个子框架， 采用MVC模型编写web框架.



### 2、mvc模型是什么

<img src="https://www.runoob.com/wp-content/uploads/2014/08/1200px-ModelViewControllerDiagram2.svg_.png">



- **Model（模型）** - 模型代表一个存取数据的对象或 JAVA POJO。它也可以带有逻辑，在数据变化时更新控制器。(mapper、service)
- **View（视图）** - 视图代表模型包含的数据的可视化。
- **Controller（控制器）** - 控制器作用于模型和视图上。它控制数据流向模型对象，并在数据变化时更新视图。它使视图与模型分离开(servlet)



### 3、配置第一个springmvc项目(面试必考)

   页面不要使用jsp（theamleaf,  freemarker来代替）

DispatcherServlet这个类

  <img src="https://pic002.cnblogs.com/images/2012/369718/2012071708190346.jpg" />



**1、核心类 ** DispatcherServlet,它是使用Servlet来实现的

**2、映射处理器 **  HandlerMapping  有点像Servlet 的 @WebServlet(urlPattern="/地址")

**3、HandlerInterceptor 拦截器**  像Servlet的Filter  (FilterChain过滤器链)

**4、Handler**    像Servlet的doGet()或doPost()的方法

**5、HandlerAdapter**  执行Handler方法的匹配器（收集参数(@PathVariable,@RequestBody,@RequestParam)、输入json格式、文件上传....）

**6、通过反射+动态代理**  执行Handler方法，返回 **ModelAndView** 模型和视图

**7、视图解析器**   ViewResolver (jsp，freemarker,thealmeaf、 @ResponseBody( JSON)、stream文件流.......)

**8、通过视图解析器把Model中的数据与view页面渲染**，生成html发给客户端



一个项目中，必须要配置一次 HandlerMapping,ViewResolver

Handler代码我们要自己写



https://docs.spring.io/spring-framework/docs/current/reference/html/web.html

![](10.png)





<img src="springmvc1.png">





### 4、springmvc常用注解

4.1、映射处理器注解(HandlerMapping)



<table>
     <tr>
     <td>注解名</td>
     <td>作用</td>
    </tr>
    <tr>
       <td>@Controller</td>
        <td>告诉Spring框架它是一个控制器类型</td>
    </tr>
    <tr>
       <td>@RestController=@Controller +<span style="color:red">@ResponseBody</span> </td>
        <td>告诉Spring框架它是一个控制器类型+返回是JSON格式</td>
    </tr>
    <tr>
      <td colspan="2">
          <img src="11.png">
        </td>
    </tr>
    <tr>
        <td>@RequestMapping(value="/url地址")支持所有的请求doGet,doPost,doPut,doDelete</td>
        <td>有点像@WebServlet(urlPattern="/url地址")</td>
    </tr>
    <tr>
        <td>@GetMapping(value="/地址")相当于是@RequestMapping(value="/地址",method = RequestMethod.GET)</td>
        <td>只支持doGet请求，相当于servlet中重写doGet()方法 </td>
    </tr>
     <tr>
        <td>@PostMapping(value="/地址")相当于是@RequestMapping(value="/地址",method = RequestMethod.POST)</td>
        <td>只支持doPost请求，相当于servlet中重写doPost()方法，<span style='color:red;'>在框架Post请求不是幂等性，每一次操作结果都可能不同(只用于做修改操作)</span> </td>
    </tr>
    <tr>
        <td colspan="2">
            post请求，支持2种类型参数：<br/>
            form entype="application/x-www-form-urlencoded" 标准的键值对格式，只能使用req.getParameter("键")或req.getParameterValues("键")来取值 <br/>
            form entype="application/json"  以流方式请求，读取流转换成字符串再转换json对象，在SpringMVC框架，我们需要使用  <span style="color:red">@ReqeustBody</span>   注解来解决
             public AjaxResult add(@Validated @RequestBody SysPost post)
        </td>
    </tr>
     <tr>
        <td>@PutMapping(value="/地址")相当于是@RequestMapping(value="/地址",method = RequestMethod.PUT)</td>
        <td>只支持doPut请求，相当于servlet中重写doPut()方法，<span style='color:red;'>在框架Put请求是幂等性，一般用于insert操作</span> </td>
    </tr>
    <tr>
        <td>@DeleteMapping(value="/地址")相当于是@RequestMapping(value="/地址",method = RequestMethod.DELETE)</td>
        <td>只支持doDelete请求，相当于servlet中重写doDelete()方法，<span style='color:red;'>在框架Delete请求是幂等性，一般用于delete操作</span> </td>
    </tr>
    <tr>
        <td>@PathVariable("/url地址/{占位符变量名}") </td>
        <td> 路径匹配算法表达式, <br/>
             @DeleteMapping("/user/delete/{postIds}") <br/>
             public AjaxResult remove(@PathVariable Long[] postIds) 
             <br/>
            以前访问的地址: http://localhost:8080/user/delete?postIds=1 <br/>
            现在路径表达式：  http://localhost:8080/user/delete/1
    </td>
    </tr>
     <tr>
        <td> 
            public AjaxResult optionselect(@RequestParam(value="optionId") String id )
         </td>
        <td> url地址请求参数与方法参数名对不上的解决方法 
              <br/>
             http://localhost:8080/optionselect?optionId=1
         </td>
    </tr>
</table>


## # 4.2.1、集成thymeleaf

```java
 <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <spring.version>5.3.20</spring.version>
  </properties>

  <dependencies>


    <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-beans</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-core</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context-support</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-aop</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.13.1</version>
    </dependency>


      <!-- https://mvnrepository.com/artifact/org.thymeleaf/thymeleaf-spring5  页面技术 -->
      <dependency>
          <groupId>org.thymeleaf</groupId>
          <artifactId>thymeleaf-spring5</artifactId>
          <version>3.0.11.RELEASE</version>
      </dependency>




  </dependencies>
```



### 4.2.2、后台向前端页面传递参数

http://c.biancheng.net/spring_boot/thymeleaf.html

```java
@Controller
public class FirstController {
     @GetMapping(value = "/demo02")
    public ModelAndView   demo02(){
        ModelAndView  mv=new ModelAndView();
        mv.setViewName("demo02");
        mv.addObject("name","小张");
        mv.addObject("sex","男");
        mv.addObject("age",23);
        return mv;
    }
}
```

**Thymeleaf 是一款用于渲染 XML/XHTML/HTML5 内容的模板引擎。它与 JSP，Velocity，FreeMaker 等模板引擎类似，也可以轻易地与 Spring MVC 等 Web 框架集成。与其它模板引擎相比，Thymeleaf 最大的特点是，即使不启动 Web 应用，也可以直接在浏览器中打开并正确显示模板页面 。**

**Thymeleaf 是新一代 Java 模板引擎，与 Velocity、FreeMarker 等传统 Java 模板引擎不同，Thymeleaf 支持 HTML 原型，其文件后缀为“.html”，因此它可以直接被浏览器打开，此时浏览器会忽略未定义的 Thymeleaf 标签属性，展示 thymeleaf 模板的静态页面效果；当通过 Web 应用程序访问时，Thymeleaf 会动态地替换掉静态内容，使页面动态显示。**



使用  th:text=""

```
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>第一个案例</title>
</head>
<body>

   <h1>取得Model的返回的结果</h1>

   <h2> 姓名：  <span th:text="${name}"></span> </h2>
   <h2> 性别：  <span th:text="${sex}"></span> </h2>
   <h2> 年龄：  <span th:text="${age}"></span> </h2>
</body>
</html>
```



### 4.2.3、通过后台接收参数，显示多笔数据如何来做

```java
 @GetMapping(value = "/demo03")
    public ModelAndView   demo03(Integer n){
        ModelAndView  mv=new ModelAndView();
        //参数有问题
        if(n==null || n<=0){
            mv.setViewName("error");
            return mv;
        }

        mv.setViewName("demo03");

        List<Map<String,Object>>  list=new ArrayList<>();
        for(int i=0;i<n;i++){
         Map<String,Object>  mp=new HashMap<>();
         mp.put("name","小张"+i);
         mp.put("sex", i%2==0?"男":"女");
         mp.put("age", 10+i);
         list.add(mp);
        }
        mv.addObject("data", list);

        return mv;
    }
```



```java
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>第一个案例</title>
</head>
<body> 
   <h1>thymeleaf循环标签，对象标签组合使用</h1>  
   <table>
       <tr>
           <td>姓名</td>
           <td>性别</td>
           <td>年龄</td>
       </tr>

       <tr th:each="m:${data}">
           <td th:text="${m.name}"></td>
           <td th:text="${m.sex}"></td>
           <td th:text="${m.age}"></td>
       </tr>

   </table>
</body>
</html>
```



## 4.2.4、后台使用restful风格表达式来接收参数，显示多笔数据如何来做

http://localhost:8080/demo03?n=20         通过? 拼接参数，这种风格不太好，现在流行 restful风格

http://localhost:8080/demo03/3               restful风格

```java
@GetMapping(value = "/demo03/{number}")
    public ModelAndView   demo04( @PathVariable(value = "number") Integer n){
        ModelAndView  mv=new ModelAndView();
        //参数有问题
        if(n==null || n<=0){
            mv.setViewName("error");
            return mv;
        }

        mv.setViewName("demo03");

        List<Map<String,Object>>  list=new ArrayList<>();
        for(int i=0;i<n;i++){
            Map<String,Object>  mp=new HashMap<>();
            mp.put("name","小张"+i);
            mp.put("sex", i%2==0?"男":"女");
            mp.put("age", 10+i);
            list.add(mp);
        }
        mv.addObject("data", list);

        return mv;
    }
```



## 4.2.5、接收自定义对象 enctype="application/x-www-form-urlencoded"

使用servlet    request.getParameter()    或    reuqest.getParemterValues()



<img src="24.png">



```java
@DateTimeFormat(pattern = "yyyy-MM-dd")
private Date birthday; 
```



post接收的数据是乱码的问题

<img src="25.png">

以前servlet项目的时候，每一次都把请求 request.setCharaterEncoding("UTF-8");    为简化，是不是搞了一个拦截器，对所有请求做编码过滤



web.xml添加filter拦截器

```java
 <!-- post提交中文乱码的问题-->
    <filter>
        <filter-name>CharacterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>forceResponseEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
    </filter>

    <filter-mapping>
        <filter-name>CharacterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
```



前置

```java
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1>向后台使用post提交数据</h1>

     <form method="post" action="/user" enctype="application/x-www-form-urlencoded">

         <h2>用户名：   <input type="text" name="name"> </h2>
         <h2>性别：   <input type="text" name="sex"> </h2>
         <h2>年龄：   <input type="text" name="age"> </h2>
         <h2>出生日期：   <input type="text" name="birthday"> </h2>

         <input type="submit" value="提交">
     </form>
</body>
</html>
```





## 4.2.6、接收自定义对象 enctype="application/json"  必须是post提交方式

前端提交就是stream数据，   不能再使用  request.getParamter()来接收，  必须使用 IO读取方式来做处理

 request.getInputStream()   IO流读取字符串再转换

必须使用springmvc给我们提供的增强类 AOP    @RequestBody 



post软件页面

<img src="26.png">



ideal 2017的页面

<img src="27.png">



ideal 2020 版本以上的界面



<img src="28.png">





```java
/**
     * 保存数据  前端提交数据类型 application/json
     * 使用工具模拟提交数据 (axios)
     * @param userInfo
     * @return
     */
    @PostMapping(value = "/user/stream")
    public String addStream(@RequestBody UserInfo userInfo){
        System.out.println(userInfo.toString());
        return "user";
    }
```



如何springmvc使用  @RequestBody ，你就必须要添加 json格式的支持   ，默认的springmvc没有帮你配置，你需要自己动的  jackson

```java
 <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>2.13.3</version>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-core</artifactId>
      <version>2.13.3</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-jaxb-annotations -->
    <dependency>
      <groupId>com.fasterxml.jackson.module</groupId>
      <artifactId>jackson-module-jaxb-annotations</artifactId>
      <version>2.13.3</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations -->
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-annotations</artifactId>
      <version>2.13.3</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/org.codehaus.jackson/jackson-core-asl -->
    <dependency>
      <groupId>org.codehaus.jackson</groupId>
      <artifactId>jackson-core-asl</artifactId>
      <version>1.9.13</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/org.codehaus.jackson/jackson-mapper-asl -->
    <dependency>
      <groupId>org.codehaus.jackson</groupId>
      <artifactId>jackson-mapper-asl</artifactId>
      <version>1.9.13</version>
    </dependency>
```



需要去springmvc的前端控制器中去配置   app-servlet.xml 中去， json解析器

```java
     <!-- 可以使用注解方式来配置web项目 -->
    <mvc:annotation-driven>
        <mvc:message-converters>
            <ref bean="stringHttpMessageConverter"/>
            <ref bean="mappingJackson2HttpMessageConverter"/>
        </mvc:message-converters>
    </mvc:annotation-driven>

    <!--  设置返回字符串编码-->
    <bean id="stringHttpMessageConverter"
          class="org.springframework.http.converter.StringHttpMessageConverter">
        <property name = "supportedMediaTypes">
            <list>
                <value>text/html;charset=UTF-8</value>
                <value>application/json;charset=UTF-8</value>
            </list>
        </property>
    </bean>

    <!--解决IE浏览器json文件下载和json数据中午乱码的问题-->
    <bean id="mappingJackson2HttpMessageConverter"
          class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
        <property name="supportedMediaTypes">
            <list>
                <value>text/html;charset=UTF-8</value>
                <value>application/json;charset=UTF-8</value>
            </list>
        </property>
    </bean>
```





## 4.2.6、springmvc 接收数据或集合

```java
    @GetMapping(value = "/deletes")
    public String arrInfo(Integer[]  ids, Model model){

        for(Integer s : ids){
            System.out.println(s);
        } 
        model.addAttribute("ids",ids); 
        return "demo04";
    }
```



前端传递数据

# http://localhost:8080/deletes?ids=1,2,3,4,5

或

##  http://localhost:8080/deletes?ids=1&ids=2&ids=3&ids=4&ids=5



## 4.2.7、springmvc  使用Map接收数据

前提是必须使用 Post   +  @RequestBody方案

```
 @PostMapping(value = "/map")
    public String mapInfo(@RequestBody  Map  map, Model model){
    }
```



# 4.3、 后台返回json数据给前端  @ResponseBody 

## springmvc有2种方案组合

1)、   @Controller  注解    +       方法上面加个  @ResponseBody             混合用 （前后台不分离，需要后台提供json）

 2)、直接使用 @RestController    =   @Controller  + @ResponseBody      (纯前后台分离)



# 五、本地上传文件

## 5.1、在本地配置一个虚拟存储路径以及访问地址

 存储路径 D:\image_server           用户访问我这个台电脑 url 地址   /img

<img src="29.png">

测试一下虚拟图片服务器是否可访问

http://localhost:8080/img/1562328784508686336.png





## 4.4.2、第二写代码上传图片到路径

```xml
导包
<!-- https://mvnrepository.com/artifact/commons-fileupload/commons-fileupload -->
<dependency>
    <groupId>commons-fileupload</groupId>
    <artifactId>commons-fileupload</artifactId>
    <version>1.4</version>
</dependency>
<!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
<dependency>
    <groupId>commons-io</groupId>
    <artifactId>commons-io</artifactId>
    <version>2.6</version>
</dependency>

```

### 4.4.3、文件上传的流程

<img src="30.png">



MultipartResolver接口  

*  MultipartResolver接口定义了文件上传过程中的相关操作，并对通用性操作进行了封装
*  MultipartResolver接口底层实现类CommonsMultipartResovler
*  CommonsMultipartResovler并未自主实现文件上传下载对应的功能，而是调用了apache的文件上传下载组件 

文件上传下载实现

*   页面表单  

```
<form action="/fileupload" method="post" enctype="multipart/form-data">
    上传LOGO： <input type="file" name="attach"/><br/>
    <input type="submit" value="上传"/>
</form>
```

SpringMVC配置  app-servlet.xml

```java
<bean id="multipartResolver"
      class="org.springfr<bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
       <property name="defaultEncoding" value="UTF-8"></property>
        <!-- 最大支持2MB= 1024*1024*2-->
        <property name="maxUploadSize" value="2097152"></property>
    </bean>amework.web.multipart.commons.CommonsMultipartResolver">
</bean>
```

控制器代码

```java
@RequestMapping(value = "/fileupload")
public void fileupload(MultipartFile attach){
    file.transferTo(new File("file.png"));
}
```



文件上传注意事项目：

1. 文件命名问题， 获取上传文件名，并解析文件名与扩展名
2. 文件名过长问题
3. 文件保存路径
4. 重名问题

```java
 @ResponseBody
    @PostMapping(value = "/fileupload")
    public Map<String,Object> uploadFile(MultipartFile attach) throws IOException {
        Map<String,Object>  mp=new HashMap<>();

        // 如果文件为空
        if(attach.isEmpty()){
            mp.put("error","上传文件不允许为空");
            return mp;
        }

        //文件原始名称
        String oldFileName=attach.getOriginalFilename();
        //文件的mime类型  https://www.jianshu.com/p/6e3b0d980bec
        String mineTyipe= attach.getContentType();
        //文件大小 字节
        long size=attach.getSize();

        String ext="";
        //得到文件后缀名,  这个判断代表没有后缀名
        if(oldFileName.lastIndexOf(".")>0){ 
            ext= oldFileName.substring( oldFileName.lastIndexOf("."));
        }

        //创建唯一的名称   UUID最简单的唯一标识
        String fileName= UUID.randomUUID().toString()+ext;
        //创建一个新的文件
        File newFile=new File(ROOT_PATH, fileName);

        //文件另存为哪里  transferTo  需要调用 commons-fileupload.jar的类
        attach.transferTo( newFile );

        //返回给前端信息
        String url =IMAGE_SERVER+fileName;
        mp.put("filename",oldFileName);
        mp.put("url",url);
        return mp;
    }
```



 



# 6、springmvc 要对静态资源放行，不然访问不了



 springmvc框架会把 http://localhost:8080/static/plugins/bootstrap/css/bootstrap.css 请求认为是HandlerMapping控制器的地址，所以找不到

配置，告诉springmvc，静态资源，你要给我放行

```
 <mvc:resources mapping="/static/**" location="/static/"></mvc:resources>
```

<img src="12.png">



 



## 6.1、新增页面操作

<img src="13.png">

想尽办法使用ajax读取数据，把它放到指定的div标签中

https://jquery.cuishifeng.cn/jQuery.Ajax.html

https://jquery.cuishifeng.cn/append.html    在节点中追加元素

https://jquery.cuishifeng.cn/empty.html  删除节点的数据



# 六、springmvc拦截器HandlerInterceptor



<img src="31.png">



 拦截器（ Interceptor）是一种动态拦截方法调用的机制
 作用：

      	1. 在指定的方法调用前后执行预先设定后的的代码
     	2. 阻止原始方法的执行

 核心原理： AOP思想
 拦截器链：多个拦截器按照一定的顺序，对原始被调用功能进行增强  



**拦截器Interceptor    VS   过滤器Filter**
 归属不同： Filter属于Servlet技术， Interceptor属于SpringMVC技术
 拦截内容不同： Filter对所有访问进行增强， Interceptor仅针对SpringMVC的访问进行增强  

<img src="32.png">





   ## 1、filter  过滤器

​     Filter可以认为是[Servlet](https://so.csdn.net/so/search?q=Servlet&spm=1001.2101.3001.7020)的一种“加强版”，它主要用于对用户请求进行预处理，也可以对HttpServletResponse进行后处理，是个典型的处理链。Filter也可以对用户请求生成响应，这一点与Servlet相同，但实际上很少会使用Filter向用户请求生成响应。

​     使用Filter完整的流程是：Filter对用户请求进行预处理，接着将请求交给Servlet进行预处理并生成响应，最后Filter再对服务器响应进行后处理。

   ```java
void init(FilterConfig config):用于完成Filter的初始化。
void destory():用于Filter销毁前，完成某些资源的回收。
    
void doFilter(ServletRequest request,ServletResponse response,FilterChain chain){
    
     //放行
    chain.doFilter(request,response);
}
   ```



## 2、HandlerInterceptor拦截器介绍

 [拦截器](https://so.csdn.net/so/search?q=拦截器&spm=1001.2101.3001.7020)，在AOP(Aspect-Oriented Programming)中用于在某个方法或字段被访问之前，进行拦截，然后在之前或之后加入某些操作。拦截是AOP的一种实现策略。

  在执行 servlet(handler)  之前或之后要处理事情   ,构造成一个过滤器链

```
在执行 handler方法之前

default boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

            //true 放行
		return true;
	}

	/**
	 * Interception point after successful execution of a handler.
	    执行完handler以后做的事情
	 */
	default void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			@Nullable ModelAndView modelAndView) throws Exception {
			
	}
	
	/**
	 * Callback after completion of request processing, that is, after rendering
	 * the view. Will be called on any outcome of handler execution, thus allows
	 * for proper resource cleanup.
	   准备结果发给前端了，要做什么事情
	 */
	default void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			@Nullable Exception ex) throws Exception {
	}
	//三个方法的运行顺序为    preHandle -> postHandle -> afterCompletion
    //如果preHandle返回值为false，三个方法仅运行preHandle
```



配置拦截器   

```
<mvc:interceptors>
    <mvc:interceptor>
        <mvc:mapping path="/showPage"/>
        <bean class="com.hxzy.interceptor.MyInterceptor"/>
    </mvc:interceptor>
</mvc:interceptors>
```

  <u>注意：配置顺序为先配置执行位置，后配置执行类</u>  

## 3、 拦截器执行流程

<img src="33.png">



### 3.1、前置处理方法

原始方法之前运行

```java
public boolean preHandle(HttpServletRequest request,
                         HttpServletResponse response,
                         Object handler) throws Exception {
    System.out.println("preHandle");
    return true;
}
```

* 参数
   request:请求对象
   response:响应对象
   handler:被调用的处理器对象，本质上是一个方法对象，对反射中的Method对象进行了再包装
* 返回值
   返回值为false，被拦截的处理器将不执行  



### 3.2、后置处理方法

原始方法运行后运行，如果原始方法被拦截，则不执行  

```java
public void postHandle(HttpServletRequest request,
                       HttpServletResponse response,
                       Object handler,
                       ModelAndView modelAndView) throws Exception {
    System.out.println("postHandle");
}
```

 参数
 modelAndView:如果处理器执行完成具有返回结果，可以读取到对应数据与页面信息，并进行调整  

### 3.3、完成处理方法

  拦截器最后执行的方法，无论原始方法是否执行  

```java
public void afterCompletion(HttpServletRequest request,
                            HttpServletResponse response,
                            Object handler,
                            Exception ex) throws Exception {
    System.out.println("afterCompletion");
}
```

 参数
 ex:如果处理器执行过程中出现异常对象，可以针对异常情况进行单独处理  

## 3.4、拦截器配置项  

```xml
<mvc:interceptors>
    <!--开启具体的拦截器的使用，可以配置多个-->
    <mvc:interceptor>
        <!--设置拦截器的拦截路径，支持*通配-->
        <!--/**         表示拦截所有映射-->
        <!--/*          表示拦截所有/开头的映射-->
        <!--/user/*     表示拦截所有/user/开头的映射-->
        <!--/user/add*  表示拦截所有/user/开头，且具体映射名称以add开头的映射-->
        <!--/user/*All  表示拦截所有/user/开头，且具体映射名称以All结尾的映射-->
        <mvc:mapping path="/*"/>
        <mvc:mapping path="/**"/>
        <mvc:mapping path="/handleRun*"/>
        <!--设置拦截排除的路径，配置/**或/*，达到快速配置的目的-->
        <mvc:exclude-mapping path="/b*"/>
        <!--指定具体的拦截器类-->
        <bean class="MyInterceptor"/>
    </mvc:interceptor>
</mvc:interceptors>
```



## 3.5、多拦截器配置

<img src="34.png">

**责任链模式**
 责任链模式是一种行为模式
 特征：
沿着一条预先设定的任务链顺序执行，每个节点具有独立的工作任务
 优势：
独立性：只关注当前节点的任务，对其他任务直接放行到下一节点
隔离性：具备链式传递特征，无需知晓整体链路结构，只需等待请求到达后进行处理即可
灵活性：可以任意修改链路结构动态新增或删减整体链路责任
解耦：将动态任务与原始任务解耦
 弊端：
链路过长时，处理效率低下
可能存在节点上的循环引用现象，造成死循环，导致系统崩溃  



## 3.5、Filter和HandlerInterceptor的区别(背)

1)、Filter是基于函数回调的，而Interceptor则是基于Java反射的。
2)、Filter依赖于Servlet容器，而Interceptor不依赖于Servlet容器它只是一个接口。
3)、Filter对几乎所有的请求起作用，而Interceptor只能对springmvc的handler请求起作用。
4)、Interceptor可以访问Spring容器里的对象，而Filter不能。
5)、Interceptor可以被多次调用，而Filter只能在容器初始化时调用一次。





## 5、springmvc使用json处理 double和BigDecmial有小数点为0的情况

<img src="23.png">



这个是 Jackson闯的货

```java
# 自定义json字段处理器
public class CustomerDoubleSerialize extends JsonSerializer<BigDecimal> {

    //原本这里是  ##.00 ,带来的问题是如果数据库数据为0.00返回“ .00 “经评论指正，改为0.00
    private DecimalFormat df = new DecimalFormat("0.00");

    @Override
    public void serialize(BigDecimal arg0, JsonGenerator arg1, SerializerProvider arg2) throws IOException {
        if(arg0 != null && !"".equals(arg0)) {
            arg1.writeString(df.format(arg0));
        }else {
            arg1.writeString(arg0 + "");
        }
    }

}

# 自定义json字段处理器
public class CustomerDoubleSerialize extends JsonSerializer<Dubole> {

    //原本这里是  ##.00 ,带来的问题是如果数据库数据为0.00返回“ .00 “经评论指正，改为0.00
    private DecimalFormat df = new DecimalFormat("0.00");

    @Override
    public void serialize(Dubole arg0, JsonGenerator arg1, SerializerProvider arg2) throws IOException {
        if(arg0 != null && !"".equals(arg0)) {
            arg1.writeString(df.format(arg0));
        }else {
            arg1.writeString(arg0 + "");
        }
    }

}
```



使用

```java
@JsonSerialize(using=CustomerDoubleSerialize.class)
private BigDecmial   wtPrice;
```





# 7、异常处理

## 7.1 异常处理器

 **HandlerExceptionResolver**接口（异常处理器）  

```java
@Component
public class ExceptionResolver implements HandlerExceptionResolver {
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response,
                                         Object handler,
                                         Exception ex) {
        System.out.println("异常处理器正在执行中");
        ModelAndView modelAndView = new ModelAndView();
        //定义异常现象出现后，反馈给用户查看的信息
        modelAndView.addObject("msg","出错啦！ ");
        //定义异常现象出现后，反馈给用户查看的页面
        modelAndView.setViewName("error.jsp");
        return modelAndView;
    }
}
```

  根据异常的种类不同，进行分门别类的管理，返回不同的信息  

```java
public class ExceptionResolver implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response,
                                         Object handler,
                                         Exception ex) {
        System.out.println("my exception is running ...."+ex);
        ModelAndView modelAndView = new ModelAndView();
        if( ex instanceof NullPointerException){
            modelAndView.addObject("msg","空指针异常");
        }else if ( ex instanceof  ArithmeticException){
            modelAndView.addObject("msg","算数运算异常");
        }else{
            modelAndView.addObject("msg","未知的异常");
        }
        modelAndView.setViewName("error.jsp");
        return modelAndView;
    }
}
```

## 7.2 注解开发异常处理器

* 使用注解实现异常分类管理
   名称： @ControllerAdvice
   类型： 类注解
   位置：异常处理器类上方
   作用：设置当前类为异常处理器类
   范例：

```java
@Component
@ControllerAdvice
public class ExceptionAdvice {
}  
```

* 使用注解实现异常分类管理
   名称： @ExceptionHandler
   类型： 方法注解
   位置：异常处理器类中针对指定异常进行处理的方法上方
   作用：设置指定异常的处理方式
   范例：
   说明：处理器方法可以设定多个

 ```java
@ExceptionHandler(Exception.class)
@ResponseBody
public String doOtherException(Exception ex){
    return "出错啦，请联系管理员！ ";
}  
 ```

## 7.3 异常处理解决方案

* 异常处理方案
  * 业务异常：
     发送对应消息传递给用户，提醒规范操作
  * 系统异常：
     发送固定消息传递给用户，安抚用户
     发送特定消息给运维人员，提醒维护
     记录日志
  * 其他异常：
     发送固定消息传递给用户，安抚用户
     发送特定消息给编程人员，提醒维护
     纳入预期范围内
     记录日志  





## 7.4 自定义异常

* 异常定义格式

  ```java
  //自定义异常继承RuntimeException，覆盖父类所有的构造方法
  public class BusinessException extends RuntimeException {
      public BusinessException() {
      }
  
      public BusinessException(String message) {
          super(message);
      }
  
      public BusinessException(String message, Throwable cause) {
          super(message, cause);
      }
  
      public BusinessException(Throwable cause) {
          super(cause);
      }
  
      public BusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
          super(message, cause, enableSuppression, writableStackTrace);
      }
  }
  ```

* 异常触发方式

  ```java
  if(user.getName().trim().length()<4) {
      throw new BusinessException("用户名长度必须在2-4位之间，请重新输入！ ");
  }
  ```

* 通过自定义异常将所有的异常现象进行分类管理，以统一的格式对外呈现异常消息  



# 8异步调用

## 8.1 发送异步请求（回顾）  

```js
<a href="javascript:void(0);" id="testAjax">访问controller</a>
<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $(function(){
    $("#testAjax").click(function(){ //为id="testAjax"的组件绑定点击事件
        $.ajax({ //发送异步调用
            type:"POST", //请求方式： POST请求
            url:"ajaxController", //请求参数（也就是请求内容）
            data:'ajax message', //请求参数（也就是请求内容）
            dataType:"text", //响应正文类型
            contentType:"application/text", //请求正文的MIME类型
        });
    });
});
</script>
```

## 8.2 接受异步请求参数

 名称： @RequestBody
 类型： 形参注解
 位置：处理器类中的方法形参前方
 作用：将异步提交数据组织成标准请求参数格式，并赋值给形参
 范例：

```java
@RequestMapping("/ajaxController")
public String ajaxController(@RequestBody String message){
    System.out.println(message);
    return "page.jsp";
}  
```

* 注解添加到Pojo参数前方时，封装的异步提交数据按照Pojo的属性格式进行关系映射
* 注解添加到集合参数前方时，封装的异步提交数据按照集合的存储结构进行关系映射 

```java
@RequestMapping("/ajaxPojoToController")
//如果处理参数是POJO，且页面发送的请求数据格式与POJO中的属性对应，@RequestBody注解可以自动映射对应请求数据到POJO中
//注意：POJO中的属性如果请求数据中没有，属性值为null，POJO中没有的属性如果请求数据中有，不进行映射
public String  ajaxPojoToController(@RequestBody User user){
    System.out.println("controller pojo :"+user);
    return "page.jsp";
}

@RequestMapping("/ajaxListToController")
//如果处理参数是List集合且封装了POJO，且页面发送的数据是JSON格式的对象数组，数据将自动映射到集合参数中
public String  ajaxListToController(@RequestBody List<User> userList){
    System.out.println("controller list :"+userList);
    return "page.jsp";
}
```

## 8.3 异步请求接受响应数据

* 方法返回值为Pojo时，自动封装数据成json对象数据

```java
@RequestMapping("/ajaxReturnJson")
@ResponseBody
public User ajaxReturnJson(){
    System.out.println("controller return json pojo...");
    User user = new User();
    user.setName("Jockme");
    user.setAge(40);
    return user;
}  
```

* 方法返回值为List时，自动封装数据成json对象数组数据  

```java
@RequestMapping("/ajaxReturnJsonList")
@ResponseBody
//基于jackon技术，使用@ResponseBody注解可以将返回的保存POJO对象的集合转成json数组格式数据
public List ajaxReturnJsonList(){
    System.out.println("controller return json list...");
    User user1 = new User();
    user1.setName("Tom");
    user1.setAge(3);

    User user2 = new User();
    user2.setName("Jerry");
    user2.setAge(5);

    ArrayList al = new ArrayList();
    al.add(user1);
    al.add(user2);

    return al;
}
```

# 9 异步请求-跨域访问

## 2.1 跨域访问介绍

* 当通过域名A下的操作访问域名B下的资源时，称为跨域访问
* 跨域访问时，会出现无法访问的现象   

![image-20200427162623591](D:/上课笔记/spring/笔记/day02.assets/image-20200427162623591.png)

## 2.2 跨域环境搭建

* 为当前主机添加备用域名
  * 修改windows安装目录中的host文件
  * 格式： ip 域名
* 动态刷新DNS
  *  命令： ipconfig /displaydns
  *  命令： ipconfig /flushdns   

## 2.3 跨域访问支持  

 名称： @CrossOrigin
 类型： 方法注解 、 类注解
 位置：处理器类中的方法上方 或 类上方
 作用：设置当前处理器方法/处理器类中所有方法支持跨域访问
 范例：  

```java
@RequestMapping("/cross")
@ResponseBody
//使用@CrossOrigin开启跨域访问
//标注在处理器方法上方表示该方法支持跨域访问
//标注在处理器类上方表示该处理器类中的所有处理器方法均支持跨域访问
@CrossOrigin
public User cross(HttpServletRequest request){
    System.out.println("controller cross..."+request.getRequestURL());
    User user = new User();
    user.setName("Jockme");
    user.setAge(39);
    return user;
}
```





## 10、集成ssm三大框架

## 10.1、ssm

   spring          springmvc         mybatis框架

  spring 管理所有的类，包含事务、通用资源等等

  springmvc 简化 servlet操作

  mybatis 简化数据库的操作



## 10.2、导入相关的依赖

```java
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.hxzy</groupId>
  <artifactId>ssm-web</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>war</packaging>

  <name>ssm-web Maven Webapp</name>
  <!-- FIXME change it to the project's website -->
  <url>http://www.example.com</url>


  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <spring.version>5.3.20</spring.version>
  </properties>

  <dependencies>


    <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-beans</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-core</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context-support</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-aop</artifactId>
      <version>${spring.version}</version>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.13.1</version>
    </dependency>


    <!-- https://mvnrepository.com/artifact/org.thymeleaf/thymeleaf-spring5  页面技术 -->
    <dependency>
      <groupId>org.thymeleaf</groupId>
      <artifactId>thymeleaf-spring5</artifactId>
      <version>3.0.11.RELEASE</version>
    </dependency>

    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>1.18.16</version>
    </dependency>


    <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>2.13.3</version>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-core</artifactId>
      <version>2.13.3</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-jaxb-annotations -->
    <dependency>
      <groupId>com.fasterxml.jackson.module</groupId>
      <artifactId>jackson-module-jaxb-annotations</artifactId>
      <version>2.13.3</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations -->
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-annotations</artifactId>
      <version>2.13.3</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/org.codehaus.jackson/jackson-core-asl -->
    <dependency>
      <groupId>org.codehaus.jackson</groupId>
      <artifactId>jackson-core-asl</artifactId>
      <version>1.9.13</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/org.codehaus.jackson/jackson-mapper-asl -->
    <dependency>
      <groupId>org.codehaus.jackson</groupId>
      <artifactId>jackson-mapper-asl</artifactId>
      <version>1.9.13</version>
    </dependency>


    <!-- 文件上传的包-->
    <!-- https://mvnrepository.com/artifact/commons-fileupload/commons-fileupload -->
    <dependency>
      <groupId>commons-fileupload</groupId>
      <artifactId>commons-fileupload</artifactId>
      <version>1.4</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>2.6</version>
    </dependency>


    <!-- https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>3.1.0</version>
      <scope>provided</scope>
    </dependency>


    <!--mybatis环境-->
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis</artifactId>
      <version>3.5.3</version>
    </dependency>
    <!--mysql环境-->
    <!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>8.0.16</version>
    </dependency>

    <!--spring整合jdbc-->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
      <version>${spring.version}</version>
    </dependency>
    <!--spring整合mybatis-->
    <dependency>
      <groupId>org.mybatis</groupId>
      <artifactId>mybatis-spring</artifactId>
      <version>2.0.3</version>
    </dependency>
    <!--druid连接池-->
    <dependency>
      <groupId>com.alibaba</groupId>
      <artifactId>druid</artifactId>
      <version>1.1.16</version>
    </dependency>
    <!--分页插件坐标-->
    <dependency>
      <groupId>com.github.pagehelper</groupId>
      <artifactId>pagehelper</artifactId>
      <version>5.1.2</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core -->
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <version>2.17.1</version>
    </dependency>



  </dependencies>

  <build>
    <finalName>ssm-web</finalName>
    <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
      <plugins>
        <plugin>
          <artifactId>maven-clean-plugin</artifactId>
          <version>3.1.0</version>
        </plugin>
        <!-- see http://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_war_packaging -->
        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <version>3.0.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.8.0</version>
        </plugin>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>2.22.1</version>
        </plugin>
        <plugin>
          <artifactId>maven-war-plugin</artifactId>
          <version>3.2.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>2.5.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>2.8.2</version>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
</project>


```



## 10.3、配置 web.xml

配置springmvc和阿里巴巴druid数据库连接池的监听sql的框架

```xml
<?xml version="1.0" encoding="UTF-8"?>

<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
                      http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">

    <!-- 做一全局监听器-->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>

    <!-- 加载 spring ioc和aop的核心配置文件 -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:spring-application.xml</param-value>
    </context-param>


    <!-- springmvc的入口(核心 DispatcherServlet) -->
    <servlet>
        <servlet-name>app</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet-mapping>
        <servlet-name>app</servlet-name>
        <url-pattern>/*</url-pattern>
    </servlet-mapping>


    <!-- post提交中文乱码的问题-->
    <filter>
        <filter-name>CharacterEncodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>forceResponseEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
    </filter>

    <filter-mapping>
        <filter-name>CharacterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>


    <!-- 连接池 启用 Web 监控统计功能    start-->
    <filter>
        <filter-name>DruidWebStatFilter</filter-name >
        <filter-class> com.alibaba.druid.support.http.WebStatFilter </filter-class>
        <init-param>
            <param-name>exclusions </param-name>
            <param-value>*.js ,*.gif ,*.jpg ,*.png ,*.css ,*.ico ,/druid/* </param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>DruidWebStatFilter </filter-name>
        <url-pattern>/*</url-pattern >
    </filter-mapping>
    <servlet>
        <servlet-name>DruidStatView</servlet-name>
        <servlet-class>com.alibaba.druid.support.http.StatViewServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>DruidStatView</servlet-name>
        <url-pattern>/druid/*</url-pattern>
    </servlet-mapping>

</web-app>

```



## 10.4、配置 jdbc.properties 数据相关的文件

```java
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/waimai?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
jdbc.username=root
jdbc.password=root
```



## 10.5、配置 Root WebApplicationContext的内容     spring-application.xml

<img src="35.png">

三层架构（service业务逻辑， repository数据访问，   datasource 数据源， Transaction事务, 事务增强管理）

配置log4j2.xml

```java
<?xml version="1.0" encoding="UTF-8"?>
<Configuration xmlns="http://logging.apache.org/log4j/2.0/config">

    <Appenders>
        <Console name="stdout" target="SYSTEM_OUT">
            <PatternLayout pattern="%5level [%t] - %msg%n"/>
        </Console>
    </Appenders>

    <Loggers>
        <Logger name="com.hxzy" level="debug"/>
        <Root level="error" >
            <AppenderRef ref="stdout"/>
        </Root>
    </Loggers>

</Configuration>
```





新建mybatis-config.xml

```java
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
 
    <settings>
        <!-- 配置日志-->
        <setting name="logImpl" value="LOG4J2"/>
    </settings>

    <!-- 分页插件-->
    <plugins>
        <plugin interceptor="com.github.pagehelper.PageInterceptor">
            <property name="helperDialect" value="mysql"/>
        </plugin>
    </plugins>


</configuration>
```



spring-application.xml 配置

```java

<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd">

 <!--  Root  WebAplicationContext-->
  <!-- 三层（业务逻辑层， 数据访问， 数据库连接池，事务等配置） -->

    <!--开启bean注解扫描-->
    <context:component-scan base-package="com.hxzy">
        <!-- 除了 Controller控制器以外的 -->
        <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>

    <!--加载properties文件-->
    <context:property-placeholder location="classpath*:jdbc.properties"/>

    <!--数据源-->
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource" init-method="init" destroy-method="close">
        <property name="driverClassName" value="${jdbc.driver}"/>
        <property name="url" value="${jdbc.url}"/>
        <property name="username" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
        <!-- 配置监控统计拦截的filters -->
        <property name="filters" value="stat" />
    </bean>

    <!--整合mybatis到spring中-->
    <bean class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"/>
        <property name="typeAliasesPackage" value="com.hxzy.entity"/>
        <!-- 配置其它属性-->
        <property name="configLocation" value="classpath:mybatis-config.xml"></property>
    </bean>

    <!--映射扫描-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="basePackage" value="com.hxzy.mapper"/>
    </bean>

    <!--事务管理器-->
    <bean id="txManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <!--开启注解式事务-->
    <tx:annotation-driven transaction-manager="txManager"/>

</beans>
```



## 10.6、配置前端控制器servlet

<img src="36.png">

控制器， ViewResolver视图解析器( thylemeaf,  jackson，文件上传   ), HandlerMapping映射处理器

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- ServletWebApplicationContext-->
    <!--  servlet类（Controler,  视图解析器(jsp, json...)  ,  HandlerMapping(url映射关系) ） -->

    <context:component-scan base-package="com.hxzy.controller"></context:component-scan>


    <!-- 对静态资源放行, css,js -->
    <mvc:resources mapping="/static/**" location="/static/"></mvc:resources>



    <!--    配置视图解析器 Thymeleaf   -->
    <bean id="templateResolver" class="org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver">
        <!-- 页面存放的位置 -->
        <property name="prefix" value="/WEB-INF/html/" />
        <!-- 文件后缀名叫什么 -->
        <property name="suffix" value=".html" />
        <!--输出的模型，html输出  -->
        <property name="templateMode" value="HTML" />
        <!-- 是不是要缓存-->
        <property name="cacheable" value="false" />
        <!-- 文件编码格式-->
        <property name="characterEncoding" value="UTF-8"/>
    </bean>

    <!-- 模板引擎-->
    <bean id="templateEngine" class="org.thymeleaf.spring5.SpringTemplateEngine">
        <property name="templateResolver" ref="templateResolver" />
    </bean>

    <!--  视图解析器-->
    <bean id="viewResolver" class="org.thymeleaf.spring5.view.ThymeleafViewResolver">
        <property name="templateEngine" ref="templateEngine" />
        <property name="characterEncoding" value="UTF-8"/>
    </bean>


    <!-- 可以使用注解方式来配置web项目 -->
    <mvc:annotation-driven>
        <mvc:message-converters>
            <ref bean="stringHttpMessageConverter"/>
            <ref bean="mappingJackson2HttpMessageConverter"/>
        </mvc:message-converters>
    </mvc:annotation-driven>

    <!--  设置返回字符串编码-->
    <bean id="stringHttpMessageConverter"
          class="org.springframework.http.converter.StringHttpMessageConverter">
        <property name = "supportedMediaTypes">
            <list>
                <value>text/html;charset=UTF-8</value>
                <value>application/json;charset=UTF-8</value>
            </list>
        </property>
    </bean>

    <!--解决IE浏览器json文件下载和json数据中午乱码的问题-->
    <bean id="mappingJackson2HttpMessageConverter"
          class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
        <property name="supportedMediaTypes">
            <list>
                <value>text/html;charset=UTF-8</value>
                <value>application/json;charset=UTF-8</value>
            </list>
        </property>
    </bean>


    <!--  配置文件上传解析器-->
    <bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
       <property name="defaultEncoding" value="UTF-8"></property>
        <!-- 最大支持2MB= 1024*1024*2-->
        <property name="maxUploadSize" value="2097152"></property>
    </bean> 

</beans>
```





## 10.7、使用 mybatis-generator-gui 逆向工具生成底层代码

自已封装通用业务逻辑

```java
public interface IService <Model, PK extends Serializable> {
    int deleteByPrimaryKey(PK id);

    int insert(Model record);

    int insertSelective(Model record);

    Model selectByPrimaryKey(PK id);

    int updateByPrimaryKeySelective(Model record);

    int updateByPrimaryKey(Model record);
}
```



自已封装通用业务逻辑和实现

```java
public abstract class ServiceImpl<Model, PK extends Serializable> implements IService<Model,PK> {

    private MyBatisBaseDao  myBatisBaseDao;

    /**
     * 为父类赋值
     * @param myBatisBaseDao
     */
    public void setMyBatisBaseDao(MyBatisBaseDao myBatisBaseDao) {
        this.myBatisBaseDao = myBatisBaseDao;
    }

    @Override
    public int deleteByPrimaryKey(PK id) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");

        return this.myBatisBaseDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Model record) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return  this.myBatisBaseDao.insert(record);
    }

    @Override
    public int insertSelective(Model record) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return  this.myBatisBaseDao.insertSelective(record);
    }

    @Override
    public Model selectByPrimaryKey(PK id) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return (Model) this.myBatisBaseDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Model record) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return this.myBatisBaseDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Model record) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return this.myBatisBaseDao.updateByPrimaryKey(record);
    }
}
```



## 10.9、测试SSM

<img src="37.png">





```java
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-application.xml")
public class ContamerTest {

    @Autowired
    private ContamerService contamerService;

    @Test
    public void testFindById(){
        Contamer contamer = this.contamerService.selectByPrimaryKey(1);

        System.out.println(contamer.toString());

    }
}

```







# 10.9、引用  hutool工具类

https://hutool.cn/docs/#/



```java
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-all</artifactId>
    <version>5.8.10</version>
</dependency>
```

 

BCrypt加密算法

加密   BCrypt.hashpw("admin8888");

判断两次密码是否一致      boolean    BCrypt.checkpw()("admin8888",    数据库之前加密值);



## 10.10、分页查询步骤

 <img src="38.png">

<img src="39.png">



制作一个通用的控制器

```java
/**
 * 基本控制器
 *
 * @author tonneyyy
 */
public class BaseController {

    /**
     * 分页第一步，开始PageHelper插件的分页
     * @param pageDTO
     */
    protected  void buildPage(PageDTO pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
    }

    /**
     * 第页第三步，把PageHelper返回的结果Page转换自定义的对象
     * @param list
     * @return
     */
    protected R pageToPageVO(List  list) {
        if (list instanceof Page) {
            PageVO pageVo = new PageVO();
            Page pg = ((Page) list);
            pageVo.setTotal(pg.getTotal());
            pageVo.setRows(list);
            return R.okHasData(pageVo);
        } else {
            R r = R.build(AckCode.FAIL);
            r.setMsg("分页对象不正确");
            return r;
        }
    }

}
```



查询数据

```java
/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Controller
public class ContamerController  extends BaseController{

    @Autowired
    private ContamerService contamerService;


    /**
     * 显示页面
     * @return
     */
    @GetMapping(value = "/contamer")
    public String  index(){

         return "contamer/index";
    }

    /**
     * 查询数据返回给前端
     * @param contamerSearch
     * @return
     */
    @ResponseBody
    @GetMapping(value ="/contamer/data" )
    public R searchData(ContamerSearch  contamerSearch){
        //第一步开启分页模式
        super.buildPage(contamerSearch);
        //第二步查询数据
        List<Contamer> list= this.contamerService.search(contamerSearch);
        return super.pageToPageVO(list);
    }

}
```







## 10.11、前端代码

更改对es6 支持

<img src="40.png">



```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>客户管理</title>
    <!-- 引入样式 -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">

</head>
<body>
<div id="app">



    <el-table      :data="tableData.rows"         border          style="width: 100%">
        <el-table-column     prop="id"     label="编号"  width="180"> </el-table-column>
        <el-table-column      prop="loginid" label="账户"  width="180">  </el-table-column>

    </el-table>

    <!-- 分页条 -->
    <el-pagination
            v-if="tableData.total>0"
            @size-change="handleSizeChange"
            @current-change="handleCurrentChange"
            :current-page="queryParam.page"
            :page-sizes="[1, 5, 10, 20,30,50,100]"
            :page-size="queryParam.size"
            layout="total, sizes, prev, pager, next, jumper"
            :total="tableData.total">
    </el-pagination>

</div>

</body>
<!-- 开发环境版本，包含了有帮助的命令行警告 -->
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<!-- 引入组件库 -->
<script src="https://unpkg.com/element-ui/lib/index.js"></script>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>

    var app = new Vue({
        el: '#app',
        data: {
            tableData: {
                rows:[],
                total: 0
            },
            queryParam: {
                page:1,
                size:10
            }
        },
        methods: {
            fetchData(){
                axios.get('/contamer/data', {
                    params: this.queryParam
                }) .then( resp => {
                    const respData = resp.data
                    if(respData.code===200){
                        this.tableData = respData.data
                    }else{
                        //提示错误
                        this.$message.error( json.msg);
                    }
                })
                 .catch( e =>{
                     this.$message.error( e);
               } );
            },
            handleSizeChange(val) {
                //每页几笔
                this.queryParam.page=1
                this.queryParam.size = val
                this.fetchData()
            },
            handleCurrentChange(val) {
                this.queryParam.page=val
                this.fetchData()
            }
        },
        created(){
            this.fetchData()
        }
    })


</script>
</html>
```









# mybatis是如何执行的(背)

<img src="19.png">



```java
(1)读取MyBatis配置文件mybatis-config.xml。mybatis-config.xml作为MyBatis的全局配置文件，配置了MyBatis的运行环境等信息，其中主要内容是获取数据库连接。

(2)加载映射文件Mapper.xml。Mapper.xml文件即SQL映射文件，该文件中配置了操作数据库的SQL语句，需要在mybatis-config.xml中加载才能执行。mybatis-config.xml可以加载多个配置文件，每个配置文件对应数据库中的一张表。

(3)构建会话工厂。通过MyBatis的环境等配置信息构建会话工厂SqlSessionFactory。

(4)创建SqlSession对象。由会话工厂创建SqlSession对象，该对象中包含了执行SQL的所有方法。

(5)MyBatis底层定义了一个Executor接口来操作数据库，它会根据SqlSession传递的参数动态的生成需要执行的SQL语句，同时负责查询缓存的维护。

(6)在Executor接口的执行方法中，包含一个MappedStatement类型的参数，该参数是对映射信息的封装，用来存储要映射的SQL语句的id、参数等。Mapper.xml文件中一个SQL对应一个MappedStatement对象，SQL的id即是MappedStatement的id。

(7)输入参数映射。在执行方法时，MappedStatement对象会对用户执行SQL语句的输入参数进行定义(可以定义为Map、List类型、基本类型和POJO类型)，Executor执行器会通过MappedStatement对象在执行SQL前，将输入的Java对象映射到SQL语句中。这里对输入参数的映射过程就类似于JDBC编程中对preparedStatement对象设置参数的过程。

(8)输出结果映射。在数据库中执行完SQL语句后，MappedStatement对象会对SQL执行输出的结果进行定义(可以定义为Map和List类型、基本类型、POJO类型)，Executor执行器会通过MappedStatement对象在执行SQL语句后，将输出结果映射至Java对象中。这种将输出结果映射到Java对象的过程就类似于JDBC编程中对结果的解析处理过程。

```











1)、创建SqlSessionFactory工厂，自动读取并解析 mybatis的XML的配置文件  （ Configuration  配置管理类  通过XMLConfigBuilder解析 mybatis-config.xml ）

产生出 一个名为DefaultSqlSessionFactory 的工厂实现类

```java
String resource = "org/mybatis/example/mybatis-config.xml";
InputStream inputStream = Resources.getResourceAsStream(resource);
 SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
```

​    

2)、通过SqlSessionFactory工厂来构造会话对象SqlSession，执行器

SqlSession  session=SqlSessionFactory.openSession();     

```
  Transaction tx = null;     事务
    try {
      final Environment environment = configuration.getEnvironment();  (事务，DataSource) 
      //开启事务
      final TransactionFactory transactionFactory = getTransactionFactoryFromEnvironment(environment);
      // 设定事务提交方案        autoCommit=true自动的是交事务， false 手动提交事务 (设定数据库，让这个事务手动提交 set autocommit=0;)
      tx = transactionFactory.newTransaction(environment.getDataSource(), level, autoCommit);
      // 开一个jdbc执行sql的对象方案  ， SIMPLE（Statement 默认的）, REUSE(PrepreStatement), BATCH(批量提交)
      final Executor executor = configuration.newExecutor(tx, execType);
      return new DefaultSqlSession(configuration, executor, autoCommit);
    } catch (Exception e) {
      closeTransaction(tx); // may have fetched a connection so lets call close()
      throw ExceptionFactory.wrapException("Error opening session.  Cause: " + e, e);
    } finally {
      ErrorContext.instance().reset();
    }
```



3)、通过Executor执行器 ( SIMPLE（Statement 默认的）, REUSE(PrepreStatement), BATCH(批量提交) )



4)、加载你到底要执行的sql是什么    MappedStatement     -->     sqlSession.getMapper(实体类Mapper.class);得到代理对象

​     Blog blog = (Blog) session.selectOne("org.mybatis.example.BlogMapper.selectBlog", 101);

 或者

BlogMapper  mapper=   session.getMapper(BlogMapper.class);

Blog blog=mapper.selectBlog(101);



 解析sql，并为sql赋值，向数据库发送sql语句，接收数据库返回结果，组装成我们自定义的resultType 或 resultMap的结果

```
<select id="selectByPrimaryKey" parameterType="java.math.BigDecimal" resultMap="BaseResultMap">
    select 
    <include refid="Base_Column_List" />
    from SYS_DICT_DATA
    where DICT_CODE = #{dictCode,jdbcType=DECIMAL}
  </select>
```



 ## mybatis的常用标签   mapper.xml  (背)

<insert>    <delete>    <update>   <select>     <resultMap>      <include>    

动态sql标签

   <trim>(   <where>,<set>)     <if >   <foreach>     <choose> when  otherwise 

<foreach      > 有哪些属性?

```
 foreach item="变量名/value" index="下标/key" collection="集合/数据/Map"     open="("   separator="," close=")"
```



## mybatis中的  ${} 和#{}的区别(背)

#{}  占位符，可以预仿简单的sql注入，     只能在where标签里面用       values()   set  

 ${}直接取值，有sql注入风险                    可以在任何地方用











