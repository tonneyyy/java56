package com.hxzy;

import cn.hutool.crypto.digest.BCrypt;
import com.hxzy.entity.Contamer;
import com.hxzy.service.ContamerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-application.xml")
public class ContamerTest {

    @Autowired
    private ContamerService contamerService;

    @Test
    public void testFindById(){
        Contamer contamer = this.contamerService.selectByPrimaryKey(1);

        System.out.println(contamer.toString());

    }

    @Test
    public void testAdd(){
        Contamer contamer = this.contamerService.selectByPrimaryKey(1);
        contamer.setId(null);
        contamer.setLoginid("wangyi");

        String pwd= BCrypt.hashpw("admin8888");
        contamer.setPasswold(pwd);
        contamer.setUserName("飞鸟");

        //新增
        int insert = this.contamerService.insert(contamer);
        System.out.println(insert);
    }
}
