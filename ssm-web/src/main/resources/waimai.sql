/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : waimai

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2022-06-30 16:51:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `address_book`
-- ----------------------------
DROP TABLE IF EXISTS `address_book`;
CREATE TABLE `address_book` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `consignee` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '收货人',
  `sex` tinyint(4) NOT NULL COMMENT '性别 0 女 1 男',
  `phone` varchar(11) COLLATE utf8_bin NOT NULL COMMENT '手机号',
  `province_code` varchar(12) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '省级区划编号',
  `province_name` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '省级名称',
  `city_code` varchar(12) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '市级区划编号',
  `city_name` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '市级名称',
  `district_code` varchar(12) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '区级区划编号',
  `district_name` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '区级名称',
  `detail` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '详细地址',
  `label` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '标签',
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '默认 0 否 1是',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `create_by` bigint(20) NOT NULL COMMENT '创建人',
  `update_by` bigint(20) NOT NULL COMMENT '修改人',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='地址管理';

-- ----------------------------
-- Records of address_book
-- ----------------------------

-- ----------------------------
-- Table structure for `captcha`
-- ----------------------------
DROP TABLE IF EXISTS `captcha`;
CREATE TABLE `captcha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(80) NOT NULL COMMENT '验证码唯一标识',
  `value` varchar(20) NOT NULL COMMENT '验证码图片生成的值',
  `expired_time` datetime NOT NULL COMMENT '验证码过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COMMENT='存储图片验证码的表';

-- ----------------------------
-- Records of captcha
-- ----------------------------

-- ----------------------------
-- Table structure for `contamer`
-- ----------------------------
DROP TABLE IF EXISTS `contamer`;
CREATE TABLE `contamer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `loginId` varchar(255) NOT NULL COMMENT '账号',
  `passwold` varchar(255) NOT NULL COMMENT '密码加密',
  `user_name` varchar(255) NOT NULL COMMENT '昵称',
  `gender` tinyint(4) NOT NULL COMMENT '性别(0 女 1 男)',
  `portrait` varchar(255) NOT NULL COMMENT '头像',
  `status` int(10) NOT NULL COMMENT '状态1：停用0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='前端用户';

-- ----------------------------
-- Records of contamer
-- ----------------------------
INSERT INTO `contamer` VALUES ('1', 'test', '$2a$10$MFpcQKu19rpcQ0zMmHYnce1w5wVzvTEakmAbQpeI2HbSaT698EdG2', '小心心', '0', 'http://192.168.25.180:8888/group1/M00/00/00/wKgZtGKsOzKAPos8AAcVeF0ebQM222.jpg', '0', '2022-06-27 14:26:29', '小心心', null, '后台员工');

-- ----------------------------
-- Table structure for `dish_flavor`
-- ----------------------------
DROP TABLE IF EXISTS `dish_flavor`;
CREATE TABLE `dish_flavor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dish_id` bigint(20) NOT NULL COMMENT '菜品',
  `name` varchar(80) COLLATE utf8_bin NOT NULL COMMENT '口味名称',
  `value` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '口味数据list',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `create_by` varchar(80) COLLATE utf8_bin NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_by` varchar(80) COLLATE utf8_bin DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='菜品口味关系表';

-- ----------------------------
-- Records of dish_flavor
-- ----------------------------
INSERT INTO `dish_flavor` VALUES ('1', '4', '辣度', '[\"不辣\",\"微辣\",\"中辣\",\"重辣\"]', '0', '川菜馆', '2022-06-23 11:44:51', '2022-06-23 11:44:51', null);
INSERT INTO `dish_flavor` VALUES ('2', '4', '甜味', '[\"无糖\",\"少糖\",\"半糖\",\"多糖\",\"全糖\"]', '0', '川菜馆', '2022-06-23 11:44:51', '2022-06-23 11:44:51', null);
INSERT INTO `dish_flavor` VALUES ('5', '5', '甜味', '[\"少糖\",\"半糖\",\"多糖\"]', '0', '川菜馆', '2022-06-23 14:50:42', '2022-06-23 14:50:42', '川菜馆');
INSERT INTO `dish_flavor` VALUES ('6', '5', '辣度', '[\"不辣\",\"微辣\",\"中辣\"]', '0', '川菜馆', '2022-06-23 14:50:42', '2022-06-23 14:50:42', '川菜馆');

-- ----------------------------
-- Table structure for `employee`
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `login_name` varchar(50) NOT NULL COMMENT '登录账户',
  `login_pwd` varchar(80) NOT NULL COMMENT '登录密码',
  `gender` tinyint(4) NOT NULL COMMENT '性别(0 女 1 男)',
  `status` tinyint(4) NOT NULL COMMENT '状态（1停用，0正常）',
  `avatar` varchar(500) DEFAULT NULL COMMENT '头像',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(50) NOT NULL COMMENT '创建人',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改人',
  `token` varchar(80) DEFAULT NULL COMMENT '令牌',
  `token_expired` datetime DEFAULT NULL COMMENT '令牌过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='后台管理员表';

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('1', '13688888888', '$2a$10$bpbqF88g8R51PpkdxKjowupyupkEfdveiuIiLnQZO0MCq/tZktK3K', '1', '0', null, '2022-06-10 16:22:01', 'admin', '2022-06-30 14:29:03', 'admin', 'f67ad1e3-0c96-475f-9926-0c278ff16745', '2022-06-30 15:29:03');
INSERT INTO `employee` VALUES ('2', '13600000000', '$2a$10$xVqjtZwUhHWIupfEHSqHyOmQRlICjJOYZAvdPvaco6tP/XuXVFhMa', '0', '0', 'http://192.168.25.180:8888/group1/M00/00/00/wKgZtGKsJL2AZlscAABRt-dLJqY620.jpg', '2022-06-10 16:22:58', 'admin', '2022-06-22 10:03:39', '13688888888', null, null);
INSERT INTO `employee` VALUES ('3', '13611111111', '$2a$10$MFpcQKu19rpcQ0zMmHYnce1w5wVzvTEakmAbQpeI2HbSaT698EdG2', '0', '0', 'http://192.168.25.180:8888/group1/M00/00/00/wKgZtGKsJHKAUpFVAABXquEKWTU840.jpg', '2022-06-17 10:37:56', '13688888888', '2022-06-20 10:31:56', '13688888888', null, null);

-- ----------------------------
-- Table structure for `merchandise`
-- ----------------------------
DROP TABLE IF EXISTS `merchandise`;
CREATE TABLE `merchandise` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '商品名称',
  `price` float(10,2) NOT NULL COMMENT '商品价格',
  `picture` varchar(200) DEFAULT NULL COMMENT '商品图片',
  `description` varchar(255) DEFAULT NULL COMMENT '商品描述',
  `score` int(10) DEFAULT NULL COMMENT '商品综合评分',
  `b_id` bigint(20) NOT NULL COMMENT '商家id',
  `m_id` bigint(20) NOT NULL COMMENT '商品分类',
  `isgrounding` int(11) NOT NULL COMMENT '是否上架 1：上架 ，0：没上架',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(50) NOT NULL COMMENT '创建人',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='商品信息';

-- ----------------------------
-- Records of merchandise
-- ----------------------------
INSERT INTO `merchandise` VALUES ('1', '宫保鸡丁', '26.00', null, null, null, '1', '1', '0', '2022-05-05 00:00:00', 'admin', '2022-06-22 14:10:56', 'admin');
INSERT INTO `merchandise` VALUES ('4', '龙聚德烤鸭套饭', '25.00', 'http://192.168.20.180:8888/group1/M00/00/01/wKgUtGKz1vyAOD_8AAMzD9FhuaI231.png', '111', '0', '1', '1', '1', '2022-06-23 11:44:50', '川菜馆', '2022-06-23 14:53:32', '川菜馆');
INSERT INTO `merchandise` VALUES ('5', '韩式炸鸡比萨', '20.00', 'http://192.168.20.180:8888/group1/M00/00/01/wKgUtGK0A-aAWhWgAACxvegoSoY158.jpg', '8寸,适合个人使用', '0', '1', '2', '1', '2022-06-23 14:31:38', '川菜馆', '2022-06-23 14:50:42', '川菜馆');

-- ----------------------------
-- Table structure for `merchandise_class`
-- ----------------------------
DROP TABLE IF EXISTS `merchandise_class`;
CREATE TABLE `merchandise_class` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '类别名称',
  `picture` varchar(255) DEFAULT NULL COMMENT '口味图片',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(50) NOT NULL COMMENT '创建人',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COMMENT='商品分类表';

-- ----------------------------
-- Records of merchandise_class
-- ----------------------------
INSERT INTO `merchandise_class` VALUES ('1', '盖浇饭', 'http://192.168.25.180:8888/group1/M00/00/00/wKgZtGKq6eCAen7PAAA32zPRTiY50.jpeg', '2022-06-16 14:40:19', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('2', '汉堡薯条', 'http://192.168.25.180:8888/group1/M00/00/00/wKgZtGKq6eCAen7PAAA32zPRTiY50.jpeg', '2022-06-16 14:40:51', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('3', '饺子', 'http://192.168.25.180:8888/group1/M00/00/00/wKgZtGKrHzGAA5BzAAC1izulBrk745.jpg', '2022-06-16 20:17:00', 'admin', '2022-06-18 14:46:05', '13688888888');
INSERT INTO `merchandise_class` VALUES ('4', '东北菜', null, '2022-06-23 15:11:49', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('5', '瓦罐汤', null, '2022-06-23 15:12:09', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('6', '花甲粉', null, '2022-06-23 15:12:29', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('7', '馄饨抄手', null, '2022-06-23 15:13:12', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('8', '牛肉汤', null, '2022-06-23 15:13:37', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('9', '黄焖鸡米饭', null, '2022-06-23 15:14:04', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('10', '酸辣粉', null, '2022-06-23 15:14:24', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('11', '煲仔饭', null, '2022-06-23 15:14:44', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('12', '咖喱饭', null, '2022-06-23 15:14:58', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('13', '沙县小吃', null, '2022-06-23 15:15:08', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('14', '炒饭', null, '2022-06-23 15:15:18', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('15', '冒菜', null, '2022-06-23 15:15:26', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('16', '煎饼', null, '2022-06-23 15:15:40', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('17', '螺蛳粉', null, '2022-06-23 15:15:56', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('18', '米粉米线', null, '2022-06-23 15:16:06', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('19', '烤肉拌饭', null, '2022-06-23 15:16:38', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('20', '披萨', null, '2022-06-23 15:16:54', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('21', '驴肉火烧', null, '2022-06-23 15:17:07', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('22', '生煎', null, '2022-06-23 15:17:22', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('23', '重庆小面', null, '2022-06-23 15:17:31', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('24', '热干面', null, '2022-06-23 15:17:44', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('25', '兰州拉面', null, '2022-06-23 15:18:02', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('26', '凉皮/凉粉', null, '2022-06-23 15:18:30', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('27', '麻辣汤/关东煮', null, '2022-06-23 15:18:55', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('28', '豆浆油条', null, '2022-06-23 15:19:41', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('29', '烧烤', null, '2022-06-23 15:20:10', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('30', '炸鸡炸串', null, '2022-06-23 15:20:22', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('31', '私房菜', null, '2022-06-23 15:20:39', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('32', '海鲜', null, '2022-06-23 15:20:54', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('33', '港菜/茶餐厅', null, '2022-06-23 15:21:16', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('34', '清真菜', null, '2022-06-23 15:21:26', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('35', '牛排', null, '2022-06-23 15:21:56', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('36', '小火锅', null, '2022-06-23 15:22:08', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('37', '韩国料理', null, '2022-06-23 15:22:24', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('38', '日本料理', null, '2022-06-23 15:22:34', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('39', '韩式烤肉', null, '2022-06-23 15:22:44', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('40', '麻辣香锅', null, '2022-06-23 15:23:03', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('41', '印度菜', null, '2022-06-23 15:23:30', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('42', '意大利菜', null, '2022-06-23 15:23:46', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('43', '西餐', null, '2022-06-23 15:23:57', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('44', '焖锅', null, '2022-06-23 15:24:21', 'admin', null, null);
INSERT INTO `merchandise_class` VALUES ('45', '凉面/稀饭', null, '2022-06-23 15:24:53', 'admin', null, null);

-- ----------------------------
-- Table structure for `merchandiser`
-- ----------------------------
DROP TABLE IF EXISTS `merchandiser`;
CREATE TABLE `merchandiser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '商家名称',
  `address` varchar(255) DEFAULT NULL COMMENT '商家地址',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `picture` varchar(255) DEFAULT NULL COMMENT '商家图片',
  `longitude` decimal(10,6) DEFAULT NULL COMMENT ' 商家位置经度',
  `latitude` decimal(10,6) DEFAULT NULL COMMENT '商家位置纬度',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by` varchar(50) DEFAULT NULL COMMENT '修改人',
  `login_name` varchar(50) NOT NULL COMMENT '商家账户',
  `login_pwd` varchar(80) NOT NULL COMMENT '商家密码',
  `token` varchar(80) DEFAULT NULL COMMENT '令牌',
  `token_expired` datetime DEFAULT NULL COMMENT '令牌过期时间',
  `status` tinyint(4) DEFAULT '1' COMMENT '审核状态（0已审核，1未审核）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='商家信息';

-- ----------------------------
-- Records of merchandiser
-- ----------------------------
INSERT INTO `merchandiser` VALUES ('1', '川菜馆', '大竹林', '13312345678', 'http://192.168.20.180:8888/group1/M00/00/00/wKgUtGKxflWAT5nBAAFE9mtR-h0648.png', '106.472962', '29.631662', '2022-05-05 00:00:00', 'admin', '2022-06-23 14:55:45', '13688888888', 'ccg', '$2a$10$MFpcQKu19rpcQ0zMmHYnce1w5wVzvTEakmAbQpeI2HbSaT698EdG2', null, null, '0');
INSERT INTO `merchandiser` VALUES ('2', '鲜鲜肥肠饭', '重庆市南岸区黄金公路鲜龙井火锅(南山总店)', '13688888888', 'http://192.168.20.180:8888/group1/M00/00/00/wKgUtGKxfkmAFOIsAAP8fFwrjss46.jpeg', '106.597010', '29.497220', '2022-06-17 16:29:42', 'admin', '2022-06-22 10:10:18', '13688888888', 'fef', '$2a$10$MFpcQKu19rpcQ0zMmHYnce1w5wVzvTEakmAbQpeI2HbSaT698EdG2', null, null, '0');
INSERT INTO `merchandiser` VALUES ('3', '小王烧烤店', '重庆市渝北区绿竹路中国石化加油站(大竹林站)西南侧约40米齐禧火锅馆', '13608368217', 'http://192.168.20.180:8888/group1/M00/00/02/wKgUtGK1ga-AHlTQAAMJh9Xwkjc700.jpg', '106.471572', '29.627842', '2022-06-24 17:19:52', '13608368217', '2022-06-24 17:19:52', null, '13608368217', '$2a$10$LFZHRwYlorcn3F61tX2zgOBNMMUxEvzfFlaCeNT11CmhouftAFhpC', null, null, '1');
INSERT INTO `merchandiser` VALUES ('4', '周记农家木桶饭（世茂店）', '重庆市渝北区湖山路世茂茂悦府-一期周记农家木桶饭(世茂店)', '19923699604', 'http://192.168.20.160:8888/group1/M00/00/00/wKgUoGK6mxyAb83fAAPaiRBtUeg311.jpg', '106.526092', '29.658492', '2022-06-28 14:09:34', '19923699604', '2022-06-28 14:10:44', null, '19923699604', '$2a$10$tEfE.7yHkepAWKtUbWzce./UTYdLH/4/35xSTBSR9.YSCqV0h19DG', '2b2ee412-9a60-41e6-897e-dae635e0c9c4', '2022-06-28 15:10:44', '0');

-- ----------------------------
-- Table structure for `order_detail`
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '名字',
  `image` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '图片',
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `dish_id` bigint(20) DEFAULT NULL COMMENT '菜品id',
  `m_id` bigint(20) DEFAULT NULL COMMENT 's商家ID',
  `dish_flavor` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '口味',
  `number` int(11) NOT NULL DEFAULT '1' COMMENT '数量',
  `amount` decimal(10,2) NOT NULL COMMENT '金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='订单明细表';

-- ----------------------------
-- Records of order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `number` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '订单号',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '订单状态 1待付款，2待派送，3已派送，4已完成，5已取消',
  `user_id` bigint(20) NOT NULL COMMENT '下单用户id',
  `m_id` bigint(20) DEFAULT NULL COMMENT '商家ID',
  `address_book_id` bigint(20) NOT NULL COMMENT '地址id',
  `order_time` datetime NOT NULL COMMENT '下单时间',
  `checkout_time` datetime NOT NULL COMMENT '结账时间',
  `pay_method` int(11) NOT NULL DEFAULT '1' COMMENT '支付方式 1微信,2支付宝',
  `amount` decimal(10,2) NOT NULL COMMENT '实收金额',
  `remark` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  `phone` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '收获人电话',
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '收获地址',
  `user_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '收获人用户名',
  `consignee` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '收货人姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='订单表';

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for `shopping_cart`
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
  `image` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '图片',
  `user_id` bigint(20) NOT NULL COMMENT '主键',
  `dish_id` bigint(20) DEFAULT NULL COMMENT '菜品id',
  `setmeal_id` bigint(20) DEFAULT NULL COMMENT '套餐id',
  `dish_flavor` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '口味',
  `number` int(11) NOT NULL DEFAULT '1' COMMENT '数量',
  `amount` decimal(10,2) NOT NULL COMMENT '金额',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='购物车';

-- ----------------------------
-- Records of shopping_cart
-- ----------------------------

-- ----------------------------
-- Table structure for `sms_info`
-- ----------------------------
DROP TABLE IF EXISTS `sms_info`;
CREATE TABLE `sms_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `code` varchar(10) DEFAULT NULL COMMENT '短信验证码',
  `expired_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sms_info
-- ----------------------------
