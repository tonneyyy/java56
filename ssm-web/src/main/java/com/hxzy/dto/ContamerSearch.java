package com.hxzy.dto;

import com.hxzy.common.dto.PageDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * 前端客户查询
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class ContamerSearch extends PageDTO {

    /**
     * 账号
     */
    private String loginid;

    /**
     * 性别(0 女 1 男)
     */
    private Byte gender;

    /**
     * 状态1：停用0：正常
     */
    private Integer status;


}
