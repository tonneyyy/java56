package com.hxzy.mapper;

import com.hxzy.entity.MerchandiseClass;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * MerchandiseClassMapper继承基类
 */
@Mapper
@Repository
public interface MerchandiseClassMapper extends MyBatisBaseDao<MerchandiseClass, Long> {
}