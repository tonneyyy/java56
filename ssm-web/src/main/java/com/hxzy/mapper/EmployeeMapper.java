package com.hxzy.mapper;

import com.hxzy.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;

/**
 * EmployeeMapper继承基类
 */
@Mapper
@Repository
public interface EmployeeMapper extends MyBatisBaseDao<Employee, Long> {

    /**
     * 根据用户名来查询用户信息
     * @param loginName
     * @return
     */
    @Select(value = "select id, login_name as loginName, login_pwd as loginPwd, gender, `status`, avatar from employee where login_name=#{loginName}")
    Employee findByLoginName(  String loginName);
}