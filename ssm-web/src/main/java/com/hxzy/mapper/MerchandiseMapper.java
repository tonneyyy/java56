package com.hxzy.mapper;

import com.hxzy.entity.Merchandise;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * MerchandiseMapper继承基类
 */
@Mapper
@Repository
public interface MerchandiseMapper extends MyBatisBaseDao<Merchandise, Long> {
}