package com.hxzy.mapper;

import com.hxzy.dto.ContamerSearch;
import com.hxzy.entity.Contamer;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ContamerMapper继承基类
 */
@Mapper
@Repository
public interface ContamerMapper extends MyBatisBaseDao<Contamer, Integer> {

    /**
     * 分页查询
     * @param contamerSearch
     * @return
     */
    List<Contamer> search(ContamerSearch contamerSearch);
}