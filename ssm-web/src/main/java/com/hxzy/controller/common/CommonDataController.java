package com.hxzy.controller.common;

import com.hxzy.common.controller.BaseController;
import com.hxzy.common.vo.CommonVO;
import com.hxzy.common.vo.R;
import com.hxzy.util.CommonDataUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 通用数据
 *
 * @author tonneyyy
 */
@RestController
@RequestMapping(value = "/api")
public class CommonDataController extends BaseController {

    @GetMapping(value = "/common/data")
    public R loadData(){
        Map<String ,List<CommonVO>>  mp=new HashMap<>();
        mp.put("sexData", CommonDataUtil.sexDataList());
        mp.put("stateData",CommonDataUtil.stateDataList());
        return R.okHasData(mp);
    }


}
