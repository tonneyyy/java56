package com.hxzy.controller.admin;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hxzy.common.controller.BaseController;
import com.hxzy.common.vo.PageVO;
import com.hxzy.common.vo.R;
import com.hxzy.dto.ContamerSearch;
import com.hxzy.entity.Contamer;
import com.hxzy.service.ContamerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 客户API
 *
 * @author tonneyyy
 */
@RestController
@RequestMapping(value = "/api")
public class ContamerController  extends BaseController{

    @Autowired
    private ContamerService contamerService;

    /**
     * 查询数据返回给前端
     * @param contamerSearch
     * @return
     */
    @GetMapping(value ="/contamer/data" )
    public R searchData(ContamerSearch  contamerSearch){
        //第一步开启分页模式
        super.buildPage(contamerSearch);
        //第二步查询数据
        List<Contamer> list= this.contamerService.search(contamerSearch);
        return super.pageToPageVO(list);
    }

}
