package com.hxzy.service.impl;

import com.hxzy.dto.ContamerSearch;
import com.hxzy.entity.Contamer;
import com.hxzy.mapper.ContamerMapper;
import com.hxzy.service.ContamerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 前端用户业务逻辑
 *
 * @author tonneyyy
 */
@Service
public class ContamerServiceImpl extends ServiceImpl<Contamer,Integer>  implements ContamerService {

    private ContamerMapper  contamerMapper;

    @Autowired
    public void setContamerMapper(ContamerMapper contamerMapper) {
        this.contamerMapper = contamerMapper;
        super.setMyBatisBaseDao(contamerMapper);
    }

    @Override
    public List<Contamer> search(ContamerSearch contamerSearch) {
        return this.contamerMapper.search(contamerSearch);
    }


}
