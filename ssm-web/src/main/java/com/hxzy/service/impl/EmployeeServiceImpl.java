package com.hxzy.service.impl;

import cn.hutool.crypto.digest.BCrypt;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.exception.ServiceException;
import com.hxzy.dto.LoginDTO;
import com.hxzy.entity.Employee;
import com.hxzy.mapper.EmployeeMapper;
import com.hxzy.service.EmployeeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Log4j2
@Service
public class EmployeeServiceImpl extends ServiceImpl<Employee,Integer> implements EmployeeService {

    private EmployeeMapper employeeMapper;

    @Autowired
    public void setEmployeeMapper(EmployeeMapper employeeMapper) {
        this.employeeMapper = employeeMapper;
        super.setMyBatisBaseDao(employeeMapper);
    }

    @Override
    public Employee login(LoginDTO loginDTO) {
        Employee  dbEmployee=  this.employeeMapper.findByLoginName(loginDTO.getLoginName());
        if(dbEmployee==null){
            log.info(loginDTO.getLoginName()+","+AckCode.USER_NOT_FOUND.getMsg());
            throw new ServiceException(AckCode.USERNAME_PASSWORD_ERROR);
        }

        //判断密码
        boolean checkpw = BCrypt.checkpw(loginDTO.getLoginPwd(), dbEmployee.getLoginPwd());
        if(!checkpw){
            log.info(loginDTO.getLoginName()+","+AckCode.LOGIN_PASSWORD_ERROR.getMsg());
            throw new ServiceException(AckCode.USERNAME_PASSWORD_ERROR);
        }

        //判断状态
        if(dbEmployee.getStatus().equals(1)){
            log.info(loginDTO.getLoginName()+","+AckCode.DEVICE_BANNED.getMsg());
            throw new ServiceException(AckCode.DEVICE_BANNED);
        }

        return dbEmployee;
    }
}
