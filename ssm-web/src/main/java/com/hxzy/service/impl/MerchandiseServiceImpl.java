package com.hxzy.service.impl;

import com.hxzy.entity.Merchandise;
import com.hxzy.mapper.MerchandiseMapper;
import com.hxzy.service.MerchandiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商品信息业务逻辑实现
 *
 * @author tonneyyy
 */
@Service
public class MerchandiseServiceImpl extends ServiceImpl<Merchandise,Integer> implements MerchandiseService {

    private MerchandiseMapper merchandiseMapper;

    @Autowired
    public void setMerchandiseMapper(MerchandiseMapper merchandiseMapper) {
        this.merchandiseMapper = merchandiseMapper;
        super.setMyBatisBaseDao(merchandiseMapper);
    }
}