package com.hxzy.service;

import com.hxzy.dto.LoginDTO;
import com.hxzy.entity.Employee;

import javax.validation.Valid;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
public interface EmployeeService extends IService<Employee,Integer> {
    Employee login( LoginDTO loginDTO);
}
