package com.hxzy.service;

import java.io.Serializable;

/**
 * 通用业务逻辑
 *
 * @author tonneyyy
 */
public interface IService <Model, PK extends Serializable> {
    int deleteByPrimaryKey(PK id);

    int insert(Model record);

    int insertSelective(Model record);

    Model selectByPrimaryKey(PK id);

    int updateByPrimaryKeySelective(Model record);

    int updateByPrimaryKey(Model record);
}
