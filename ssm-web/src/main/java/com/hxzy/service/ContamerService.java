package com.hxzy.service;

import com.hxzy.dto.ContamerSearch;
import com.hxzy.entity.Contamer;

import java.util.List;

/**
 * 前端客户
 *
 * @author tonneyyy
 */
public interface ContamerService extends IService<Contamer,Integer> {

    /**
     * 分页查询
     * @param contamerSearch
     * @return
     */
    List<Contamer> search(ContamerSearch contamerSearch);
}
