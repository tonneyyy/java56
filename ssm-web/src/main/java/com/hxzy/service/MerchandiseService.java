package com.hxzy.service;

import com.hxzy.entity.Merchandise;

/**
 * 商品信息业务逻辑接口
 *
 * @author tonneyyy
 */
public interface MerchandiseService extends IService<Merchandise,Integer> {
}
