package com.hxzy.config;

import com.hxzy.common.enums.AckCode;
import com.hxzy.common.exception.ServiceException;
import com.hxzy.common.vo.R;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * springmvc 全局异常处理的类
 *
 * @author tonneyyy
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BindException.class)
    public R bindingResult(BindException bindException){
        List<ObjectError> allErrors = bindException.getAllErrors();
        String msg=allErrors.get(0).getDefaultMessage();
        R r= R.build(AckCode.FAIL);
        r.setMsg(msg);
        return r;
    }

    /**
     * 自定义异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = ServiceException.class)
    public R serviceException(ServiceException e){
        R r= R.build(e.getAckCode());
        return r;
    }
}
