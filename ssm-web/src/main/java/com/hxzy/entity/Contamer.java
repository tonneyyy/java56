package com.hxzy.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * @author 
 * 前端用户
 */
@Data
public class Contamer implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 账号
     */
    private String loginid;

    /**
     * 密码加密
     */
    private String passwold;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 性别(0 女 1 男)
     */
    private Byte gender;

    /**
     * 头像
     */
    private String portrait;

    /**
     * 状态1：停用0：正常
     */
    private Integer status;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    private static final long serialVersionUID = 1L;
}