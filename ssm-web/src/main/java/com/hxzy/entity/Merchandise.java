package com.hxzy.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * @author 
 * 商品信息
 */
@Data
public class Merchandise implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品价格
     */
    private Float price;

    /**
     * 商品图片
     */
    private String picture;

    /**
     * 商品描述
     */
    private String description;

    /**
     * 商品综合评分
     */
    private Integer score;

    /**
     * 商家id
     */
    private Long bId;

    /**
     * 商品分类
     */
    private Long mId;

    /**
     * 是否上架 1：上架 ，0：没上架
     */
    private Integer isgrounding;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    private static final long serialVersionUID = 1L;
}