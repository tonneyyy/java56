package com.hxzy.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * @author 
 * 后台管理员表
 */
@Data
public class Employee implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 登录账户
     */
    private String loginName;

    /**
     * 登录密码
     */
    private String loginPwd;

    /**
     * 性别(0 女 1 男)
     */
    private Byte gender;

    /**
     * 状态（1停用，0正常）
     */
    private Byte status;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;



    private static final long serialVersionUID = 1L;
}