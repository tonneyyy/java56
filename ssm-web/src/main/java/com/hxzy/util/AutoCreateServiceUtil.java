package com.hxzy.util;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 自动创建业务逻辑层的代码使用 页面静态化技术
 *
 * @author tonneyyy
 */
public class AutoCreateServiceUtil implements  Runnable{

     /*
        实体类名 entityName  大写
        实体类名小写字母开头  lowerEntityName
        实体类主键类型  primaryKeyType
        描述descript
         */
     private String entityName;
     private String lowerEntityName;
     private String primaryKeyType;
     private String descript;

     private final String SERVICE_DIR="D:\\java56_code\\java56\\ssm-web\\src\\main\\java\\com\\hxzy\\service\\";
    /**
     *  构造自动生成的代码
     * @param entityName 实体类名
     * @param primaryKeyType 实体类名小写字母开头
     * @param descript 描述
     */
    public AutoCreateServiceUtil(String entityName, String primaryKeyType, String descript) {
        this.entityName = entityName;
        this.primaryKeyType = primaryKeyType;
        this.descript = descript;
        //小写字母
        this.lowerEntityName= this.entityName.substring(0,1).toLowerCase()+this.entityName.substring(1);
    }


    private void createServiceJava() throws IOException {
        //模板引擎
        TemplateEngine engine = new TemplateEngine();
        //读取磁盘中的模板文件
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        //路径
        resolver.setPrefix("template/");
        //后缀
        resolver.setSuffix(".txt");
        //设置模板模式、默认是HTML
        resolver.setTemplateMode("TEXT");
        //设置引擎使用 resolve
        engine.setTemplateResolver(resolver);
        //准备数据 使用context
        Context context = new Context();
        //添加基本类型
        /*
        实体类名 entityName  大写
        实体类名小写字母开头  lowerEntityName
        实体类主键类型  primaryKeyType
        描述 descript
         */
        context.setVariable("entityName",this.entityName);
        context.setVariable("lowerEntityName",this.lowerEntityName);
        context.setVariable("primaryKeyType",this.primaryKeyType);
        context.setVariable("descript",this.descript);
        //生成内容变为字符串
        String out = engine.process("ServiceTemplate",context);
       // System.out.printf(out);
        // 把out写入到指定目录
        FileOutputStream outputStream=new FileOutputStream(new File(SERVICE_DIR,this.entityName+"Service.java"));
        // 要注意看有没有乱码，如果有就换成FileWriter
        outputStream.write(out.getBytes());
        outputStream.flush();
        outputStream.close();
        System.out.println(this.entityName+"Service.java创建成功");

    }

    private void createServiceImplJava() throws IOException {
        //模板引擎
        TemplateEngine engine = new TemplateEngine();
        //读取磁盘中的模板文件
        ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
        //路径
        resolver.setPrefix("template/");
        //后缀
        resolver.setSuffix(".txt");
        //设置模板模式、默认是HTML
        resolver.setTemplateMode("TEXT");
        //设置引擎使用 resolve
        engine.setTemplateResolver(resolver);
        //准备数据 使用context
        Context context = new Context();
        //添加基本类型
        /*
        实体类名 entityName  大写
        实体类名小写字母开头  lowerEntityName
        实体类主键类型  primaryKeyType
        描述 descript
         */
        context.setVariable("entityName",this.entityName);
        context.setVariable("lowerEntityName",this.lowerEntityName);
        context.setVariable("primaryKeyType",this.primaryKeyType);
        context.setVariable("descript",this.descript);
        //生成内容变为字符串
        String out = engine.process("ServiceImplTemplate",context);
       // System.out.printf(out);
        // 把out写入到指定目录
        FileOutputStream outputStream=new FileOutputStream(new File(SERVICE_DIR+"impl\\",this.entityName+"ServiceImpl.java"));
        // 要注意看有没有乱码，如果有就换成FileWriter
        outputStream.write(out.getBytes());
        outputStream.flush();
        outputStream.close();
        System.out.println(this.entityName+"ServiceImpl.java创建成功");
    }

    @Override
    public void run() {
        try {
            this.createServiceJava();
            this.createServiceImplJava();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
