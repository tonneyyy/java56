package com.hxzy.util;

import com.hxzy.common.vo.CommonVO;

import java.util.ArrayList;
import java.util.List;

/**
 * 通用数据
 *
 * @author tonneyyy
 */
public class CommonDataUtil {

    /**
     * 性别的数据
     * @return
     */
    public static List<CommonVO> sexDataList(){
        List<CommonVO> list=new ArrayList<>();
        list.add(new CommonVO(0 ,"女"));
        list.add(new CommonVO(1,"男"));
        return list;
    }

    /**
     * 状态的数据
     * @return
     */
    public static List<CommonVO>  stateDataList(){
        List<CommonVO> list=new ArrayList<>();

        list.add(new CommonVO(0 ,"正常"));
        list.add(new CommonVO(1,"停用"));
        return list;
    }

}
