package com.hxzy.common.controller;

import cn.hutool.core.lang.UUID;
import com.hxzy.common.consts.RedisConst;
import com.hxzy.common.vo.R;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.GifCaptcha;
import com.wf.captcha.SpecCaptcha;
import javafx.scene.chart.ValueAxis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 通用验证码
 *
 * @author tonneyyy
 */
@RestController
@RequestMapping(value = "/common")
public class CaptchaController {

    @Autowired
    private RedisTemplate  redisTemplate;

    @GetMapping(value = "/png")
    public R png(@RequestParam(name = "width", defaultValue = "150") int width,
                 @RequestParam(name = "height", defaultValue = "40")int height,
                 @RequestParam(name = "len", defaultValue = "4")int len){
        // png类型
        SpecCaptcha captcha = new SpecCaptcha(width, height,len);
        //图片对应的值,存储redis中
        String value= captcha.text();  // 获取验证码的字符

        //发给前端的
        String base64Image= captcha.toBase64();
        String uuid= UUID.fastUUID().toString();

        //写入到redis中(3分钟以后过期)
        String redisKey= RedisConst.getRedisKey(RedisConst.CAPTCHA_KEY, uuid);
        this.redisTemplate.opsForValue().set(redisKey, value,RedisConst.CAPTCHA_EXPIRED_MINUTES, TimeUnit.MINUTES);


        Map<String,Object> data=new HashMap<>();
        data.put("uuid",uuid);
        data.put("image", base64Image);
        return R.okHasData(data);
    }

    /**
     * 有动画的
     * @param width
     * @param height
     * @param len
     * @return
     */
    @GetMapping(value = "/gif")
    public R gif(@RequestParam(name = "width", defaultValue = "150") int width,
                 @RequestParam(name = "height", defaultValue = "40")int height,
                 @RequestParam(name = "len", defaultValue = "4")int len){
        // png类型
        GifCaptcha captcha = new GifCaptcha(width, height,len);
        //图片对应的值,存储redis中
        String value= captcha.text();  // 获取验证码的字符

        //发给前端的
        String base64Image= captcha.toBase64();
        String uuid= UUID.fastUUID().toString();

        //写入到redis中(3分钟以后过期)
        String redisKey= RedisConst.getRedisKey(RedisConst.CAPTCHA_KEY, uuid);
        this.redisTemplate.opsForValue().set(redisKey, value,RedisConst.CAPTCHA_EXPIRED_MINUTES, TimeUnit.MINUTES);


        Map<String,Object> data=new HashMap<>();
        data.put("uuid",uuid);
        data.put("image", base64Image);
        return R.okHasData(data);
    }

    /**
     * 算术运算
     * @param width
     * @param height
     * @param len
     * @return
     */
    @GetMapping(value = "/airth")
    public R airth(@RequestParam(name = "width", defaultValue = "150") int width,
                 @RequestParam(name = "height", defaultValue = "40")int height,
                 @RequestParam(name = "len", defaultValue = "2")int len){
        // png类型
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(width, height,len);
        //图片对应的值,存储redis中
        String value= captcha.text();  // 获取验证码的字符

        //发给前端的
        String base64Image= captcha.toBase64();
        String uuid= UUID.fastUUID().toString();

        //写入到redis中(3分钟以后过期)
        String redisKey= RedisConst.getRedisKey(RedisConst.CAPTCHA_KEY, uuid);
        this.redisTemplate.opsForValue().set(redisKey, value,RedisConst.CAPTCHA_EXPIRED_MINUTES, TimeUnit.MINUTES);


        Map<String,Object> data=new HashMap<>();
        data.put("uuid",uuid);
        data.put("image", base64Image);
        return R.okHasData(data);
    }
}
