package com.hxzy.common.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hxzy.common.dto.PageDTO;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.vo.PageVO;
import com.hxzy.common.vo.R;
import org.springframework.util.Assert;

import java.util.List;

/**
 * 基本控制器
 *
 * @author tonneyyy
 */
public class BaseController {

    /**
     * 分页第一步，开始PageHelper插件的分页
     * @param pageDTO
     */
    protected  void buildPage(PageDTO pageDTO){
        PageHelper.startPage(pageDTO.getPage(),pageDTO.getSize());
    }

    /**
     * 第页第三步，把PageHelper返回的结果Page转换自定义的对象
     * @param list
     * @return
     */
    protected R pageToPageVO(List  list) {
        if (list instanceof Page) {
            PageVO pageVo = new PageVO();
            Page pg = ((Page) list);
            pageVo.setTotal(pg.getTotal());
            pageVo.setRows(list);
            return R.okHasData(pageVo);
        } else {
            R r = R.build(AckCode.FAIL);
            r.setMsg("分页对象不正确");
            return r;
        }
    }

}
