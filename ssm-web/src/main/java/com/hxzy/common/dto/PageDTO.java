package com.hxzy.common.dto;


import lombok.Getter;
import lombok.Setter;

/**
 * 通用接收分页参数
 */
@Getter
@Setter
public class PageDTO {
    private Integer  page;

    private Integer size;
}
