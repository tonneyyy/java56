package com.hxzy.vo;

import com.hxzy.entity.Employee;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 管理员登录令牌信息对象
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class AdminLoginVO {

    private Employee  employee;

    private String uuid;

    /**
     * 令牌创建时间
     */
    private Date createTime;
    /**
     * 令牌过期时间
     */
    private Date expiredTimer;

    public AdminLoginVO(Employee employee, String uuid) {
        this.employee = employee;
        this.uuid = uuid;
    }

    /**
     * 为了给json反序列化注入值用的
     */
    public AdminLoginVO() {
    }
}
