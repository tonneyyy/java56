import hasPermi from './permission/index.js'
import isStore from './merchandisepermission/index.js'
/*
Vue.directive('指令名', 
 {
  inserted(el, binding, vnode)  {
    
  }
}
)
*/ 

// 创建多个指令
const install = function(Vue) { 
  Vue.directive('hasPermi', hasPermi) 
  Vue.directive('isStore', isStore) 
}

// 如果window中拥有的vue,就用我们的覆盖别人的
if (window.Vue) {
  window['hasPermi'] = hasPermi
  window['isStore'] = isStore
  Vue.use(install); // eslint-disable-line
}

export default install
