// 自定义指令  管理员能看到按钮，否则就要把按钮从父节点中移出
import store from '@/store'

export default {
  // 插入 el 当前dom标签
  inserted(el, binding, vnode) {
    const usertype = store.getters && store.getters.loginType
    if (usertype!=0) {
      // <el-button   v-permission   id="test">   https://www.runoob.com/jsref/prop-node-parentnode.html
      el.parentNode && el.parentNode.removeChild(el)
      console.log(el)
      console.log(el.parentNode)
    }
  }
}
