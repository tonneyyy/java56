//商家
import request from '@/utils/request'


export function dataListApi(params) {
  return request({
    url:  '/api/merchandiser/data',
    method: 'get',
    params
  })
}

/** 根据主键查询
 * @param {Object} id
 */
export function findByIdApi(id){
  return request({
    url:  '/api/merchandiser/'+id,
    method: 'get'
  })
}

/** 验证值是否存在
 * @param {Object} params
 */
export function accountExists(params) {
  return request({
    url:  '/api/merchandiser/exists',
    method: 'get',
    params
  })
}



/** 修改保存
 * @param {Object} data
 */
export function editApi(data){
  return request({
    url:  '/api/merchandiser',
    method: 'PUT',
    data
  })
}

/** 更改密码
 * @param {Object} data
 */
export function changepwd(data){
  return request({
    url:  '/api/merchandiser/changepwd',
    method: 'PUT',
    data
  })
}
