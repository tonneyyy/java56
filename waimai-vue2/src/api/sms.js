import request from '@/utils/request'

/** 忘记密码，发送短信
 * @param {Object} phone
 */
export function forgetpwd(phone) {
  return request({
    url:  '/api/sms/forget/pwd?phone='+phone,
    method: 'get' 
  })
}
