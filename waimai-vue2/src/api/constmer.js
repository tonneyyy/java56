import request from '@/utils/request'

 

export function dataListApi(params) {
  return request({
    url:  '/api/contamer/data',
    method: 'get',
    params
  })
}
