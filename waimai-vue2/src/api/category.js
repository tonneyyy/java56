import request from '@/utils/request'


/** 商品分类
 * @param {Object} params
 */
export function dataListApi(params) {
  return request({
    url:  '/api/category/data',
    method: 'get',
    params
  })
}


/** 商品分类新增
 * @param {Object} data
 */
export function addApi(data) {
  return request({
    url:  '/api/category',
    method: 'post',
    data
  })
}

/** 商品分类修改
 * @param {Object} data
 */
export function editApi(data) {
  return request({
    url:  '/api/category',
    method: 'put',
    data
  })
}


/**
 * 根据主键查询
 * @param {Object} id
 */
export function findById(id) {
  return request({
    url:  '/api/category/'+id,
    method: 'get'
  })
}

/**
 * 删除操作
 * @param {Object} ids
 */
export function deleteByIdApi(ids){
  return request({
      url:  '/api/category?ids='+ids,
      method: 'delete'
    })
}

/**
 * 根据商家查询分类信息
 */
export function findByMid(){
  return request( {
      url:  '/api/category/bymid',
      method: 'get'
    } )
  
}