import request from '@/utils/request'
 

export function commonDataApi( ) {
  return request({
    url:   '/api/common/data',
    method: 'get' 
  })
}

/**
 * 口味爱好
 */
export function  commonGoodsflavor(){
  return request({
    url:   '/api/common/goodsflavor',
    method: 'get' 
  })
}