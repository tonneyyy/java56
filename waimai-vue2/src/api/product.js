//商品信息
import request from '@/utils/request'


export function dataListApi(params) {
  return request({
    url:  '/api/product/data',
    method: 'get',
    params
  })
}

/**
 * 根据主键查询
 * @param {Object} id
 */
export function findById(id) {
  return request({
    url:  '/api/product/'+id,
    method: 'get'
  })
}


/**
 * 新增商品信息
 * @param {Object} data
 */
export function addApi(data) {
  return request({
    url:  '/api/product',
    method: 'post',
    data
  })
}

/**
 * 修改商品信息
 * @param {Object} data
 */
export function editApi(data) {
  return request({
    url:  '/api/product',
    method: 'put',
    data
  })
}