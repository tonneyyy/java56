import request from '@/utils/request'
 

export function png( ) {
  return request({
    url:   '/common/png',
    method: 'get' 
  })
}

export function airth( ) {
  return request({
    url:   '/common/airth',
    method: 'get' 
  })
}

export function gif( ) {
  return request({
    url:   '/common/gif',
    method: 'get' 
  })
}