import request from '@/utils/request'


export function dataListApi(params) {
  return request({
    url:  '/api/employee/data',
    method: 'get',
    params
  })
}

/** 根据id查询
 * @param {Object} id
 */
export function findByIdApi(id){
  return request({
    url:  '/api/employee/'+id,
    method: 'get'
  })
}


/** 新增
 * @data
 */
export function addApi(data){
  return request({
    url:  '/api/employee',
    method: 'POST',
    data
  })
}


/** 修改
 * @param  data
 */
export function editApi(data){
  return request({
    url:  '/api/employee',
    method: 'PUT',
    data
  })
}
