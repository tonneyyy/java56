// 获取路由权限（动态路由）
// 通用路由，     管理员的路由=constantRoutes+管理员的路由，   商家的路由=constantRoutes+enterpriseRoutes
import router,{ constantRoutes, adminRoutes ,enterpriseRoutes } from '@/router'
import store from '@/store'

const permission = {
  state: {
    // 默认路由状态
    routers: constantRoutes,
    // 根据当前登录用户对象，加载adminRoutes还是enterpriseRoutes
    addRouters: []
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      // 所有的路由=通用路由+ 商家还是管理路由
      state.routers = constantRoutes.concat(routers)
    }
  },
  actions: {
    // 生成路由
    GenerateRoutes({ commit }) {

       return new Promise(resolve => {
         console.log('-------GenerateRoutes------')
         console.log(store.getters)
         const loginType = store.getters.loginType
         console.log(loginType)
         // 管理员
         if(loginType ===0){
            commit('SET_ROUTERS', adminRoutes)
         }else{
           //商家
            commit('SET_ROUTERS', enterpriseRoutes)
         }

         resolve()
        })

       }

      }
}

export default permission
