const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  // 公共一个getter方法，获取当前用户登录类型
  loginType: state => state.user.login_type,
  //完整路由,主页左边菜单组件使用,layout/compoents/Sidebar/index.vue中使用的
  allRouter: state => state.permission.routers,
  //只获取追加路由模块，最外层 permission.js的动态路追加用的
  addRouter: state => state.permission.addRouters
}
export default getters
