function loginApi(data) {
    return $axios({
      'url': '/front/user/login',
      'method': 'post',
      data
    })
  }

function loginoutApi() {
  return $axios({
    'url': '/front/user/logout',
    'method': 'post'
  })
}

function userinfoApi() {
  return $axios({
    'url': '/front/user/info',
    'method': 'get'
  })
}
