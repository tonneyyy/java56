//获取所有的菜品分类
function categoryListApi() {
    return $axios({
      'url': '/front/category/list/1',
      'method': 'get',
    })
  }

//获取菜品分类对应的菜品
function dishListApi(data) {
    return $axios({
        'url': '/front/dish/list',
        'method': 'get',
        params:{...data}
    })
}

//获取菜品分类对应的套餐
function setmealListApi(data) {
    return $axios({
        'url': '/front/setmeal/list',
        'method': 'get',
        params:{...data}
    })
}

//获取购物车内商品的集合(某一个商家里面的商品信息某个用户)
function cartListApi(data) {
    return $axios({
        'url': '/front/user/shoppingCart/get/1',
        'method': 'get',
        params:{...data}
    })
}

//购物车中添加商品
function  addCartApi(data){
    return $axios({
        'url': '/front/user/shoppingCart/add',
        'method': 'post',
        data
      })
}

//购物车中修改商品
function  updateCartApi(data){
    return $axios({
        'url': '/front/user/shoppingCart/sub',
        'method': 'post',
        data
      })
}

//删除购物车的商品
function clearCartApi() {
    return $axios({
        'url': '/front/user/shoppingCart/clean',
        'method': 'delete',
    })
}

//获取套餐的全部菜品
function setMealDishDetailsApi(id) {
    return $axios({
        'url': `/front/setmeal/dish/${id}`,
        'method': 'get',
    })
}


