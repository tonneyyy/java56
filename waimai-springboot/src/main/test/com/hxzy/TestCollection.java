package com.hxzy;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.digest.BCrypt;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
public class TestCollection {

    @Test
    public void test03(){
        // 年月日时分+ 自增的值


        for(int i=1;i<=20;i++) {
            long orderId = IdUtil.getSnowflake().nextId();

            System.out.println(orderId);
        }
    }

    @Test
    public void test02(){
        String admin8888 = BCrypt.hashpw("admin8888");
        System.out.println(admin8888);
    }


    @Test
    public void test01(){
        //数据库的值
        List<Long> list01= Arrays.asList(7L,10L);
        Set<Long> set01=new HashSet<>(list01);

        //现在的值
        List<Long> list02=Arrays.asList(7L,11L);
        Set<Long> set02=new HashSet<>(list02);

        //求交集
        boolean b = set01.retainAll(set02);
        System.out.println(b);
        System.out.println("交集");
        System.out.println(set01);

        System.out.println("");

        //求list01与set01的差集
        Set<Long> set001=new HashSet<>(list01);
        set001.removeAll(set01);
        System.out.println("差集");
        System.out.println(set001);

    }
}
