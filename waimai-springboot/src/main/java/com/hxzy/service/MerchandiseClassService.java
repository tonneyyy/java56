package com.hxzy.service;

import com.hxzy.dto.CategorySearch;
import com.hxzy.entity.MerchandiseClass;
import com.hxzy.vo.CategoryVO;

import java.util.List;

/**
 * 商品分类业务逻辑接口
 *
 * @author tonneyyy
 */
public interface MerchandiseClassService extends IService<MerchandiseClass,Long> {

    /**
     * 分页查询
     * @param categorySearch
     * @return
     */
    List<CategoryVO> search(CategorySearch categorySearch);

    /**
     * 根据商品分类删除
     * @param ids
     * @return
     */
    int deleteIds(Long[] ids);

    /**
     * 根据商家ID查询商品分类
     * @param mid
     * @return
     */
    List<MerchandiseClass> findCategoryBymid(long mid);
}
