package com.hxzy.service;

import com.hxzy.dto.ProductDTO;
import com.hxzy.dto.ProductSearch;
import com.hxzy.entity.Merchandise;
import com.hxzy.vo.ProductVO;

import javax.validation.Valid;
import java.util.List;

/**
 * 商品信息业务逻辑接口
 *
 * @author tonneyyy
 */
public interface MerchandiseService extends IService<Merchandise,Integer> {

    /**
     * 根据商品分类查询商品个数
     * @param mid
     * @return
     */
    int findCountBymid(long mid);

    /**
     * 商品分页查询
     * @param productSearch
     * @return
     */
    List<ProductVO> search(ProductSearch productSearch);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    ProductVO findById(Long id);

    /**
     * 新增保存
     * @param productDTO
     * @return
     */
    int add(@Valid ProductDTO productDTO);

    /**
     * 修改保存
     * @param productDTO
     * @return
     */
    int update(@Valid ProductDTO productDTO);

    /**
     * 获取某个商家的菜品分类对应的菜品
     * @param categoryId
     * @param bid
     * @return
     */
    List<ProductVO> findProdeuctByCategoryIdAndbid(Long categoryId, Long bid);
}
