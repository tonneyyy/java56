package com.hxzy.service;

import com.hxzy.dto.LoginDTO;
import com.hxzy.vo.AdminLoginVO;

/**
 * 后台员工或商家登录的业务逻辑
 *
 * @author tonneyyy
 */
public interface LoginService {

    /**
     * 登录
     * @param loginDTO
     * @return
     */
    AdminLoginVO login(LoginDTO loginDTO);
}
