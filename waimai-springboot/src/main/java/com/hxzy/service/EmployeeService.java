package com.hxzy.service;

import com.hxzy.dto.EmployeeSearch;
import com.hxzy.dto.LoginDTO;
import com.hxzy.entity.Employee;

import javax.validation.Valid;
import java.util.List;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
public interface EmployeeService extends IService<Employee,Long> {


    /**
     * 分页查询
     * @param employeeSearch
     * @return
     */
    List<Employee> search(EmployeeSearch employeeSearch);
}
