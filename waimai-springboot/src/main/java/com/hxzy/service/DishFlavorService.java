package com.hxzy.service;

import com.hxzy.entity.DishFlavor;

import java.util.List;

/**
 * 口味列表
 *
 * @author tonneyyy
 */
public interface DishFlavorService extends IService<DishFlavor,Long> {

    /**
     * 根据菜口id查询
     * @param dishId
     * @return
     */
    List<DishFlavor>  findByDishId(Long dishId);

    /**
     * 根据dish_id删除
     * @param id
     * @return
     */
    int deleteByDishId(Long id);
}
