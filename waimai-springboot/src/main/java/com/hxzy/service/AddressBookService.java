package com.hxzy.service;

import com.hxzy.entity.AddressBook;

import java.util.List;

/**
 * 收货地址信息业务逻辑接口
 *
 * @author tonneyyy
 */
public interface AddressBookService extends IService<AddressBook,Long> {

    /**
     * 根据用户id查询
     * @param id
     * @return
     */
    List<AddressBook> findByUserId(Integer id);

    /**
     * 修改自己默认收货地址
     * @param addressBook
     * @return
     */
    int defaultAddress(AddressBook addressBook);

    /**
     * 设定删除标识
     * @param id
     * @return
     */
    int updateDelete(Long id);
}
