package com.hxzy.service;

import com.hxzy.dto.MerchandiserChangePwd;
import com.hxzy.dto.MerchandiserSearch;
import com.hxzy.entity.Merchandiser;

import javax.validation.Valid;
import java.util.List;

/**
 * 商家信息业务逻辑接口
 *
 * @author tonneyyy
 */
public interface MerchandiserService extends IService<Merchandiser,Long> {
    List<Merchandiser> search(MerchandiserSearch search);

    /**
     * 更新密码
     * @param merchandiserChangePwd
     * @return
     */
    int changePwd( MerchandiserChangePwd merchandiserChangePwd);

 
}
