package com.hxzy.service;

import com.hxzy.dto.OrderDTO;
import com.hxzy.dto.OrderSearch;
import com.hxzy.entity.Orders;
import com.hxzy.entity.ShoppingCart;
import com.hxzy.vo.OrderVO;

import java.util.List;

/**
 * 订单信息业务逻辑接口
 *
 * @author tonneyyy
 */
public interface OrdersService extends IService<Orders,Long> {

    /**
     * 订单查询
     * @param orderSearch
     * @return
     */
    List<OrderVO> orderPage(OrderSearch orderSearch);

    /**
     * 下单
     * @param orderDTO
     * @param list
     * @return 订单号
     */
    Long submit(OrderDTO orderDTO, List<ShoppingCart> list);

    /**
     * 根据订单号查询数据
     * @param orderId
     * @return
     */
    Orders selectByOrderId(Long orderId);

    /**
     * 更新订单状态
     * @param orderId
     * @param payTime
     * @return
     */
    int updateByOrderId(Long orderId, String payTime);
}
