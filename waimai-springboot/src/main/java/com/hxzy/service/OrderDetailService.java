package com.hxzy.service;

import com.hxzy.entity.OrderDetail;

import java.util.List;

/**
 * 订单明细信息业务逻辑接口
 *
 * @author tonneyyy
 */
public interface OrderDetailService extends IService<OrderDetail,Long> {

    /**
     * 根据订单编号查询订单明细
     * @param orderId
     * @return
     */
    List<OrderDetail>  findByOrderId(Long orderId);
}
