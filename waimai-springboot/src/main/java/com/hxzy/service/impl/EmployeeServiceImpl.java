package com.hxzy.service.impl;

import cn.hutool.crypto.digest.BCrypt;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.exception.ServiceException;
import com.hxzy.dto.EmployeeSearch;
import com.hxzy.dto.LoginDTO;
import com.hxzy.entity.Employee;
import com.hxzy.mapper.EmployeeMapper;
import com.hxzy.service.EmployeeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Log4j2
@Service
public class EmployeeServiceImpl extends ServiceImpl<Employee,Long> implements EmployeeService {

    private EmployeeMapper employeeMapper;

    @Autowired
    public void setEmployeeMapper(EmployeeMapper employeeMapper) {
        this.employeeMapper = employeeMapper;
        super.setMyBatisBaseDao(employeeMapper);
    }



    @Override
    public List<Employee> search(EmployeeSearch employeeSearch) {
        return this.employeeMapper.search(employeeSearch);
    }
}
