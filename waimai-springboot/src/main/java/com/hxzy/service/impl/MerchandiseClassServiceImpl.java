package com.hxzy.service.impl;

import com.hxzy.dto.CategorySearch;
import com.hxzy.entity.MerchandiseClass;
import com.hxzy.mapper.MerchandiseClassMapper;
import com.hxzy.service.MerchandiseClassService;
import com.hxzy.service.MerchandiseService;
import com.hxzy.vo.CategoryVO;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 商品分类业务逻辑实现
 *
 * @author tonneyyy
 */
@Service
public class MerchandiseClassServiceImpl extends ServiceImpl<MerchandiseClass,Long> implements MerchandiseClassService {

    private MerchandiseClassMapper merchandiseClassMapper;

    /**
     * 商品信息
     */
    @Autowired
    private MerchandiseService merchandiseService;


    @Autowired
    public void setMerchandiseClassMapper(MerchandiseClassMapper merchandiseClassMapper) {
        this.merchandiseClassMapper = merchandiseClassMapper;
        super.setMyBatisBaseDao(merchandiseClassMapper);
    }

    @Override
    public List<CategoryVO> search(CategorySearch categorySearch) {
        return this.merchandiseClassMapper.search(categorySearch);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteIds(Long[] ids) {
        int count=0;
        for(long id : ids){
            int countBymid = this.merchandiseService.findCountBymid(id);
            //商品分类没有使用过
            if(countBymid==0){
                count+=this.merchandiseClassMapper.deleteByPrimaryKey(id);
            }
        }
        return count;
    }

    /**
     * 根据商家ID查询商品分类
     * @param mid
     * @return
     */
    @Override
    public List<MerchandiseClass> findCategoryBymid(long mid) {
        return this.merchandiseClassMapper.findCategoryBymid(mid);
    }
}