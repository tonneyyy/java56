package com.hxzy.service.impl;

import com.hxzy.dto.MerchandiserChangePwd;
import com.hxzy.dto.MerchandiserSearch;
import com.hxzy.entity.Merchandiser;
import com.hxzy.mapper.MerchandiserMapper;
import com.hxzy.service.MerchandiserService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 商品信息业务逻辑实现
 *
 * @author tonneyyy
 */
@Service
public class MerchandiserServiceImpl extends ServiceImpl<Merchandiser,Long> implements MerchandiserService {

    private MerchandiserMapper merchandiserMapper;

    @Autowired
    public void setMerchandiserMapper(MerchandiserMapper merchandiserMapper) {
        this.merchandiserMapper = merchandiserMapper;
        super.setMyBatisBaseDao(merchandiserMapper);
    }

    @Override
    public List<Merchandiser> search(MerchandiserSearch search) {
        return this.merchandiserMapper.search(search);
    }

    @Override
    public int changePwd(MerchandiserChangePwd merchandiserChangePwd) {
        return this.merchandiserMapper.changePwd(merchandiserChangePwd);
    }


}