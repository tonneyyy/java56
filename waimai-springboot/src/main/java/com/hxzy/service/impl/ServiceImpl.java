package com.hxzy.service.impl;

import com.hxzy.mapper.MyBatisBaseDao;
import com.hxzy.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * 业务逻辑实现
 *
 * @author tonneyyy
 */
public abstract class ServiceImpl<Model, PK extends Serializable> implements IService<Model,PK> {

    private MyBatisBaseDao  myBatisBaseDao;

    /**
     * 为父类赋值 父类的引用 指向子类的实现
     * @param myBatisBaseDao
     */
    public void setMyBatisBaseDao(MyBatisBaseDao myBatisBaseDao) {
        this.myBatisBaseDao = myBatisBaseDao;
    }

    @Override
    public int deleteByPrimaryKey(PK id) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");

        return this.myBatisBaseDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Model record) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return  this.myBatisBaseDao.insert(record);
    }

    @Override
    public int insertSelective(Model record) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return  this.myBatisBaseDao.insertSelective(record);
    }

    @Override
    public Model selectByPrimaryKey(PK id) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return (Model) this.myBatisBaseDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Model record) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return this.myBatisBaseDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Model record) {
        Assert.notNull(this.myBatisBaseDao, "Property 'myBatisBaseDao' is required");
        return this.myBatisBaseDao.updateByPrimaryKey(record);
    }
}
