package com.hxzy.service.impl;

import com.hxzy.entity.DishFlavor;
import com.hxzy.mapper.DishFlavorMapper;
import com.hxzy.service.DishFlavorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 口味列表
 *
 * @author tonneyyy
 */
@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavor,Long> implements DishFlavorService {

    private DishFlavorMapper dishFlavorMapper;

    @Autowired
    public void setDishFlavorMapper(DishFlavorMapper dishFlavorMapper) {
        this.dishFlavorMapper = dishFlavorMapper;
        super.setMyBatisBaseDao(dishFlavorMapper);
    }


    /**
     * 根据菜口id查询
     * @param dishId
     * @return
     */
    @Override
    public List<DishFlavor> findByDishId(Long dishId) {
        return this.dishFlavorMapper.findByDishId(dishId);
    }

    @Override
    public int deleteByDishId(Long id) {
        return this.dishFlavorMapper.deleteByDishId(id);
    }
}
