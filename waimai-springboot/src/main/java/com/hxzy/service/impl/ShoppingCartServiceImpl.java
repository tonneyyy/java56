package com.hxzy.service.impl;

import com.hxzy.dto.ShoppingCartSearch;
import com.hxzy.entity.ShoppingCart;
import com.hxzy.mapper.ShoppingCartMapper;
import com.hxzy.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;

/**
 * 购物车
 *
 * @author tonneyyy
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCart,Long> implements ShoppingCartService {

    private ShoppingCartMapper shoppingCartMapper;

    @Autowired
    public void setShoppingCartMapper(ShoppingCartMapper shoppingCartMapper) {
        this.shoppingCartMapper = shoppingCartMapper;
        super.setMyBatisBaseDao(shoppingCartMapper);
    }

    /**
     * 根据商家ID和用户ID查询购物车信息
     * @param shoppingCartSearch
     * @return
     */
    @Override
    public List<ShoppingCart> searchByShoppingCart(ShoppingCartSearch shoppingCartSearch) {
        return this.shoppingCartMapper.searchByShoppingCart(shoppingCartSearch);
    }
}


