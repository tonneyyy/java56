package com.hxzy.service.impl;

import com.hxzy.entity.AddressBook;
import com.hxzy.mapper.AddressBookMapper;
import com.hxzy.service.AddressBookService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 收货地址信息业务逻辑实现
 *
 * @author tonneyyy
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBook,Long> implements AddressBookService {

    private AddressBookMapper addressBookMapper;

    @Autowired
    public void setAddressBookMapper(AddressBookMapper addressBookMapper) {
        this.addressBookMapper = addressBookMapper;
        super.setMyBatisBaseDao(addressBookMapper);
    }

    /**
     * 根据用户id查询
     * @param id
     * @return
     */
    @Override
    public List<AddressBook> findByUserId(Integer id) {
        return this.addressBookMapper.findByUserId(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int defaultAddress(AddressBook addressBook) {
        //先把这个用户 userId=?   全部设定 is_default=0
        //  update address_book set is_default=0  where user_id=?     and is_default=1
        int result=this.addressBookMapper.rejectDefaultAddress(addressBook.getUserId());

        //  再把这个id的值 is_default=1
        //  update address_book set is_default=1  where id=? and user_id=?
        result+=this.addressBookMapper.allowDeafultAddress(addressBook.getId(),addressBook.getUserId());

        return result;
    }

    @Override
    public int updateDelete(Long id) {
        return this.addressBookMapper.updateDelete(id);
    }
}