package com.hxzy.service.impl;

import com.hxzy.entity.OrderDetail;
import com.hxzy.mapper.OrderDetailMapper;
import com.hxzy.service.OrderDetailService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 订单明细信息业务逻辑实现
 *
 * @author tonneyyy
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetail,Long> implements OrderDetailService {

    private OrderDetailMapper orderDetailMapper;

    @Autowired
    public void setOrderDetailMapper(OrderDetailMapper orderDetailMapper) {
        this.orderDetailMapper = orderDetailMapper;
        super.setMyBatisBaseDao(orderDetailMapper);
    }

    @Override
    public List<OrderDetail> findByOrderId(Long orderId) {
        return this.orderDetailMapper.findByOrderId(orderId);
    }
}