package com.hxzy.service.impl;

import cn.hutool.crypto.digest.BCrypt;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.exception.ServiceException;
import com.hxzy.dto.LoginDTO;
import com.hxzy.entity.Employee;
import com.hxzy.mapper.LoginMapper;
import com.hxzy.service.IService;
import com.hxzy.service.LoginService;
import com.hxzy.vo.AdminLoginVO;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Log4j2
@Service
public class LoginServiceImpl   implements LoginService {

    @Autowired
    private LoginMapper loginMapper;


    @Override
    public AdminLoginVO login(LoginDTO loginDTO) {

        AdminLoginVO dbLoginVO=  this.loginMapper.findByLoginName(loginDTO.getLoginName());
        if(dbLoginVO==null){
            log.info(loginDTO.getLoginName()+","+ AckCode.USER_NOT_FOUND.getMsg());
            throw new ServiceException(AckCode.USERNAME_PASSWORD_ERROR);
        }

        //判断密码
        boolean checkpw = BCrypt.checkpw(loginDTO.getLoginPwd(), dbLoginVO.getLoginPwd());
        if(!checkpw){
            log.info(loginDTO.getLoginName()+","+AckCode.LOGIN_PASSWORD_ERROR.getMsg());
            throw new ServiceException(AckCode.USERNAME_PASSWORD_ERROR);
        }

        //判断状态
        Byte status = dbLoginVO.getStatus();
        if( status.intValue()==1){
            //后台员工
            if(dbLoginVO.getType()==0){
                log.info(loginDTO.getLoginName()+","+AckCode.DEVICE_BANNED.getMsg());
                throw new ServiceException(AckCode.DEVICE_BANNED);
            }else{
                log.info(loginDTO.getLoginName()+","+AckCode.ENTERPRISE_NOT_VALIDATED.getMsg());
                throw new ServiceException(AckCode.ENTERPRISE_NOT_VALIDATED);
            }
        }
        return dbLoginVO;
    }
}
