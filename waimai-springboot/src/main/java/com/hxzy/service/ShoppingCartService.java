package com.hxzy.service;

import com.hxzy.dto.ShoppingCartSearch;
import com.hxzy.entity.ShoppingCart;

import java.util.List;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
public interface ShoppingCartService extends IService<ShoppingCart,Long> {

    /**
     * 根据商家ID和用户ID查询购物车信息
     * @param shoppingCartSearch
     * @return
     */
    List<ShoppingCart> searchByShoppingCart(ShoppingCartSearch shoppingCartSearch);
}
