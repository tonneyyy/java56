package com.hxzy.service;

import com.hxzy.dto.ContamerLoginDTO;
import com.hxzy.dto.ContamerSearch;
import com.hxzy.entity.Contamer;
import com.hxzy.vo.ContamerLoginVO;

import javax.validation.Valid;
import java.util.List;

/**
 * 前端客户
 *
 * @author tonneyyy
 */
public interface ContamerService extends IService<Contamer,Integer> {

    /**
     * 分页查询
     * @param contamerSearch
     * @return
     */
    List<Contamer> search(ContamerSearch contamerSearch);

    /**
     * 登录
     * @param contamerLoginDTO
     * @return
     */
    ContamerLoginVO login(@Valid ContamerLoginDTO contamerLoginDTO);
}
