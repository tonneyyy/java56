package com.hxzy.vo;

import com.hxzy.entity.Merchandise;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 商品信息
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class ProductVO extends Merchandise {

    /**
     * 商家名称
     */
    private String enterpriseName;

    /**
     * 分类名称
     */
    private String categoryName;


    /**
     * 口味列表
     */
    private List<DishFlavorsVO> dishFlavors;
}
