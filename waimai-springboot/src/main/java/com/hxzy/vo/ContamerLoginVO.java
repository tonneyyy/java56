package com.hxzy.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 前端用户登录返回
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class ContamerLoginVO {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 账号
     */
    private String loginid;

    /**
     * 密码加密
     */
    private String passwold;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 性别(0 女 1 男)
     */
    private Byte gender;

    /**
     * 头像
     */
    private String portrait;

    /**
     * 状态1：停用0：正常
     */
    private Integer status;

    private String uuid;

    /**
     * 令牌创建时间
     */
    private Date createTime;
    /**
     * 令牌过期时间
     */
    private Date expiredTimer;

}
