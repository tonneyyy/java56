package com.hxzy.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 菜品口味列表
 * { name: '', value: [], inputVisible: false, inputValue: '' }
 * @author tonneyyy
 */
@Getter
@Setter
public class DishFlavorsVO {

    /**
     * 数据库的主键
     */
    private Long id;

    private String name;

    private List<String> value;

    private boolean inputVisible;

    private String inputValue;
}
