package com.hxzy.vo;

import com.hxzy.common.consts.IAdd;
import com.hxzy.common.consts.IUpdate;
import com.hxzy.entity.Employee;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * 管理员登录令牌信息对象
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class AdminLoginVO {

    /**
     * 登录类型（0代表管理员，1代表用户)
     */
    private Integer  type;

    private Long id;
    /**
     * 登录账户
     */
    private String loginName;

    /**
     * 名称
     */
    private String name;
    /**
     * 登录密码
     */
    private String loginPwd;
    /**
     * 状态（1停用，0正常）
     */
    private Byte status;

    /**
     * 头像
     */
    private String avatar;

    private String uuid;

    /**
     * 令牌创建时间
     */
    private Date createTime;
    /**
     * 令牌过期时间
     */
    private Date expiredTimer;

    public AdminLoginVO(  String uuid) {
        this.uuid = uuid;
    }

    /**
     * 为了给json反序列化注入值用的
     */
    public AdminLoginVO() {
    }
}
