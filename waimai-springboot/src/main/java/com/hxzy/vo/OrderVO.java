package com.hxzy.vo;

import com.hxzy.entity.OrderDetail;
import com.hxzy.entity.Orders;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class OrderVO extends Orders {

    /**
     * 订单的详情
     */
    private List<OrderDetail>  orderDetails;
}
