package com.hxzy.vo;

import com.hxzy.entity.MerchandiseClass;
import lombok.Getter;
import lombok.Setter;

/**
 * 商品分类输出
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class CategoryVO extends MerchandiseClass {

    /**
     * 商家名称
     */
    private String merName;
}
