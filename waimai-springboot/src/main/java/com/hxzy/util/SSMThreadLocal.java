package com.hxzy.util;

import com.hxzy.vo.AdminLoginVO;
import com.hxzy.vo.ContamerLoginVO;

/**
 * 本地线程池
 *
 * @author tonneyyy
 */
public class SSMThreadLocal {

    /**
     * 存放当前后台登录的用户
     */
    public static ThreadLocal<AdminLoginVO>  adminLoginVOThreadLocal=new ThreadLocal<>();

    /**
     * 存储当前前端的登录用户
     */
    public static ThreadLocal<ContamerLoginVO> contamerLoginVOThreadLocal=new ThreadLocal<>();
}
