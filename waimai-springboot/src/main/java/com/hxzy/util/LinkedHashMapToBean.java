package com.hxzy.util;

import cn.hutool.json.JSONUtil;

/**
 * LinkedHashMap转换为实体类
 *
 * @author tonneyyy
 */
public class LinkedHashMapToBean {


    /**
     *  把object转换成指类型的bean
     * @param obj
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> T  toBean( Object obj,    Class<T> tClass){
        String jsonStr=JSONUtil.toJsonStr(obj);
        return JSONUtil.toBean(jsonStr, tClass);
    }
}
