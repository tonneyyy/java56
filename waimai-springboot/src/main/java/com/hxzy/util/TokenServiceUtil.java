package com.hxzy.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.hxzy.common.consts.RedisConst;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.exception.ServiceException;
import com.hxzy.entity.Employee;
import com.hxzy.vo.AdminLoginVO;
import com.hxzy.vo.ContamerLoginVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 令牌创建、刷新、删除等工具类
 *
 * @author tonneyyy
 */
@Component
public class TokenServiceUtil {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 生成普通的uuid令牌
     * 请参考createJwtToken
     * @return
     */
    @Deprecated
    public String createToken(AdminLoginVO  adminLoginVO){
        String uuid= UUID.randomUUID().toString();
        adminLoginVO.setUuid(uuid);
        adminLoginVO.setCreateTime(new Date());
        adminLoginVO.setExpiredTimer(DateUtil.offsetMinute(adminLoginVO.getCreateTime(),RedisConst.EMPLOYEE_EXPIRED_MINUTES));

        //向redis中写入值
        String redisKey= RedisConst.getRedisKey(RedisConst.EMPLOYEE_KEY,uuid);
        this.redisTemplate.opsForValue().set(redisKey,adminLoginVO,RedisConst.EMPLOYEE_EXPIRED_MINUTES, TimeUnit.MINUTES);

        //返回令牌
        return uuid;
    }

    /**
     * 创建jwt token
     * @param adminLoginVO
     * @return
     */
    public String createJwtToken(AdminLoginVO  adminLoginVO){
        String uuid= UUID.randomUUID().toString();
        adminLoginVO.setUuid(uuid);
        adminLoginVO.setCreateTime(new Date());
        adminLoginVO.setExpiredTimer(DateUtil.offsetMinute(adminLoginVO.getCreateTime(),RedisConst.EMPLOYEE_EXPIRED_MINUTES));

        //向redis中写入值
        String redisKey= RedisConst.getRedisKey(RedisConst.EMPLOYEE_KEY,uuid);
        this.redisTemplate.opsForValue().set(redisKey,adminLoginVO,RedisConst.EMPLOYEE_EXPIRED_MINUTES, TimeUnit.MINUTES);

        //返回jwt令牌
        return uuidToJwt(uuid);
    }

    /**
     * 把uuid转换成为jwt
     * @param uuid
     * @return
     */
    private String uuidToJwt(String uuid){
        Map<String, Object> map = new HashMap<String, Object>() {
            private static final long serialVersionUID = 1L;
            {
                put("uuid", uuid);
            }
        };
        return JWTUtil.createToken(map, RedisConst.JWT_SECURITY.getBytes());
    }

    /**
     * 把jwt解析，取得里面的 uuid
     * @param jwtToken
     * @return
     */
    private String parseJwtToUUID(String jwtToken){
        final JWT jwt = JWTUtil.parseToken(jwtToken);
        Object uuid = jwt.getPayload("uuid");
        return uuid.toString();
    }

    /**
     * 验证令牌的有效性
     * @return
     */
    public AdminLoginVO validatorToken(String jwtToken){
        if(StrUtil.isBlank(jwtToken)){
            throw new ServiceException(AckCode.TOKEN_NOT_BLANK);
        }

        //判断令牌是否有Bearer前缀，如果有就要删除它
        if(jwtToken.startsWith("Bearer")){
            jwtToken=jwtToken.replace("Bearer","").trim();
        }

        //解析jwt
        String uuid=this.parseJwtToUUID(jwtToken);
        //组装rediskey
        String redisKey=RedisConst.getRedisKey(RedisConst.EMPLOYEE_KEY,uuid);
        //查询
        Object obj = this.redisTemplate.opsForValue().get(redisKey);
        //找不到
        if(obj==null){
            throw new ServiceException(AckCode.TOKEN_FAIL);
        }
        //如果redis键的过期时间还剩下10分钟，这时候，我们可以再给这个redis续时
       AdminLoginVO adminLoginVO= (AdminLoginVO) obj;
        refreshToken(adminLoginVO);

        return adminLoginVO;
    }

    /**
     * 刷新令牌的时间
     * @param adminLoginVO
     */
    private void refreshToken(AdminLoginVO  adminLoginVO){
        long expiredTime=adminLoginVO.getExpiredTimer().getTime();
        long currentTime=System.currentTimeMillis();
        //令牌还有10分钟就要过期了
        if(expiredTime-currentTime<=1000L*60*10){
            adminLoginVO.setCreateTime(new Date());
            adminLoginVO.setExpiredTimer(DateUtil.offsetMinute(adminLoginVO.getCreateTime(),RedisConst.EMPLOYEE_EXPIRED_MINUTES));
            //更新redis中的键的值
            //向redis中写入值
            String redisKey= RedisConst.getRedisKey(RedisConst.EMPLOYEE_KEY,adminLoginVO.getUuid());
            this.redisTemplate.opsForValue().set(redisKey,adminLoginVO,RedisConst.EMPLOYEE_EXPIRED_MINUTES, TimeUnit.MINUTES);

        }
    }

    /**
     * 前端用户换令牌
     * @param contamerLoginVO
     * @return
     */
    public String createJwtToken(ContamerLoginVO contamerLoginVO) {
        String uuid= UUID.randomUUID().toString();
        contamerLoginVO.setUuid(uuid);
        contamerLoginVO.setCreateTime(new Date());
        contamerLoginVO.setExpiredTimer(DateUtil.offsetDay(contamerLoginVO.getCreateTime(),RedisConst.CONTAMER_EXPIRED_DAYS));

        //向redis中写入值
        String redisKey= RedisConst.getRedisKey(RedisConst.CONTAMER_KEY,uuid);
        this.redisTemplate.opsForValue().set(redisKey,contamerLoginVO,RedisConst.CONTAMER_EXPIRED_DAYS, TimeUnit.DAYS);

        //返回jwt令牌
        return uuidToJwt(uuid);
    }

    /**
     * 验证前端令牌的有效性
     * @return
     */
    public ContamerLoginVO validatorContamerToken(String jwtToken){
        if(StrUtil.isBlank(jwtToken)){
            throw new ServiceException(AckCode.TOKEN_NOT_BLANK);
        }

        //判断令牌是否有Bearer前缀，如果有就要删除它
        if(jwtToken.startsWith("Bearer")){
            jwtToken=jwtToken.replace("Bearer","").trim();
        }

        //解析jwt
        String uuid=this.parseJwtToUUID(jwtToken);
        //组装rediskey
        String redisKey=RedisConst.getRedisKey(RedisConst.CONTAMER_KEY,uuid);
        //查询
        Object obj = this.redisTemplate.opsForValue().get(redisKey);
        //找不到
        if(obj==null){
            throw new ServiceException(AckCode.TOKEN_FAIL);
        }
        //如果redis键的过期时间还剩下10分钟，这时候，我们可以再给这个redis续时
        ContamerLoginVO contamerLoginVO= (ContamerLoginVO) obj;
        refreshToken(contamerLoginVO);

        return contamerLoginVO;
    }

    /**
     * 刷新前端令牌的时间
     * @param contamerLoginVO
     */
    private void refreshToken(ContamerLoginVO  contamerLoginVO){
        long expiredTime=contamerLoginVO.getExpiredTimer().getTime();
        long currentTime=System.currentTimeMillis();
        //令牌还有1天就要过期了
        if(expiredTime-currentTime<=1000L*60*60*24){
            contamerLoginVO.setCreateTime(new Date());
            contamerLoginVO.setExpiredTimer(DateUtil.offsetDay(contamerLoginVO.getCreateTime(),RedisConst.CONTAMER_EXPIRED_DAYS));
            //更新redis中的键的值
            //向redis中写入值
            String redisKey= RedisConst.getRedisKey(RedisConst.CONTAMER_KEY,contamerLoginVO.getUuid());
            this.redisTemplate.opsForValue().set(redisKey,contamerLoginVO,RedisConst.CONTAMER_EXPIRED_DAYS, TimeUnit.DAYS);
        }
    }
}
