package com.hxzy.mapper;

import com.hxzy.dto.EmployeeSearch;
import com.hxzy.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * EmployeeMapper继承基类
 */
@Mapper
@Repository
public interface EmployeeMapper extends MyBatisBaseDao<Employee, Long> {



    /**
     * 查询
     * @param employeeSearch
     * @return
     */
    List<Employee> search(EmployeeSearch employeeSearch);
}