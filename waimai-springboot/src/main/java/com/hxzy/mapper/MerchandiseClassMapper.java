package com.hxzy.mapper;

import com.hxzy.dto.CategorySearch;
import com.hxzy.entity.MerchandiseClass;
import com.hxzy.vo.CategoryVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * MerchandiseClassMapper继承基类
 */
@Mapper
@Repository
public interface MerchandiseClassMapper extends MyBatisBaseDao<MerchandiseClass, Long> {

    List<CategoryVO> search(CategorySearch categorySearch);

    /**
     * 根据商家ID查询商品分类
     * @param mid
     * @return
     */
    @Select(value = "SELECT id,`name`,picture FROM merchandise_class  where m_id= #{mid}")
    List<MerchandiseClass> findCategoryBymid(long mid);
}