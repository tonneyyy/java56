package com.hxzy.mapper;

import com.hxzy.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

/**
 * DishFlavorMapper继承基类
 */
@Mapper
@Repository
public interface DishFlavorMapper extends MyBatisBaseDao<DishFlavor, Long> {

    /**
     * 根据菜口id查询
     * @param dishId
     * @return
     */
    List<DishFlavor> findByDishId(Long dishId);

    @Delete(value = "delete from dish_flavor where dish_id=#{id}")
    int deleteByDishId(Long id);
}