package com.hxzy.mapper;

import com.hxzy.dto.OrderSearch;
import com.hxzy.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * OrdersMapper继承基类
 */
@Mapper
@Repository
public interface OrdersMapper extends MyBatisBaseDao<Orders, Long> {

    /**
     * 订单查询
     * @param orderSearch
     * @return
     */
    List<Orders> search(OrderSearch orderSearch);

    /**
     * 根据订单号查询数据
     * @param orderId
     * @return
     */
    Orders selectByOrderId(Long orderId);

    /**
     * 支付
     * @param orderId
     * @param payTime
     * @return
     */
    int updateByOrderId(@Param(value = "orderId") Long orderId, @Param(value = "payTime") String payTime);
}