package com.hxzy.mapper;

import com.hxzy.dto.MerchandiserChangePwd;
import com.hxzy.dto.MerchandiserSearch;
import com.hxzy.entity.Merchandiser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * MerchandiserMapper继承基类
 */
@Mapper
@Repository
public interface MerchandiserMapper extends MyBatisBaseDao<Merchandiser, Long> {
    List<Merchandiser> search(MerchandiserSearch search);

    /**
     * 更改密码
     * @param merchandiserChangePwd
     * @return
     */
    @Update(value = "update merchandiser set login_pwd=#{loginPwd} where id=#{id}")
    int changePwd(MerchandiserChangePwd merchandiserChangePwd);
}