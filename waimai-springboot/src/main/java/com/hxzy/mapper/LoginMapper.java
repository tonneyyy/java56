package com.hxzy.mapper;

import com.hxzy.dto.LoginDTO;
import com.hxzy.vo.AdminLoginVO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 后台员工或商家登录的业务逻辑
 *
 * @author tonneyyy
 */
@Mapper
public interface LoginMapper  {


    AdminLoginVO findByLoginName(String loginName);
}
