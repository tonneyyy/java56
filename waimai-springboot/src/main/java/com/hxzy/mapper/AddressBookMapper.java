package com.hxzy.mapper;

import com.hxzy.entity.AddressBook;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * AddressBookMapper继承基类
 */
@Mapper
@Repository
public interface AddressBookMapper extends MyBatisBaseDao<AddressBook, Long> {

    /**
     * 根据用户id查询
     * @param id
     * @return
     */
    List<AddressBook> findByUserId(Integer id);

    /**
     * 先把这个用户 userId=?   全部设定 is_default=0
     * update address_book set is_default=0  where user_id=?     and is_default=1
     * @param userId
     * @return
     */
    @Update(value = "update address_book set is_default=0  where user_id=#{userId}    and is_default=1")
    int rejectDefaultAddress(Long userId);

    /**
     * update address_book set is_default=1  where id=? and user_id=?
     * @param id
     * @param userId
     * @return
     */
    @Update(value = "update address_book set is_default=1  where id=#{id} and user_id=#{userId}")
    int allowDeafultAddress(@Param(value = "id") Long id,@Param(value = "userId") Long userId);

    @Update(value = "update address_book set is_deleted=1 where id=#{id}")
    int updateDelete(Long id);
}