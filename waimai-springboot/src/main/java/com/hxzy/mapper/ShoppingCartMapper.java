package com.hxzy.mapper;

import com.hxzy.dto.ShoppingCartSearch;
import com.hxzy.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ShoppingCartMapper继承基类
 */
@Mapper
@Repository
public interface ShoppingCartMapper extends MyBatisBaseDao<ShoppingCart, Long> {

    /**
     * 根据商家ID和用户ID查询购物车信息
     * @param shoppingCartSearch
     * @return
     */
    List<ShoppingCart> searchByShoppingCart(ShoppingCartSearch shoppingCartSearch);
}