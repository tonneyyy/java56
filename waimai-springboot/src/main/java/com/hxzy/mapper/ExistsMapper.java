package com.hxzy.mapper;

import com.hxzy.common.dto.ExistsDTO;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
public interface ExistsMapper {

    /**
     * 判断值是否存在
     * @param existsDTO
     * @return 0代表不存在，1代表存在
     */
    int existsValue(ExistsDTO existsDTO);
}
