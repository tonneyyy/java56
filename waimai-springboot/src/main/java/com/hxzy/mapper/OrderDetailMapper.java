package com.hxzy.mapper;

import com.hxzy.entity.OrderDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * OrderDetailMapper继承基类
 */
@Mapper
@Repository
public interface OrderDetailMapper extends MyBatisBaseDao<OrderDetail, Long> {

    /**
     * 根据订单编号查询订单明细
     * @param orderId
     * @return
     */
    List<OrderDetail> findByOrderId(Long orderId);
}