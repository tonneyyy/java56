package com.hxzy.mapper;

import com.hxzy.dto.ProductSearch;
import com.hxzy.entity.Merchandise;
import com.hxzy.vo.ProductVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * MerchandiseMapper继承基类
 */
@Mapper
@Repository
public interface MerchandiseMapper extends MyBatisBaseDao<Merchandise, Long> {

    /**
     * 根据商品分类查询商品个数
     * @param mid
     * @return
     */
    @Select(value = "select count(*) from merchandise where m_id=#{mid}")
    int findCountBymid(long mid);

    /**
     * 商品分页查询
     * @param productSearch
     * @return
     */
    List<ProductVO> search(ProductSearch productSearch);

    /**
     * 获取某个商家的菜品分类对应的菜品
     * @param categoryId
     * @param bid
     * @return
     */
    List<Merchandise> findProdeuctByCategoryIdAndbid(@Param(value = "categoryId") Long categoryId, @Param(value = "bid") Long bid);
}