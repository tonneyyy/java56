package com.hxzy.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 
 * 地址管理
 */
@Data
public class AddressBook implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 收货人
     */
    @NotBlank(message = "收货人不能为空")
    private String consignee;

    /**
     * 性别 0 女 1 男
     */
    @NotNull(message = "性别不能为空")
    private Byte sex;

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    private String phone;

    /**
     * 省级区划编号
     */
    @NotBlank(message = "省级区划编号不能为空")
    private String provinceCode;

    /**
     * 省级名称
     */
    @NotBlank(message = "省级名称不能为空")
    private String provinceName;

    /**
     * 市级区划编号
     */
    @NotBlank(message = "市级区划编号不能为空")
    private String cityCode;

    /**
     * 市级名称
     */
    @NotBlank(message = "市级名称不能为空")
    private String cityName;

    /**
     * 区级区划编号
     */
    @NotBlank(message = "区级区划编号不能为空")
    private String districtCode;

    /**
     * 区级名称
     */
    @NotBlank(message = "区级名称编号不能为空")
    private String districtName;

    /**
     * 详细地址
     */
    @NotBlank(message = "详细地址不能为空")
    private String detail;

    /**
     * 标签
     */
    private String label;

    /**
     * 默认 0 否 1是
     */
    private Byte isDefault;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 修改人
     */
    private Long updateBy;

    /**
     * 是否删除
     */
    private Integer isDeleted;

    private static final long serialVersionUID = 1L;
}