package com.hxzy.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.hxzy.common.consts.IAdd;
import com.hxzy.common.consts.IUpdate;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 
 * 后台管理员表
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Employee implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 登录账户
     */
    @NotBlank(message = "登录账户不允许为空",groups = {IAdd.class, IUpdate.class})
    private String loginName;

    /**
     * 登录密码
     */
    @NotBlank(message = "密码不允许为空",groups = {IAdd.class })
    private String loginPwd;

    /**
     * 性别(0 女 1 男)
     */
    @NotNull(message = "性别不允许为空",groups = {IAdd.class, IUpdate.class})
    private Byte gender;

    /**
     * 状态（1停用，0正常）
     */
    @NotNull(message = "状态不允许为空",groups = {IAdd.class, IUpdate.class})
    private Byte status;

    /**
     * 头像
     */
    @JsonInclude(value = JsonInclude.Include.ALWAYS)
    private String avatar;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;



    private static final long serialVersionUID = 1L;
}