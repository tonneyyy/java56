package com.hxzy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 
 * 商家信息
 */
@Data
public class Merchandiser implements Serializable {
    private Long id;

    /**
     * 商家名称
     */
    @NotBlank(message = "商家名称不能为空")
    private String name;

    /**
     * 商家地址
     */
    @NotBlank(message = "商家地址不能为空")
    private String address;

    /**
     * 联系电话
     */
    @NotBlank(message = "联系电话不能为空")
    private String phone;

    /**
     * 商家图片
     */
    @NotBlank(message = "商家图片不能为空")
    private String picture;

    /**
     *  商家位置经度
     */
    @NotNull(message = "经度不能为空")
    private BigDecimal longitude;

    /**
     * 商家位置纬度
     */
    @NotNull(message = "纬度不能为空")
    private BigDecimal latitude;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 商家账户
     */
    @NotBlank(message = "商家账户不能为空")
    private String loginName;

    /**
     * 商家密码
     */
    private String loginPwd;

    /**
     * 审核状态（0已审核，1未审核）
     */
    private Byte status;

    private static final long serialVersionUID = 1L;
}