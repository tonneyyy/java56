package com.hxzy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 * @author 
 * 订单表
 */
@Data
public class Orders implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 订单号
     */
    private String number;

    /**
     * 订单状态 1待付款，2待派送，3已派送，4已完成，5已取消
     */
    private Integer status;

    /**
     * 下单用户id
     */
    private Long userId;

    /**
     * 商家ID
     */
    private Long mid;

    /**
     * 地址id
     */
    private Long addressBookId;

    /**
     * 下单时间
     */
    private Date orderTime;

    /**
     * 结账时间
     */
    private Date checkoutTime;

    /**
     * 支付方式 1微信,2支付宝
     */
    private Integer payMethod;

    /**
     * 实收金额
     */
    private BigDecimal amount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 收获人电话
     */
    private String phone;

    /**
     * 收获地址
     */
    private String address;

    /**
     * 收获人用户名
     */
    private String userName;

    /**
     * 收货人姓名
     */
    private String consignee;

    private static final long serialVersionUID = 1L;
}