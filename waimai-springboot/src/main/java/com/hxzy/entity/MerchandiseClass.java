package com.hxzy.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author 
 * 商品分类表
 */
@Data
public class MerchandiseClass implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 商家id
     */
    private Long mid;

    /**
     * 类别名称
     */
    @NotBlank(message = "分类名称不能为空")
    private String name;

    /**
     * 口味图片
     */
    @NotBlank(message = "分类图片不能为空")
    private String picture;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 更新人
     */
    private String updateBy;

    private static final long serialVersionUID = 1L;
}