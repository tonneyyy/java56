package com.hxzy.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 
 * 商品信息
 */
@Data
public class Merchandise implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 商品名称
     */
    @NotBlank(message = "商品名称不能为空")
    private String name;

    /**
     * 商品价格
     */
    @NotNull(message = "商品价格不能为空")
    private Float price;

    /**
     * 商品图片
     */
    @NotBlank(message = "商品图片不能为空")
    private String picture;

    /**
     * 商品描述
     */
    private String description;

    /**
     * 商品综合评分
     */
    private Integer score;

    /**
     * 商家id
     */
    private Long bid;

    /**
     * 商品分类
     */
    @NotNull(message = "商品分类不能为空")
    private Long mid;

    /**
     * 是否上架 1：上架 ，0：没上架
     */
    @NotNull(message = "是否上下架不能为空")
    private Integer isgrounding;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    private static final long serialVersionUID = 1L;
}