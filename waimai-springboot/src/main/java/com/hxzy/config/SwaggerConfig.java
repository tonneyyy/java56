package com.hxzy.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * 这个访问  http://localhost:8080/swagger-ui.html
 *
 * @author tonneyyy
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket() {
        //添加通用参数的集合
        List<Parameter> parameters=new ArrayList<>();
        // 1、创建一个header 通用参数
        ParameterBuilder tokenBuilder=new ParameterBuilder();
        tokenBuilder.name("Authorization")
                .description("令牌")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false).build();

        parameters.add(tokenBuilder.build());



        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //配置全局额外的参数
                .globalOperationParameters(parameters)
                // .enable(false) //配置是否启用Swagger，如果是false，在浏览器将无法访问
                .select()// 通过.select()方法，去配置扫描接口,RequestHandlerSelectors配置如何扫描接口
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                //配置你想在那个controller层生产接口文档
                .paths(PathSelectors.any())
                .build();
    }

    //配置文档信息
    private ApiInfo apiInfo() {
        Contact contact = new Contact("外卖程序员", "https://www.baidu.com", "xxxx@126.com");
        return new ApiInfo(
                "外卖文档", // 标题
                "外卖前后台api", // 描述
                "v1.0", // 版本
                "", // 组织链接
                contact, // 联系人信息
                "Apach 2.0 许可", // 许可
                "许可链接", // 许可连接
                new ArrayList<>()// 扩展
        );
    }
}