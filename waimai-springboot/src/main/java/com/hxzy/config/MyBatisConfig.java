package com.hxzy.config;

import com.github.pagehelper.PageInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis配置
 *
 * @author tonneyyy
 */
@MapperScan(basePackages = "com.hxzy.mapper")
@Configuration
public class MyBatisConfig {


}
