package com.hxzy.config;

import com.hxzy.common.properties.SMSProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Configuration
public class ResourceProperties {

    @Bean
    public SMSProperties smsProperties(){
        return new SMSProperties();
    }
}
