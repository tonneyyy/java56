package com.hxzy.controller.common;

import com.hxzy.common.controller.BaseController;
import com.hxzy.common.vo.CommonVO;
import com.hxzy.common.vo.R;
import com.hxzy.util.CommonDataUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 通用数据
 *
 * @author tonneyyy
 */
@RestController
@Api(tags ="通用数据API")
@RequestMapping(value = "/api")
public class CommonDataController extends BaseController {

    @ApiOperation(value = "通用值")
    @GetMapping(value = "/common/data")
    public R loadData(){
        Map<String ,List<CommonVO>>  mp=new HashMap<>();
        mp.put("sexData", CommonDataUtil.sexDataList());
        mp.put("stateData",CommonDataUtil.stateDataList());
        mp.put("auditData",CommonDataUtil.auditDataList());
        mp.put("shelfData",CommonDataUtil.shelfDataList());
        return R.okHasData(mp);
    }

    @ApiOperation(value = "商品口味爱好")
    @GetMapping(value = "/common/goodsflavor")
    public R goodsFlavor(){
        List<Map<String,Object>>  arr= new ArrayList<>();
        Map<String,Object>  data01=new HashMap<>();
        data01.put("name","辣度");
        data01.put("value", Arrays.asList("不辣","微辣","中辣","重辣"));
        arr.add(data01);

        Map<String,Object>  data02=new HashMap<>();
        data02.put("name","忌口");
        data02.put("value", Arrays.asList("不要葱","不要蒜","不要香菜","不要辣"));
        arr.add(data02);

        Map<String,Object>  data03=new HashMap<>();
        data03.put("name","温度");
        data03.put("value", Arrays.asList("热饮","常温","去冰","少冰","多冰"));
        arr.add(data03);

        Map<String,Object>  data04=new HashMap<>();
        data04.put("name","甜味");
        data04.put("value",  Arrays.asList("无糖","少糖","半糖","多糖","全糖"));
        arr.add(data04);

        return R.okHasData(arr);
    }

}
