package com.hxzy.controller.admin;

import com.hxzy.common.controller.BaseController;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.vo.R;
import com.hxzy.dto.CategorySearch;
import com.hxzy.entity.MerchandiseClass;
import com.hxzy.service.MerchandiseClassService;
import com.hxzy.util.SSMThreadLocal;
import com.hxzy.vo.AdminLoginVO;
import com.hxzy.vo.CategoryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * 商品分类API
 *
 * @author tonneyyy
 */
@Api(tags = "商品分类API")
@RestController
@RequestMapping(value = "/api/category")
public class CategoryController extends BaseController{

    @Autowired
    private MerchandiseClassService merchandiseClassService;

    @ApiOperation(value = "商品分类查询")
    @GetMapping(value = "/data")
    public R search(CategorySearch categorySearch){
        AdminLoginVO adminLoginVO= SSMThreadLocal.adminLoginVOThreadLocal.get();
        //是商家登录,只允许查询自己的数据
        if(adminLoginVO.getType()==1){
            categorySearch.setMid(adminLoginVO.getId());
        }

        super.buildPage(categorySearch);
        List<CategoryVO> list=this.merchandiseClassService.search(categorySearch);
        return super.pageToPageVO(list);
    }

    @ApiOperation(value = "新增分类 ")
    @PostMapping
    public R add(@RequestBody @Valid MerchandiseClass merchandiseClass){
      AdminLoginVO  adminLoginVO=SSMThreadLocal.adminLoginVOThreadLocal.get();
      if(adminLoginVO.getType()==0){
          return R.build(AckCode.NO_PERMISSION_TO_ACCESS);
      }

      merchandiseClass.setMid(adminLoginVO.getId());
      merchandiseClass.setCreateBy(adminLoginVO.getLoginName());
      merchandiseClass.setCreateTime(new Date());

      //保存
        int result = this.merchandiseClassService.insert(merchandiseClass);
        return super.toR(result);
    }

    @ApiOperation(value = "根据主键查询")
    @GetMapping(value = "/{id}")
    public R  findById(@PathVariable(value = "id") Long id){
        MerchandiseClass merchandiseClass = this.merchandiseClassService.selectByPrimaryKey(id);
        if(merchandiseClass==null){
            return R.build(AckCode.NOT_FOUND_DATA);
        }
        merchandiseClass.setCreateTime(null);
        return R.okHasData(merchandiseClass);
    }

    @ApiOperation(value = "修改分类 ")
    @PutMapping
    public R update(@RequestBody @Valid MerchandiseClass merchandiseClass){
        AdminLoginVO  adminLoginVO=SSMThreadLocal.adminLoginVOThreadLocal.get();
        if(adminLoginVO.getType()==0){
            return R.build(AckCode.NO_PERMISSION_TO_ACCESS);
        }

        merchandiseClass.setMid(adminLoginVO.getId());
        merchandiseClass.setUpdateBy(adminLoginVO.getLoginName());
        merchandiseClass.setUpdateTime(new Date());

        //保存
        int result = this.merchandiseClassService.updateByPrimaryKeySelective(merchandiseClass);
        return super.toR(result);
    }

    @ApiOperation(value = "删除商品分类")
    @DeleteMapping
    public R delete(Long[] ids){
        int count=this.merchandiseClassService.deleteIds(ids);

        if(count==0){
            return R.build(AckCode.CATEGORY_NOT_EMPTY);
        }else if( count!=ids.length){
            return R.build(AckCode.CATEGORY_NOT_EMPTY);
        }else{
            return R.ok();
        }
    }

    @ApiOperation(value = "根据商家ID查询商品分类")
    @GetMapping(value = "/bymid")
    public R findCategoryBymid(){
        AdminLoginVO  adminLoginVO=SSMThreadLocal.adminLoginVOThreadLocal.get();
        long mid=0;
        if(adminLoginVO.getType()==1) {
            mid = adminLoginVO.getId();
        }
        List<MerchandiseClass> arr=this.merchandiseClassService.findCategoryBymid(mid);
        return R.okHasData(arr);
    }

}
