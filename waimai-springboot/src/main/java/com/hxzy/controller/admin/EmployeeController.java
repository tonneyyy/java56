package com.hxzy.controller.admin;

import cn.hutool.crypto.digest.BCrypt;
import com.hxzy.common.consts.IAdd;
import com.hxzy.common.consts.IUpdate;
import com.hxzy.common.controller.BaseController;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.vo.R;
import com.hxzy.dto.EmployeeSearch;
import com.hxzy.entity.Employee;
import com.hxzy.service.EmployeeService;
import com.hxzy.util.SSMThreadLocal;
import com.hxzy.vo.AdminLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 后台员工
 *
 * @author tonneyyy
 */
@Api(tags = "后台员工API")
@RestController
@RequestMapping(value = "/api")
public class EmployeeController extends BaseController{

    @Autowired
    private EmployeeService  employeeService;

    @ApiOperation(value = "分页查询")
    @GetMapping(value = "/employee/data")
    public R searchData(EmployeeSearch employeeSearch){
        super.buildPage(employeeSearch);
        List<Employee> arr= this.employeeService.search(employeeSearch);
        return super.pageToPageVO(arr);
    }

    @ApiOperation(value = "根据主键查询")
    @GetMapping(value = "/employee/{id}")
    public R findById(@PathVariable(value = "id") Long id){
        Employee employee = this.employeeService.selectByPrimaryKey(id);
        if(employee==null){
            return R.build(AckCode.NOT_FOUND_DATA);
        }
        employee.setCreateTime(null);
        employee.setLoginPwd(null);
        return R.okHasData(employee);
    }

    @ApiOperation(value = "新增")
    @PostMapping(value = "/employee")
    public R add(@RequestBody @Validated(value = {IAdd.class}) Employee employee){
        //密码加密
        String hashpw = BCrypt.hashpw(employee.getLoginPwd());
        employee.setLoginPwd(hashpw);
        //新增人
        employee.setCreateTime(new Date());

        AdminLoginVO adminLoginVO= SSMThreadLocal.adminLoginVOThreadLocal.get();
        employee.setCreateBy(adminLoginVO.getLoginName());

        int row = this.employeeService.insert(employee);
        return super.toR(row);
    }

    @ApiOperation(value = "修改")
    @PutMapping(value = "/employee")
    public R update(@RequestBody @Validated(value = {IUpdate.class}) Employee employee){
        Employee dbEmployee = this.employeeService.selectByPrimaryKey(employee.getId());
        if(dbEmployee==null){
            return R.build(AckCode.NOT_FOUND_DATA);
        }

        dbEmployee.setLoginName(employee.getLoginName());
        dbEmployee.setAvatar(employee.getAvatar());
        dbEmployee.setGender(employee.getGender());
        dbEmployee.setStatus(employee.getStatus());

        //修改
        dbEmployee.setUpdateTime(new Date());

        AdminLoginVO adminLoginVO= SSMThreadLocal.adminLoginVOThreadLocal.get();
        dbEmployee.setUpdateBy(adminLoginVO.getLoginName());

        int row = this.employeeService.updateByPrimaryKeySelective(employee);
        return super.toR(row);
    }
}
