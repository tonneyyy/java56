package com.hxzy.controller.admin;

import cn.hutool.crypto.digest.BCrypt;
import com.hxzy.common.consts.RedisConst;
import com.hxzy.common.controller.BaseController;
import com.hxzy.common.dto.ExistsDTO;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.vo.R;
import com.hxzy.dto.MerchandiserChangePwd;
import com.hxzy.dto.MerchandiserSearch;
import com.hxzy.entity.Merchandiser;
import com.hxzy.service.ExistsService;
import com.hxzy.service.MerchandiseService;
import com.hxzy.service.MerchandiserService;
import com.hxzy.util.SSMThreadLocal;
import com.hxzy.vo.AdminLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * 商家信息
 *
 * @author tonneyyy
 */
@Api(tags = "商家信息API")
@RestController
@RequestMapping(value = "/api")
public class MerchandiserController extends BaseController {

    @Autowired
    private MerchandiserService merchandiserService;

    @Autowired
    private ExistsService existsService;

    @Autowired
    private RedisTemplate redisTemplate;


    @ApiOperation(value = "分页查询")
    @GetMapping(value = "/merchandiser/data")
     public R search(MerchandiserSearch search){
        super.buildPage(search);

        AdminLoginVO adminLoginVO = SSMThreadLocal.adminLoginVOThreadLocal.get();
        //普通商家
        if(adminLoginVO.getType()==1){
            search.setId(adminLoginVO.getId());
        }
        List<Merchandiser> arr=this.merchandiserService.search(search);
        return super.pageToPageVO(arr);
     }

    @ApiOperation(value = "根据主键查询")
    @GetMapping(value = "/merchandiser/{id}")
    public R findById(@PathVariable(value = "id") Long id){
        Merchandiser merchandiser = this.merchandiserService.selectByPrimaryKey(id);

        if(merchandiser==null){
            return R.build(AckCode.NOT_FOUND_DATA);
        }

        merchandiser.setCreateTime(null);
        return R.okHasData(merchandiser);
    }

    @ApiOperation(value = "查询登录名唯一性")
    @GetMapping(value = "/merchandiser/exists")
    public R existsLoginName(Long id,String loginName) {
        ExistsDTO existsDTO=new ExistsDTO();
        if(id!=null) {
            existsDTO.setPkName("id");
            existsDTO.setPkValue(id);
        }

        existsDTO.setColumName("login_name");
        existsDTO.setColumnValue(loginName);
        existsDTO.setTableName("merchandiser");

        int value = this.existsService.existsValue(existsDTO);

        R r=R.ok();
        if(value>0){
            r.setMsg(AckCode.EXISTS_VALUE.getMsg());
        }
        return r;
    }

    @ApiOperation(value = "修改")
    @PutMapping(value = "/merchandiser")
    public  R update(@RequestBody @Valid Merchandiser merchandiser){
        Merchandiser dbMerchandiser = this.merchandiserService.selectByPrimaryKey(merchandiser.getId());
        if(dbMerchandiser==null){
            return R.build(AckCode.NOT_FOUND_DATA);
        }
        //复制数据
        BeanUtils.copyProperties(merchandiser,dbMerchandiser,"loginPwd","status","createTime","createBy");

        AdminLoginVO adminLoginVO = SSMThreadLocal.adminLoginVOThreadLocal.get();
        dbMerchandiser.setUpdateBy(adminLoginVO.getLoginName());
        dbMerchandiser.setUpdateTime(new Date());

        //保存
        int result = this.merchandiserService.updateByPrimaryKeySelective(dbMerchandiser);
        return super.toR(result);
    }

    @ApiOperation(value = "修改密码")
    @PutMapping(value = "/merchandiser/changepwd")
    public  R changePwd(@RequestBody @Valid MerchandiserChangePwd merchandiserChangePwd){

        String redisKey= RedisConst.getRedisKey(RedisConst.SMS_REDIS_KEY,merchandiserChangePwd.getUuid());
       Object redisValue=this.redisTemplate.opsForValue().get(redisKey);
       if(redisValue==null){
           return R.build(AckCode.SMS_CODE_OVERTIME);
       }

       if(!redisValue.toString().equalsIgnoreCase(merchandiserChangePwd.getCode())){
           return  R.build(AckCode.SMS_CODE_WRONG);
       }

       //删除验证码
        this.redisTemplate.delete(redisKey);
        //密码加密
       String hashPwd= BCrypt.hashpw(merchandiserChangePwd.getLoginPwd());
       merchandiserChangePwd.setLoginPwd(hashPwd);
       int result=this.merchandiserService.changePwd(merchandiserChangePwd);
        return super.toR(result);
    }

}
