package com.hxzy.controller.front;

import com.hxzy.common.controller.BaseController;
import com.hxzy.common.vo.R;
import com.hxzy.dto.ShoppingCartSearch;
import com.hxzy.entity.ShoppingCart;
import com.hxzy.service.ShoppingCartService;
import com.hxzy.util.SSMThreadLocal;
import com.hxzy.vo.ContamerLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 购物车
 *
 * @author tonneyyy
 */
@Api(tags = "前置购物车API")
@RestController
@RequestMapping(value = "/front/user")
public class FrontShoppingCartController extends BaseController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @ApiOperation(value = "向购物车中添加数据")
    @PostMapping(value = "/shoppingCart/add")
    public R addShopCart(@RequestBody @Valid ShoppingCart shoppingCart){

        //得到当前登录前端用户
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();
        shoppingCart.setUserId(new Long(contamerLoginVO.getId()));

        shoppingCart.setNumber(1);
        shoppingCart.setCreateTime(new Date());

        int count = this.shoppingCartService.insert(shoppingCart);

        Map<String,Object> data=new HashMap<>();
        data.put("number", shoppingCart.getNumber());

        return R.okHasData(data);
    }

    @ApiOperation(value="根据商家ID和用户ID查询购物车信息")
    @GetMapping(value = "/shoppingCart/get/{id}")
    public R getShopCart(@PathVariable(value = "id") Long enterpriseId){
        ShoppingCartSearch  shoppingCartSearch=new ShoppingCartSearch();
        shoppingCartSearch.setEnterpriseId(enterpriseId);

        //先把当前用户
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();
        shoppingCartSearch.setUserId(new Long(contamerLoginVO.getId()));

        List<ShoppingCart> list= this.shoppingCartService.searchByShoppingCart(shoppingCartSearch);

        return R.okHasData(list);
    }
}
