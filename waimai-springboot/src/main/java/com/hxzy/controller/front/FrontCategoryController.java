package com.hxzy.controller.front;

import com.hxzy.common.vo.R;
import com.hxzy.entity.MerchandiseClass;
import com.hxzy.service.MerchandiseClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 前端分类控制器
 *
 * @author tonneyyy
 */
@Api(tags = "前端分类API")
@RestController
@RequestMapping(value = "/front")
public class FrontCategoryController {


    @Autowired
    private MerchandiseClassService merchandiseClassService;

    @ApiOperation(value = "根据商家查询分类信息")
    @GetMapping(value = "/category/list/{id}")
    public R findClassByMid(@PathVariable(value = "id") Long id){
        List<MerchandiseClass> classList = this.merchandiseClassService.findCategoryBymid(id);
        return R.okHasData(classList);
    }
}
