package com.hxzy.controller.front;

import com.hxzy.common.controller.BaseController;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.vo.R;
import com.hxzy.entity.AddressBook;
import com.hxzy.service.AddressBookService;
import com.hxzy.util.SSMThreadLocal;
import com.hxzy.vo.ContamerLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * 前端用户收货地址管理
 *
 * @author tonneyyy
 */
@Api(tags = "前端用户收货地址API")
@RestController
@RequestMapping(value = "/front/user/addressBook")
public class FrontAddressBookController extends BaseController {

    @Autowired
    private AddressBookService addressBookService;

    @ApiOperation(value = "获取当前用户所有收货地址")
    @GetMapping(value = "/list")
    public R addressListApi(){
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();
        List<AddressBook> addressBooks=this.addressBookService.findByUserId(contamerLoginVO.getId());
        return R.okHasData(addressBooks);
    }

    @ApiOperation(value = "根据主键查询信息")
    @GetMapping(value = "/{id}")
    public R findById(@PathVariable(value = "id") Long id){
        AddressBook addressBook = this.addressBookService.selectByPrimaryKey(id);
        if(addressBook==null){
            return R.build(AckCode.NOT_FOUND_DATA);
        }
        return R.okHasData(addressBook);
    }


    @ApiOperation(value = "新增收货地址")
    @PostMapping
    public R add(@RequestBody @Valid AddressBook addressBook){
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();

        if(addressBook.getUserId()==null){
            addressBook.setUserId(new Long(contamerLoginVO.getId()));
        }

        addressBook.setIsDeleted(0);
        addressBook.setCreateTime(new Date());
        addressBook.setCreateBy(new Long(contamerLoginVO.getId()));
        //不是默认收货地址
        addressBook.setIsDefault((byte) 0);

        int result = this.addressBookService.insert(addressBook);

        return super.toR(result);
    }


    @ApiOperation(value = "设定收货人默认地址")
    @PostMapping(value = "/default/{id}")
    public R defaultAddress(@PathVariable(value = "id") Long id){

        AddressBook addressBook = this.addressBookService.selectByPrimaryKey(id);
        if(addressBook==null){
            return R.build(AckCode.NOT_FOUND_DATA);
        }

        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();

        //你在恶意改别的人数据
        if(!contamerLoginVO.getId().equals(addressBook.getUserId().intValue())){
          return R.build(AckCode.NOT_MODIFY_OTHER_USER);
        }

        //先把这个用户 userId=?   全部设定 is_default=0
        //  update address_book set is_default=0  where user_id=?     and is_default=1

        //  再把这个id的值 is_default=1
        //  update address_book set is_default=1  where id=? and user_id=?

        int result=this.addressBookService.defaultAddress(addressBook);
        return super.toR(result);

    }

    @ApiOperation(value = "修改收货地址")
    @PutMapping
    public R update(@RequestBody @Valid AddressBook addressBook){
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();

        AddressBook dbBook = this.addressBookService.selectByPrimaryKey(addressBook.getId());

        //你在恶意改别的人数据
        if(!contamerLoginVO.getId().equals(dbBook.getUserId().intValue())){
            return R.build(AckCode.NOT_MODIFY_OTHER_USER);
        }

        //对象copy
        BeanUtils.copyProperties(addressBook,dbBook,"isDefault","isDeleted","createBy","createTime");
        dbBook.setUpdateBy(new Long(contamerLoginVO.getId()));
        dbBook.setUpdateTime(new Date());

        int result = this.addressBookService.updateByPrimaryKeySelective(dbBook);

        return super.toR(result);
    }

    @ApiOperation(value = "根据id删除收货地址")
    @DeleteMapping(value = "/{id}")
    public R deleteById(@PathVariable(value = "id") Long id){
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();
        AddressBook dbBook = this.addressBookService.selectByPrimaryKey(id);
        //你在恶意改别的人数据
        if(!contamerLoginVO.getId().equals(dbBook.getUserId().intValue())){
            return R.build(AckCode.NOT_MODIFY_OTHER_USER);
        }

        // 如果是默认收货地址，不允许删除
        if(dbBook.getIsDefault()==1){
            return R.build(AckCode.ADDRESSBOOK_NOT_DELETE);
        }

        int result=this.addressBookService.updateDelete(id);
        return super.toR(result);
    }


    @ApiOperation(value = "获取用户的默认收货地址")
    @GetMapping(value = "/default")
    public R searchDefaultAddress(){
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();

        List<AddressBook> bookList = this.addressBookService.findByUserId(contamerLoginVO.getId());
        if(bookList.size()>0){
            return R.okHasData(bookList.get(0));
        }

        return R.build(AckCode.NOT_FOUND_DATA);
    }

}
