package com.hxzy.controller.front;

import com.hxzy.common.vo.R;
import com.hxzy.service.MerchandiseService;
import com.hxzy.vo.ProductVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 前端商品信息
 *
 * @author tonneyyy
 */
@Api(tags = "前端商品API")
@RestController
@RequestMapping(value = "/front")
public class FrondProductController {

    @Autowired
    private MerchandiseService merchandiseService;

    /**
     * 获取某个商家的菜品分类对应的菜品
     * @param categoryId
     * @param bid
     * @return
     */
    @GetMapping(value = "/dish/list")
    public R  findProductByCategoryAndBid(Long categoryId,Long bid){
        List<ProductVO> list= this.merchandiseService.findProdeuctByCategoryIdAndbid(categoryId,bid);

        return R.okHasData(list);
    }
}
