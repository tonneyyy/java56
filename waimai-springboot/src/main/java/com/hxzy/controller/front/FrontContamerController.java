package com.hxzy.controller.front;

import com.hxzy.common.consts.RedisConst;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.vo.R;
import com.hxzy.dto.ContamerLoginDTO;
import com.hxzy.service.ContamerService;
import com.hxzy.util.SSMThreadLocal;
import com.hxzy.util.TokenServiceUtil;
import com.hxzy.vo.ContamerLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Api(tags = "前端用户API")
@RestController
@RequestMapping(value = "/front/user")
public class FrontContamerController {

    @Autowired
    private ContamerService  contamerService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TokenServiceUtil  tokenServiceUtil;

    @ApiOperation(value = "前端用户登录")
    @PostMapping(value = "/login")
    public R login(@RequestBody @Valid ContamerLoginDTO contamerLoginDTO){

        //验证验证码是否过期
        String redisKey= RedisConst.getRedisKey(RedisConst.CAPTCHA_KEY,contamerLoginDTO.getUuid());
        //查询键是不是存在
        Object redisValue=this.redisTemplate.opsForValue().get(redisKey);

        if(redisValue==null){
            return R.build(AckCode.SMS_CODE_OVERTIME);
        }
        //再判断值是否相等
        if(!redisValue.toString().equalsIgnoreCase(contamerLoginDTO.getCode())){
            return R.build(AckCode.SMS_CODE_WRONG);
        }
        //验证码验证成功，删除验证码
        this.redisTemplate.delete(redisKey);

        ContamerLoginVO contamerLoginVO=this.contamerService.login(contamerLoginDTO);

        //生成自定义对象，存储令牌
        String jwt=this.tokenServiceUtil.createJwtToken(contamerLoginVO);
        return R.okHasData(jwt);
    }

    @ApiOperation(value = "退出")
    @PostMapping(value = "/logout")
    public R logout(){
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();
        String redisKey= RedisConst.getRedisKey(RedisConst.CONTAMER_KEY,contamerLoginVO.getUuid());
        this.redisTemplate.delete(redisKey);
        return R.ok();
    }

    /**
     * 获取当前用户
     * @return
     */
    @ApiOperation(value = "获取当前用户信息")
    @GetMapping(value = "/info")
    public R getUserInfo(){
        ContamerLoginVO contamerLoginVO = SSMThreadLocal.contamerLoginVOThreadLocal.get();
        return R.okHasData(contamerLoginVO);
    }
}
