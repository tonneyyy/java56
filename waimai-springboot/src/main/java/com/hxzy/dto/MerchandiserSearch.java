package com.hxzy.dto;

import com.hxzy.common.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 商家搜索
 *
 * @author tonneyyy
 */
@Getter
@Setter
@ApiModel(value = "商家查询条件")
public class MerchandiserSearch extends PageDTO {
    @ApiModelProperty(value = "商家名称")
    private String  name;

    @ApiModelProperty(value = "商家编号")
    private Long id;
    /**
     * 审核状态（0已审核，1未审核）
     */
    @ApiModelProperty(value = "审核状态（0已审核，1未审核）")
    private Byte status;

    @ApiModelProperty(value = "开始时间yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;
}
