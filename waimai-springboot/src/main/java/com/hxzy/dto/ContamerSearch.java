package com.hxzy.dto;

import com.hxzy.common.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 前端客户查询
 *
 * @author tonneyyy
 */
@ApiModel(value = "前端用户查询模型")
@Getter
@Setter
public class ContamerSearch extends PageDTO {

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String loginid;

    /**
     * 性别(0 女 1 男)
     */
    @ApiModelProperty(value = "性别(0 女 1 男)")
    private Byte gender;

    /**
     * 状态1：停用0：正常
     */
    @ApiModelProperty(value = "状态1：停用0：正常",example = "0")
    private Integer status;


}
