package com.hxzy.dto;

import com.hxzy.entity.Merchandise;
import com.hxzy.vo.DishFlavorsVO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 商品保存
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class ProductDTO extends Merchandise {

    /**
     * 口味列表
     */
    private List<DishFlavorsVO> dishFlavors;
}
