package com.hxzy.dto;

import com.hxzy.common.dto.PageDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * 分类查询
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class CategorySearch extends PageDTO{

    /**
     * 商家id
     */
    private Long mid;
    /**
     * 分类名
     */
    private String name;
}
