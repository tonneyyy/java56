package com.hxzy.dto;

import com.hxzy.common.dto.PageDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * 商品查询
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class ProductSearch extends PageDTO {

    /**
     * 商家ID
     */
    private Long bid;

    /**
     * 商品名称
     */
    private String name;
    /**
     * 是否上架 1：上架 ，0：没上架
     */
    private Integer isgrounding;
}
