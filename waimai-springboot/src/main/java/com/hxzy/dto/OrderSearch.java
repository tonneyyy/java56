package com.hxzy.dto;

import com.hxzy.common.dto.PageDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * 订单查询
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class OrderSearch extends PageDTO {

    /**
     * 下单用户
     */
    private Long userId;


}
