package com.hxzy.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 商家更新密码
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class MerchandiserChangePwd {

    @NotNull(message = "id的值不能为空")
    private Long id;

    @NotBlank(message = "密码不能为空")
    private String loginPwd;

    @NotBlank(message = "验证码不能为空")
    private String code;

    @NotBlank(message = "验证码标识不能为空")
    private String uuid;
}
