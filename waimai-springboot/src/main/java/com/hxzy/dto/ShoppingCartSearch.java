package com.hxzy.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class ShoppingCartSearch {

    /**
     * 商家id
     */
    private Long enterpriseId;
    /**
     * 用户id
     */
    private Long userId;
}
