package com.hxzy.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 订单下单模型
 */
@Getter
@Setter
public class OrderDTO {

    /**
     * 备注
     */
    private String remark;

    /**
     * 支付方式 1微信,2支付宝
     */
    @NotNull(message = "支付方式不允许为空")
    private Integer payMethod;
    /**
     * 地址id
     */
    @NotNull(message = "收货地址不允许为空")
    private Long addressBookId;

    /**
     * 商家ID
     */
    @NotNull(message = "商家ID不允许为空")
    private Long mid;
}
