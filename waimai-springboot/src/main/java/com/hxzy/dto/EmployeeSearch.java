package com.hxzy.dto;

import com.hxzy.common.dto.PageDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述
 *
 * @author tonneyyy
 */
@ApiModel(value = "员工查询模型")
@Getter
@Setter
public class EmployeeSearch extends PageDTO {

    @ApiModelProperty(value = "登录账户")
    private String loginName;



    @ApiModelProperty(value = "性别(0 女 1 男)")
    private Byte gender;


    @ApiModelProperty(value = "状态（1停用，0正常）")
    private Byte status;

}
