package com.hxzy.dto;

import com.hxzy.common.enums.AckCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * 登录接收数据模型
 *
 * @author tonneyyy
 */
@Getter
@Setter
@ToString
public class LoginDTO {

    @NotBlank(message ="用户名不能为空")
    private String loginName;

    @NotBlank(message = "密码不能为空")
    private String loginPwd;

    @NotBlank(message = "验证码不能为空")
    private String code;

    @NotBlank(message = "验证码标识不能为空")
    private String uuid;
}
