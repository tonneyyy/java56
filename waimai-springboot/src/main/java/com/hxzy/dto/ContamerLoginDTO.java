package com.hxzy.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * 前端登录对象
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class ContamerLoginDTO {

    /**
     * 登录名（手机号码）
     */
    @NotBlank(message = "登录名不能为空")
    private String loginName;

    @NotBlank(message = "密码不能为空")
    private String password;
    @NotBlank(message = "验证码不能为空")
    private String code;

    @NotBlank(message = "验证码标识不能为空")
    private String uuid;


}
