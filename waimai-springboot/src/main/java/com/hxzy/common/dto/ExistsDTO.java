package com.hxzy.common.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 查询值是否存在
 *
 * @author tonneyyy
 */
@Getter
@Setter
public class ExistsDTO {
    /**
     * 主键列名
     */
    private String pkName;
    /**
     * 主键的值
     */
    private Object pkValue;

    /**
     * 查询列名的值
     */
    private String columName;
    /**
     * 查询列的值
     */
    private String columnValue;

    /**
     * 表格
     */
    private String tableName;

}
