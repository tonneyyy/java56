package com.hxzy.common.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 读取application.yml中的值
 *
 * @author tonneyyy
 */
@ConfigurationProperties( prefix = "sms")
@Getter
@Setter
public class SMSProperties {
    private String secretId;
    private String secretKey;
    private String signName;
    private String sdkAppId;
    private Integer expired;
}
