package com.hxzy.common.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 通用数据模型
 */
@Getter
@Setter
public class CommonVO {

    private Integer value;

    private String label;

    public CommonVO() {
    }

    public CommonVO(Integer value, String label) {
        this.value = value;
        this.label = label;
    }
}
