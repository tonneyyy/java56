package com.hxzy.common.consts;

/**
 * redis的常量
 *
 * @author tonneyyy
 */
public class RedisConst {

    /**
     * jwt 签名
     */
    public static final String JWT_SECURITY="kalfjda$%@fjfklafa!!@!0$%%$#";

    /**
     * 验证证码的常量
     */
    public static final String CAPTCHA_KEY="captcha:";

    /**
     * 验证码的过期时间
     */
    public static final Integer CAPTCHA_EXPIRED_MINUTES=3;


    /**
     * 管理员Employee的令牌前缀
     */
    public static final String EMPLOYEE_KEY="employee:";

    /**
     * 过期时间
     */
    public static final Integer EMPLOYEE_EXPIRED_MINUTES=120;

    /**
     * 前端用户登录令牌
     */
    public static final String CONTAMER_KEY="contamer:";

    /**
     * 过期时间(7天)
     */
    public static final Integer CONTAMER_EXPIRED_DAYS=7;
    /**
     * SMS前缀
     */
    public static final String SMS_REDIS_KEY="sms:";


    /**
     * 拼接redis的完整的键
     * @param prefix
     * @param value
     * @return
     */
    public static String getRedisKey(String prefix,String value){
        return prefix+value;
    }
}
