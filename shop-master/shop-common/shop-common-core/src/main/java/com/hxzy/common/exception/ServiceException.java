package com.hxzy.common.exception;

import com.hxzy.common.enums.AckCode;

/**
 * 业务逻辑异常
 *
 * @author tonneyyy
 */
public class ServiceException extends RuntimeException {

    private AckCode  ackCode;

    public AckCode getAckCode() {
        return ackCode;
    }

    public ServiceException() {
    }

    public ServiceException(AckCode ackCode) {
        super(ackCode.getMsg());
        this.ackCode = ackCode;   
    }

    public ServiceException(String message, AckCode ackCode) {
        super(message);
        this.ackCode = ackCode;
    }

    public ServiceException(String message, Throwable cause, AckCode ackCode) {
        super(message, cause);
        this.ackCode = ackCode;
    }

    public ServiceException(Throwable cause, AckCode ackCode) {
        super(cause);
        this.ackCode = ackCode;
    }
}
