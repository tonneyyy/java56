package com.hxzy.common.exception;

/**
 * 自定义字符串异常
 */
public class ShopException extends RuntimeException{

    public ShopException() {
    }

    public ShopException(String message) {
        super(message);
    }

    public ShopException(String message, Throwable cause) {
        super(message, cause);
    }
}
