package com.hxzy.common.consts;

/**
 * redis的常量
 *
 * @author tonneyyy
 */
public class RedisConst {

    /**
     * jwt 签名
     */
    public static final String JWT_SECURITY="kalfjda$%@fjfklafa!!@!0$%%$#";

    /**
     * 验证证码的常量
     */
    public static final String CAPTCHA_KEY="captcha:";

    /**
     * 验证码的过期时间
     */
    public static final Integer CAPTCHA_EXPIRED_MINUTES=5;


    /**
     * 管理员或商家的令牌前缀
     */
    public static final String ADMIN_LOGIN_KEY="login_admin:";

    /**
     * 管理员或商家过期时间
     */
    public static final Integer ADMIN_LOGIN_EXPIRED_MINUTES=120;

    /**
     * 前端用户登录令牌
     */
    public static final String CONTAMER_KEY="contamer:";

    /**
     * 过期时间(7天)
     */
    public static final Integer CONTAMER_EXPIRED_DAYS=7;
    /**
     * SMS前缀
     */
    public static final String SMS_REDIS_KEY="sms:";

    /**
     * 后台账户登录失败的key
     */
    public static final String ADMIN_LOGIN_FAILED_KEY="login_admin:failed:";

    /**
     * 后台账户登录最多可尝试5次
     */
    public static final Integer ADMIN_LOGIN_RETRY_COUNT=5;

    /**
     * 后台密码连续错误3次，账户锁定10分钟
     */
    public static final Integer ADMIN_LOGIN_FAILED_LOCK_TIME_MINUTES=10;


    /**
     * 使用hash来存储所有后台登录过的信息
     */
    public static final String ADMIN_LOGIN_ALL="ADMIN_LOGIN_HASH";

    /**
     * 拼接redis的完整的键
     * @param prefix
     * @param value
     * @return
     */
    public static String getRedisKey(String prefix,String value){
        return prefix+value;
    }
}
