package com.hxzy.common.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 分页返回
 */
@Getter
@Setter
public class PageVO {
    private Long total;

    private List rows;
}
