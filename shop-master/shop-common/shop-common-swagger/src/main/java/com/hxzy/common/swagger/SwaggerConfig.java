package com.hxzy.common.swagger;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * 这个访问  http://localhost:8080/swagger-ui.html
 *
 * @author tonneyyy
 */
@Configuration
@EnableConfigurationProperties(SwaggerPropereties.class)   //  把自定义SwaggerPropereties.java加载到spring容器中去
@EnableSwagger2
public class SwaggerConfig {


    /**
     * 自动装配 SwaggerPropereties  ，如果没有 定义@EnableConfigurationProperties(SwaggerPropereties.class) 空指令
     */
    @Autowired
    private SwaggerPropereties  swaggerPropereties;


    @Bean
    public Docket docket() {
        //添加通用参数的集合
        List<Parameter> parameters=new ArrayList<>();
        // 1、创建一个header 通用参数
        ParameterBuilder tokenBuilder=new ParameterBuilder();
        tokenBuilder.name("Authorization")
                .description("令牌")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false).build();

        parameters.add(tokenBuilder.build());



        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //配置全局额外的参数
                .globalOperationParameters(parameters)
                // .enable(false) //配置是否启用Swagger，如果是false，在浏览器将无法访问
                .select()// 通过.select()方法，去配置扫描接口,RequestHandlerSelectors配置如何扫描接口
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                //配置你想在那个controller层生产接口文档
                .paths(PathSelectors.any())
                .build();
    }

    //配置文档信息
    private ApiInfo apiInfo() {
        Contact contact = new Contact(this.swaggerPropereties.getTitle(), this.swaggerPropereties.getLicenseUrl(), this.swaggerPropereties.getEmail());

        return new ApiInfo(
                this.swaggerPropereties.getTitle(), // 标题
                this.swaggerPropereties.getDescription(), // 描述
                this.swaggerPropereties.getVersion(), // 版本
                this.swaggerPropereties.getTermsOfServiceUrl(), // 组织链接
                contact, // 联系人信息
                 this.swaggerPropereties.getLicense(), // 许可
                this.swaggerPropereties.getLicenseUrl(), // 许可连接
                new ArrayList<>()// 扩展
        );
    }
}