package com.hxzy.common.swagger;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import springfox.documentation.service.Contact;

/**
 * 读取swagger配置文件( swagger 前缀)
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "swagger")
public class SwaggerPropereties {

    private String title;

    private String description;

    private String version;

    private String termsOfServiceUrl;


    private String license;

    private String licenseUrl;

    private String email;
}
