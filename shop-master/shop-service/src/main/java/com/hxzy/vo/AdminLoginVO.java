package com.hxzy.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hxzy.entity.SysUser;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

/**
 * 管理员（商户）登录模型
 */
@Getter
@Setter
public class AdminLoginVO {
    /**
     * 商户信息
     */
    private SysUserShopVO sysUserShopVO;


    /**
     * 角色
     */
    private Set<String>  roleSet;


    /**
     * 菜单权限
     */
    private Set<String> permsSet;

    /**
     * 令牌
     */
    private String uuid;

    /**
     * 令牌创建时间
     */
    private Date createTime;
    /**
     * 令牌过期时间
     */
    private Date expiredTimer;


    /**
     * 判断是否是超级管理员，  序列到json吗？   @JsonIgnore 不要序列化这个方法
     * @return
     */
    @JsonIgnore
    public boolean isSuperMan(){
        return roleSet.contains("admin");
    }

}
