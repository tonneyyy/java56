package com.hxzy.vo;

import com.hxzy.entity.ShopDetail;
import com.hxzy.entity.SysUser;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SysUserShopVO extends SysUser {


    /**
     * 商户信息
     */
    private ShopDetail shopDetail;
}
