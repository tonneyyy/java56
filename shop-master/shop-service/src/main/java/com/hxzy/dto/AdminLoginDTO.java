package com.hxzy.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * 后台登录接收数据模型
 *
 * @author tonneyyy
 */
@Getter
@Setter
@ToString
public class AdminLoginDTO {

    @NotBlank(message ="用户名/手机号不能为空")
    private String account;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotBlank(message = "验证码不能为空")
    private String code;

    @NotBlank(message = "验证码标识不能为空")
    private String uuid;
}
