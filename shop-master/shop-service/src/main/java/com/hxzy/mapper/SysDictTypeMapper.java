package com.hxzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hxzy.entity.SysDictType;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典
 */
@Mapper
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}