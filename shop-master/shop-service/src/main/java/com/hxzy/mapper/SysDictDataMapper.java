package com.hxzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hxzy.entity.SysDictData;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典明细
 */
@Mapper
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

}