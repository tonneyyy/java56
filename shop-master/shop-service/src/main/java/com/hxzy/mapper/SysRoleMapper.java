package com.hxzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hxzy.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.Set;

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 查询用户拥有的权限
     * @param userId
     * @return
     */
    Set<String> searchUserOwnRole(Long userId);
}