package com.hxzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hxzy.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.Set;

@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据用户获取它的菜单权限
     * @param userId
     * @return
     */
    Set<String> searchUserOwnPerm(Long userId);
}