package com.hxzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hxzy.entity.ShopDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商家信息
 */
@Mapper
public interface ShopDetailMapper extends BaseMapper<ShopDetail> {

}