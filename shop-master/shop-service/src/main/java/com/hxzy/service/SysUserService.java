package com.hxzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hxzy.dto.AdminLoginDTO;
import com.hxzy.entity.SysUser;
import com.hxzy.vo.AdminLoginVO;

/**
 * 角色表
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 后台用户登录
     * @param adminLoginDTO
     * @return
     */
    AdminLoginVO  login(AdminLoginDTO  adminLoginDTO);
}
