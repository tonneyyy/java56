package com.hxzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hxzy.entity.SysDictType;

/**
 * 数据字典
 */
public interface SysDictTypeService extends IService<SysDictType> {
}
