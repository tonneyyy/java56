package com.hxzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hxzy.entity.SysRole;

import java.util.Set;

/**
 * 角色业务逻辑
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 查询用户拥有的权限
     * @param userId
     * @return
     */
    Set<String> searchUserOwnRole(Long userId);
}
