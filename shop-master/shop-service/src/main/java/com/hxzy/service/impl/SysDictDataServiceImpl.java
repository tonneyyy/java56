package com.hxzy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hxzy.entity.SysDictData;
import com.hxzy.mapper.SysDictDataMapper;
import com.hxzy.service.SysDictDataService;
import org.springframework.stereotype.Service;

/**
 * 数据字典明细
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements SysDictDataService {
}
