package com.hxzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hxzy.entity.SysRole;
import com.hxzy.mapper.SysRoleMapper;
import com.hxzy.service.SysRoleService;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * 角色业务逻辑实现
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    /**
     * 查询用户拥有的权限
     * @param userId
     * @return
     */
    @Override
    public Set<String> searchUserOwnRole(Long userId) {

        return this.baseMapper.searchUserOwnRole(userId);
    }
}
