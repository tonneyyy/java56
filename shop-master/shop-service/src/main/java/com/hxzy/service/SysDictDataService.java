package com.hxzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hxzy.entity.SysDictData;

/**
 * 数据字典明细
 */
public interface SysDictDataService extends IService<SysDictData> {
}
