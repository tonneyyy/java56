package com.hxzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hxzy.entity.SysMenu;

import java.util.Set;

/**
 * 菜单表
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 根据用户获取它的菜单权限
     * @param userId
     * @return
     */
    Set<String> searchUserOwnPerm(Long userId);
}
