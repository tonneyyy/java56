package com.hxzy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hxzy.entity.SysDictType;
import com.hxzy.mapper.SysDictTypeMapper;
import com.hxzy.service.SysDictTypeService;
import org.springframework.stereotype.Service;

/**
 * 数据字典
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {
}
