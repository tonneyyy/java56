package com.hxzy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hxzy.entity.ShopDetail;
import com.hxzy.mapper.ShopDetailMapper;
import com.hxzy.service.ShopDetailService;
import org.springframework.stereotype.Service;

/**
 * 商家
 */
@Service
public class ShopDetailServiceImpl extends ServiceImpl<ShopDetailMapper, ShopDetail> implements ShopDetailService {
}
