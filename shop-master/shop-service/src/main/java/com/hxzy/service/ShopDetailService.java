package com.hxzy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hxzy.entity.ShopDetail;

/**
 * 商家信息表
 */
public interface ShopDetailService extends IService<ShopDetail> {
}
