package com.hxzy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hxzy.entity.SysMenu;
import com.hxzy.mapper.SysMenuMapper;
import com.hxzy.service.SysMenuService;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * 菜单表
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    /**
     * 根据用户获取它的菜单权限
     * @param userId
     * @return
     */
    @Override
    public Set<String> searchUserOwnPerm(Long userId) {
        return this.baseMapper.searchUserOwnPerm(userId);
    }
}
