package com.hxzy.config;

import com.hxzy.config.interceptor.AdminTokenValidatorInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * springmvc的前端控制器配置文件，以前的 <servlet-name>-servlet.xml，现在使用注解方式
 */
@Configuration
public class ShopMvcConfig  implements WebMvcConfigurer {

    /**
     * 后台要放行的url地址
     */
    @Value(value = "${token.admin.exclude}")
    private String[] adminExcludeMapping;

    /**
     * 后台jwt验证拦截器
     */
    @Autowired
    private AdminTokenValidatorInterceptor  adminTokenValidatorInterceptor;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //后台拦截器配置
        registry.addInterceptor(this.adminTokenValidatorInterceptor).addPathPatterns("/api/**").excludePathPatterns(adminExcludeMapping);

    }

    /**
     * 跨域配置
     */
    @Bean
    public CorsFilter corsFilter()
    {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // 设置访问源地址
        config.addAllowedOriginPattern("*");
        // 设置访问源请求头
        config.addAllowedHeader("*");
        // 设置访问源请求方法
        config.addAllowedMethod("*");
        // 有效期 1800秒
        config.setMaxAge(1800L);

        // 添加映射路径，拦截一切请求
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        // 返回新的CorsFilter
        return new CorsFilter(source);
    }


}
