package com.hxzy.util;

import com.hxzy.vo.AdminLoginVO;

/**
 * 用来存储全局的当前登录用户对象信息(本地线程池来实现)
 */
public class ShopThreadLocalUtil {


    /**
     * 本地线程池（如果你要做多线程修改数据，同步修改这个线程中的数据，这个本地线程池就会有问题。数据同步不了）
     * 需要换阿里的框架 transmittable-thread-local
     * TransmittableThreadLocal<AdminLoginVO> transmittableThreadLocal = new TransmittableThreadLocal<>();
     */
    public static ThreadLocal<AdminLoginVO>  adminThreadLocal=new ThreadLocal<>();

}
