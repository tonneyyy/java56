package com.hxzy.controller.admin;

import cn.hutool.core.util.IdUtil;
import com.hxzy.common.enums.AckCode;
import com.hxzy.common.vo.R;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Api(tags = "腾讯云存储API")
@RestController
@RequestMapping(value = "/api/cos")
public class TencentUploadController {


    /**
     * 区域
     */
    private final String REGION_NAME="ap-nanjing";

    /**
     * 桶名（独立文件名）
     */
    private final String BUCKETNAME="shop-1300180953";

    @ApiOperation(value = "图片上传")
    @PostMapping(value = "/upload/image")
    public R imageUpload(MultipartFile file) throws IOException {
        if(file.isEmpty()){
            return R.build(AckCode.FILE_NOT_EMPTY);
        }

        COSClient cosClient=createCosClient();

        List<String> stringList = Arrays.asList(".png", ".jpg",".jpeg");

        //上传原始的文件名
        String oriFile= file.getOriginalFilename();
        //取得文件后缀名
        String extend= oriFile.substring(oriFile.lastIndexOf(".")).toLowerCase();
        if(!stringList.contains(extend)){
            return R.build(AckCode.UPLOAD_TYPE_ERROR_IMAGE);
        }

       // 指定文件将要存放的存储桶
       // 指定文件上传到 COS 上的路径，即对象键。例如对象键为 folder/picture.jpg，则表示将文件 picture.jpg 上传到 folder 路径下

        String fileName= IdUtil.fastSimpleUUID();
        String key = "images/"+fileName+""+extend;

        //上传流
        ObjectMetadata objectMetadata = new ObjectMetadata();
        // 上传的流如果能够获取准确的流长度，则推荐一定填写 content-length
        // 如果确实没办法获取到，则下面这行可以省略，但同时高级接口也没办法使用分块上传了
        objectMetadata.setContentLength(file.getSize());

        PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKETNAME, key, file.getInputStream(),objectMetadata);
        //上传图片，得到返回结果
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

        //关闭线程
        cosClient.shutdown();

        Map<String,Object> data=new HashMap<>();
        //上传的文件名
        data.put("fileName",oriFile);
        data.put("url","https://"+BUCKETNAME+".cos."+REGION_NAME+".myqcloud.com/"+key);

        return R.okHasData(data);
    }


    private COSClient  createCosClient(){
        // 1 初始化用户身份信息（secretId, secretKey）。
        // SECRETID和SECRETKEY 请登录访问管理控制台 https://console.cloud.tencent.com/cam/capi 进行查看和管理
        String secretId = "AKIDrSmZ6M199sOedNa2wyjCOIo3nKcdgowp";
        String secretKey = "lHVsymIX2HFxNYR6UcT7UbS2PWS8gFhu";
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置 bucket 的地域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region(REGION_NAME);

        ClientConfig clientConfig = new ClientConfig(region);
        // 这里建议设置使用 https 协议
        // 从 5.6.54 版本开始，默认使用了 https
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);

        return cosClient;
    }


}
