package com.hxzy.controller.admin;

import com.hxzy.common.enums.AckCode;
import com.hxzy.common.vo.R;
import com.hxzy.entity.SysRole;
import com.hxzy.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "后台角色API")
@RestController
@RequestMapping(value = "/api/role")
public class SysRoleController {


    @Autowired
    private SysRoleService sysRoleService;


    @ApiOperation(value = "根据主键查询")
    @GetMapping(value = "/{id}")
    public R findById(@PathVariable(value = "id") Long id){
        SysRole sysRole = this.sysRoleService.getById(id);
        if(sysRole==null){
            return R.build(AckCode.NOT_FOUND_DATA);
        }
        return R.okHasData(sysRole);
    }
}
